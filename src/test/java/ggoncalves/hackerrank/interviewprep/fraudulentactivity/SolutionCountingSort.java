package ggoncalves.hackerrank.interviewprep.fraudulentactivity;

public class SolutionCountingSort {

  // Complete the activityNotifications function below.
  static int activityNotifications(int[] expenditure, int d) {

    int[] aux = new int[201];

    for (int i = 0; i < d; i++) {
      aux[expenditure[i]]++;
    }

    int notifications = 0;

    for (int i = d; i < expenditure.length; i++) {

      double median = getMedian(aux, d);

      if (expenditure[i] >= 2 * median) notifications++;

      aux[expenditure[i]]++;
      aux[expenditure[i - d]]--;

    }


    return notifications;

  }

  static double getMedian(int[] aux, int d) {

    if (d % 2 == 1) {

      int sum = 0;

      for (int i = 0; i < aux.length; i++) {
        sum += aux[i];
        if (sum > d / 2) {
          return i;
        }
      }

    } else {

      int sum = 0;

      for (int i = 0; i < aux.length; i++) {
        sum += aux[i];

        if (sum == d / 2) {
          return (double) (i + (i + 1)) / 2d;
        } else if (sum > d / 2) {
          return i;
        }
      }
    }
    return -100d;
  }

  public static void main(String[] args) {
//    int[] exp = {2, 3, 4, 2, 3, 6, 8, 4, 5};
    int[] exp = {1, 2, 3, 4, 4};
//    System.out.println(SolutionCountingSort.activityNotifications(exp, 5));
    System.out.println(SolutionCountingSort.activityNotifications(exp, 4));
  }

}
