package ggoncalves.hackerrank.mergingcommunities;

import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * Created by ggoncalves on 03/04/17.
 */
public class MergingCommunitiesTest {

//    sb.append("3 6   ");
//    sb.append("Q 1   ");
//    sb.append("M 1 2 ");
//    sb.append("Q 2   ");
//    sb.append("M 2 3 ");
//    sb.append("Q 3   ");
//    sb.append("Q 2   ");

    @Test
    public void simpleTestCase() throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("3 6 ");
        sb.append("Q 1 ");
        sb.append("M 1 2 ");
        sb.append("Q 2 ");
        sb.append("M 2 3 ");
        sb.append("Q 3 ");
        sb.append("Q 2 ");
        assertFind(6, sb.toString());
    }

    private void assertFind(int expected, String input) {

        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Solution wt = new Solution();

    }

}