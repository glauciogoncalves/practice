package ggoncalves.hackerrank.spanningtreefraction;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class SpanningTreeFractionTest {

  @Test
  public void testSimple() throws Exception {
    List<Solution.Edge> edges = new ArrayList<>();

    edges.add(new Solution.Edge(0, 1, 1, 1));
    edges.add(new Solution.Edge(1, 2, 2, 4));
    edges.add(new Solution.Edge(2, 0, 1, 2));
    edges.add(new Solution.Edge(2, 0, 8, 8));
    edges.add(new Solution.Edge(2, 0, 2, 2));
    edges.add(new Solution.Edge(2, 0, 2, 1));
    edges.add(new Solution.Edge(2, 0, 4, 2));
    edges.add(new Solution.Edge(2, 0, 4, 8));
    edges.add(new Solution.Edge(2, 0, 8, 16));

    new Solution(3, edges);
  }

  @Test
  public void testPrintResultFraction() throws Exception {
    Solution s = new Solution();
    String res = s.mountResult(2, 3);
    assertEquals("2/3", res);
  }

  @Test
  public void testPrintResultInteger1() throws Exception {
    Solution s = new Solution();
    String res = s.mountResult(2, 1);
    assertEquals("2/1", res);
  }

  @Test
  public void testPrintResultInteger2() throws Exception {
    Solution s = new Solution();
    String res = s.mountResult(4, 2);
    assertEquals("2/1", res);
  }

  @Test
  public void testPrintResultInteger3() throws Exception {
    Solution s = new Solution();
    String res = s.mountResult(10, 5);
    assertEquals("2/1", res);
  }

  @Test
  public void testPrintResultInteger4() throws Exception {
    Solution s = new Solution();
    String res = s.mountResult(9, 3);
    assertEquals("3/1", res);
  }

  @Test
  public void testPrintResultInteger5Equals() throws Exception {
    Solution s = new Solution();
    String res = s.mountResult(10, 10);
    assertEquals("1/1", res);
  }

}