package ggoncalves.hackerrank.BST;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static ggoncalves.hackerrank.BST.IsThisABinarySearchTree.Node;
import static org.junit.Assert.assertEquals;

public class IsThisABinarySearchTreeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private void assertBST(Integer[] array, boolean expectedResult) {

    Node root = mountTree(array, 0, array.length - 1);

    assertEquals(3, root.data);

//    inOrderPrint(root);

    IsThisABinarySearchTree tree = new IsThisABinarySearchTree();
    assertEquals(expectedResult, tree.checkBST(root));

  }

  private void inOrderPrint(Node node) {
    if (node == null) return;

    inOrderPrint(node.left);
    System.out.println(node.data);
    inOrderPrint(node.right);
  }

  private Node mountTree(Integer[] array, int start, int end) {

    if (start > end) return null;

    int mid = start + (end - start) / 2;

    Node node = new Node(array[mid]);

    node.left = mountTree(array, start, mid - 1);
    node.right = mountTree(array, mid + 1, end);

    return node;
  }

  @Test
  public void checkBST() {

    // 1 2 4 3 5 6 7
    Integer[] array = {1, 2, 4, 3, 5, 6, 7};
    assertBST(array, false);


  }
}