package ggoncalves.hackerrank.minimumaveragewaitingtime;

import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;

/**
 * Created by ggoncalves on 01/04/17.
 */
public class MinimumAverageWaitingTimeTest {

    @Test
    public void readAndSolve1() throws Exception {
        int[][] testCase = {
                {3},
                {0, 3},
                {1, 9},
                {2, 5}
        };
        assertFindMinimumAverageWaitingTime(8, testCase);
    }

    @Test
    public void readAndSolve2() throws Exception {
        int[][] testCase = {
                {3},
                {2, 5},
                {1, 9},
                {0, 3}
        };
        assertFindMinimumAverageWaitingTime(8, testCase);
    }

    @Test
    public void readAndSolve3() throws Exception {
        int[][] testCase = {
                {3},
                {0, 3},
                {1, 9},
                {2, 6}
        };
        assertFindMinimumAverageWaitingTime(9, testCase);
    }

    @Test
    public void readAndSolve4() throws Exception {
        int[][] testCase = {
                {3},
                {2, 6},
                {1, 9},
                {0, 3}
        };
        assertFindMinimumAverageWaitingTime(9, testCase);
    }

    @Test
    public void readAndSolve5() throws Exception {
        int[][] testCase = {
                {5},
                {961148050, 385599125},
                {951133776, 376367013},
                {283280121, 782916802},
                {317664929, 898415172},
                {980913391, 847912645}
        };
        assertFindMinimumAverageWaitingTime(1418670047, testCase);
    }

    @Test
    public void readAndSolve6() throws Exception {
        int[][] testCase = {
                {2},
                {0, 9},
                {10, 4}
        };
        assertFindMinimumAverageWaitingTime(6, testCase);
    }
// 2
// 0 9
// 10 4
    // Result 6

    private void assertFindMinimumAverageWaitingTime(int expected, int[][] input) {

        ByteArrayInputStream in = new ByteArrayInputStream(convertIntMatrixToString(input).getBytes());
        System.setIn(in);
        Solution wt = new Solution();
        long result = wt.findMinimumAverageWaitingTime();
        assertEquals(expected, result);

    }

    private String convertIntMatrixToString(int[][] input) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.length; i++) {

            for (int j = 0; j < input[i].length; j++) {

                sb.append(input[i][j] + " ");

            }

        }

        return sb.toString();
    }


    //    @Test
    public void makePizza() throws Exception {

    }

}