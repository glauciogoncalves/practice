package ggoncalves.hackerrank.componentsingraph;

import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * Created by ggoncalves on 02/04/17.
 */
public class ComponentsInGraphTest {

    @Test
    public void find() throws Exception {
        int[][] testCase = {
                {5},
                {1, 6},
                {2, 7},
                {3, 8},
                {4, 9},
                {2, 6}
        };
        assertFind(6, testCase);
    }


//    40
//            {5 ,56},
//            {4 ,51},
//            {2 ,53},
//            {8 ,52},
//            {5 ,43},
//            {2 ,80},
//            {5 ,47},
//            {4 ,79},
//            {3 ,75},
//            {1 ,67},
//            {7 ,61},
//            {2 ,57},
//            {5 ,47},
//            {4 ,63},
//            {10,79},
//            {1 ,57},
//            {4 ,42},
//            {8 ,79},
//            {6 ,53},
//            {3 ,74},
//            {7 ,60},
//            {10,79},
//            {5 ,46},
//            {3 ,50},
//            {6 ,57},
//            {8 ,71},
//            {6 ,74},
//            {10,44},
//            {9 ,80},
//            {7 ,59},
//            {7 ,74},
//            {6 ,55},
//            {3 ,77},
//            {3 ,53},
//            {5 ,50},
//            {9 ,70},
//            {4 ,72},
//            {5 ,73},
//            {6 ,70},
//            {7 ,46},

    @Test
    public void find2() throws Exception {
        int[][] testCase = {
                {40},
                {5, 56},
                {4, 51},
                {2, 53},
                {8, 52},
                {8, 79},
                {5, 43},
                {2, 80},
                {5, 47},
                {4, 79},
                {3, 75},
                {1, 67},
                {7, 61},
                {2, 57},
                {5, 47},
                {4, 63},
                {10, 79},
                {1, 57},
                {4, 42},
                {6, 53},
                {3, 74},
                {7, 60},
                {10, 79},
                {5, 46},
                {3, 50},
                {6, 57},
                {8, 71},
                {6, 74},
                {10, 44},
                {9, 80},
                {7, 59},
                {7, 74},
                {6, 55},
                {3, 77},
                {3, 53},
                {5, 50},
                {9, 70},
                {4, 72},
                {5, 73},
                {6, 70},
                {7, 46}
        };
        assertFind(6, testCase);
    }


    //    @Test
    public void smallest() throws Exception {

    }

    //    @Test
    public void bigest() throws Exception {

    }

    private void assertFind(int expected, int[][] input) {

        ByteArrayInputStream in = new ByteArrayInputStream(convertIntMatrixToString(input).getBytes());
        System.setIn(in);
        Solution wt = new Solution();

    }

    private String convertIntMatrixToString(int[][] input) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.length; i++) {

            for (int j = 0; j < input[i].length; j++) {

                sb.append(input[i][j] + " ");

            }

        }

        return sb.toString();
    }

}