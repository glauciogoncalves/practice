package ggoncalves.hackerrank.string;

import org.junit.Test;

import static org.junit.Assert.*;

public class TwoCharactersTest {

  //  @Test
  public void primarySample() {
    assertEquals(5, TwoCharacters.alternate("beabeefeab"));
  }

  @Test
  public void expression_True_1() {
    assertTrue(TwoCharacters.checkExpression("ab"));
  }

  @Test
  public void expression_True_2() {
    assertTrue(TwoCharacters.checkExpression("bab"));
  }

  @Test
  public void expression_True_3() {
    assertTrue(TwoCharacters.checkExpression("bababababababa"));
  }

  @Test
  public void expression_False_1() {
    assertFalse(TwoCharacters.checkExpression("baab"));
  }

  @Test
  public void expression_False_2() {
    assertFalse(TwoCharacters.checkExpression("bbab"));
  }

  @Test
  public void expression_False_3() {
    assertFalse(TwoCharacters.checkExpression("kjj"));
  }

  @Test
  public void alternate_1() {
    assertEquals(5, TwoCharacters.alternate("beabeefeab"));
  }

  @Test
  public void alternate_2() {
    assertEquals(8, TwoCharacters.alternate("asdcbsdcagfsdbgdfanfghbsfdab"));
  }

  @Test
  public void alternate_3() {
    assertEquals(0, TwoCharacters.alternate("asvkugfiugsalddlasguifgukvsa"));
  }


  @Test
  public void expression_True_4() {
    TwoCharacters.permute2("abcde", "");
  }

}