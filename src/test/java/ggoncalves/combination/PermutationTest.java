package ggoncalves.combination;

import static org.junit.Assert.*;

import org.junit.Test;

public class PermutationTest {

	@Test
	public void testNextPermutation() {
		assertEquals("abced", Permutation.nextPermutation("abcde"));
	}
	
	@Test
	public void testNextPermutation_2() {
		assertEquals("adffigjkkm", Permutation.nextPermutation("adffgmkkji"));
	}
	
	@Test
	public void testNextPermutation_Null() {
		assertNull(Permutation.nextPermutation("a"));
	}
	
	@Test
	public void testNextPermutation_Null_2() {
		assertNull(Permutation.nextPermutation("ba"));
	}
	
	@Test
	public void testNextPermutation_Null_3() {
		assertNull(Permutation.nextPermutation(""));
	}
	
	@Test
	public void testNextPermutation_Null_4() {
		assertNull(Permutation.nextPermutation("  "));
	}

	@Test
	public void testNextPermutation_Null_5() {
		assertNull(Permutation.nextPermutation(null));
	}
}
