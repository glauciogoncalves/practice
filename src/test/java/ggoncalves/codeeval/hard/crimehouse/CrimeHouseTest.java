package ggoncalves.codeeval.hard.crimehouse;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Classe de teste unitário para o problema do Crime Time (hard).
 */
public class CrimeHouseTest {

    public void assertCrimeHouseCase(String expected, String lineCase) {
        CrimeHouse ch = new CrimeHouse();
        String result = ch.crimeHouse(lineCase);
        assertEquals(expected, result);
    }

    @Test
    public void testCrimeHouseCase1() {
        assertCrimeHouseCase("1", "3; E 5|L 0|E 5");
    }

    @Test
    public void testCrimeHouseCase2() {
        assertCrimeHouseCase("CRIME TIME", "2; L 1|L 1");
    }

    @Test
    public void testCrimeHouseCase3() {
        assertCrimeHouseCase("1", "4; L 1|E 0|E 0|L 1");
    }

    @Test
    public void testCrimeHouseCase4() {
        assertCrimeHouseCase("4", "7; L 2|E 0|E 1|E 2|E 0|E 3|L 4");
    }

    @Test
    public void testCrimeHouseCase5() {
        assertCrimeHouseCase("0", "13; L 4|L 1|L 2|E 0|L 1|E 0|L 2|E 0|L 2|E 0|E 0|L 1|L 4");
    }

    @Test
    public void testCrimeHouseMyCase1() {
        assertCrimeHouseCase("CRIME TIME", "13; E 5|E 5|E 5|L 0|E 0");
    }

    @Test
    public void testCrimeHouseMyCase2() {
        assertCrimeHouseCase("CRIME TIME", "13; E 5|E 2|E 5|E 2|L 0|E 0");
    }

    @Test
    public void testCrimeHouseMyCase3() {
        assertCrimeHouseCase("CRIME TIME", "13; E 5|E 2|L 0|L 2|L 2|E 0");
    }

    @Test
    public void testCrimeHouseMyCase4() {
        assertCrimeHouseCase("CRIME TIME", "13; E 5|E 2|L 2|E 5");
    }

    @Test
    public void testCrimeHouseMyCase5() {
        assertCrimeHouseCase("CRIME TIME", "13; E 5|E 2|L 5|E 3|L 5");
    }

    @Test
    public void testCrimeHouseMyCase6() {
        assertCrimeHouseCase("1", "13; E 5|E 2|L 5|E 0|L 5");
    }
}