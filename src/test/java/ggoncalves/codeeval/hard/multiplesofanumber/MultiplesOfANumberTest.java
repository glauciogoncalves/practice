package ggoncalves.codeeval.hard.multiplesofanumber;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Implementação do problema:
 * <p/>
 * https://www.codeeval.com/open_challenges/18/
 *
 * @author glaucio.c.goncalves@gmail.com
 */
public class MultiplesOfANumberTest {

    @Test
    public void testMultiplesOfANumberEmpty() throws Exception {
        MultiplesOfANumber mn = new MultiplesOfANumber();
        String result = mn.multiplesOfANumber("");
        assertTrue(result.isEmpty());
    }

    @Test
    public void testMultiplesOfANumberSiteCase1() throws Exception {
        MultiplesOfANumber mn = new MultiplesOfANumber();
        String result = mn.multiplesOfANumber("13,8");
        assertEquals("16", result);
    }

    @Test
    public void testMultiplesOfANumberSiteCase2() throws Exception {
        MultiplesOfANumber mn = new MultiplesOfANumber();
        String result = mn.multiplesOfANumber("17,16");
        assertEquals("32", result);
    }

    @Test
    public void testDummy() throws Exception {

        int p = 16;
        int zeros = 0;
        do {
            p = p >>> 1;
            zeros++;
        }
        while (p > 1);

        int n = 0b1 << (zeros + 1);

        System.out.println(n);

        int count = 1;
        while (n < 256) {
            n = (0b10 + count++) << zeros;
            System.out.println(n);
        }

        System.out.println(zeros);


        System.out.println(n);
        System.out.println(Integer.toBinaryString(n));

        n = (0b10 + 1) << 3;
        System.out.println(n);
        System.out.println(Integer.toBinaryString(n));

        n = (0b10 + 1 + 1) << 3;
        System.out.println(n);
        System.out.println(Integer.toBinaryString(n));

        n = (0b10 + 1 + 1 + 1) << 3;
        System.out.println(n);
        System.out.println(Integer.toBinaryString(n));

        n = (0b10 + 1 + 1 + 1 + 1) << 3;
        System.out.println(n);
        System.out.println(Integer.toBinaryString(n));

    }
}