package ggoncalves.codeeval.hard.robotmove;

import org.junit.Test;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Teste para o problema "Robot Movements".
 * <p/>
 * https://www.codeeval.com/public_sc/56/
 */
public class RobotMoveMainTest {

    public void testCalculate() throws Exception {
        RobotMoveMain rm = new RobotMoveMain();
        System.out.println(rm.getIndexFor('a'));
        System.out.println(rm.getIndexFor('b'));
        System.out.println(rm.getIndexFor('p'));

    }

    @Test
    public void testDummy() throws Exception {
        System.out.println(1 << 1);
    }
}
