package ggoncalves.codeeval.hard.stringpermutations;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringPermutationMainTest {

	@Test
	public void testStringPermutationSite1() {
		StringPermutationMain main = new StringPermutationMain();
		String result = main.stringPermutation("hat");
		assertEquals("aht,ath,hat,hta,tah,tha", result);
	}

	@Test
	public void testStringPermutationSite2() {
		StringPermutationMain main = new StringPermutationMain();
		String result = main.stringPermutation("abc");
		assertEquals("abc,acb,bac,bca,cab,cba", result);
	}
	
	@Test
	public void testStringPermutationSite3() {
		StringPermutationMain main = new StringPermutationMain();
		String result = main.stringPermutation("Zu6");
		assertEquals("6Zu,6uZ,Z6u,Zu6,u6Z,uZ6", result);
	}
}
