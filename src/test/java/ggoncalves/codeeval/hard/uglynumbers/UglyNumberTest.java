package ggoncalves.codeeval.hard.uglynumbers;

import static org.junit.Assert.*;

import org.junit.Test;

public class UglyNumberTest {

	@Test
	public void testSiteCase1() {
		
		Main main = new Main();
		String result = main.uglyNumbersCount("1");
		assertEquals("0", result);
		
	}
	
	@Test
	public void testSiteCase2() {
		
		Main main = new Main();
		String result = main.uglyNumbersCount("9");
		assertEquals("1", result);
		
	}
	
	@Test
	public void testSiteCase3() {
		
		Main main = new Main();
		String result = main.uglyNumbersCount("011");
		assertEquals("6", result);
		
	}
	
	@Test
	public void testSiteCase4() {
		
		Main main = new Main();
		String result = main.uglyNumbersCount("12345");
		assertEquals("64", result);
		
	}
	
	@Test
	public void testMountEquation2_B() {
		Main main = new Main();
		String result = main.mountEquation("12", "b");
		assertEquals("12", result);
	}
	
	@Test
	public void testSolveEquation2_B() {
		Main main = new Main();
		int result = main.solveEquation("12");
		assertEquals(12, result);
	}
	
	@Test
	public void testMountEquation2_plus() {
		Main main = new Main();
		String result = main.mountEquation("12", "+");
		assertEquals("1+2", result);
	}
	
	@Test
	public void testSolveEquation2_plus() {
		Main main = new Main();
		int result = main.solveEquation("1+2");
		assertEquals(3, result);
	}
	
	@Test
	public void testMountEquation2_minus() {
		
		Main main = new Main();
		String result = main.mountEquation("12", "-");
		assertEquals("1-2", result);
		
	}
	
	@Test
	public void testSolveEquation2_minus() {
		
		Main main = new Main();
		int result = main.solveEquation("1-2");
		assertEquals(-1, result);
		
	}
	
	@Test
	public void testMountEquationBig_B() {
		
		Main main = new Main();
		String result = main.mountEquation("123456789", "bbbbbbbb");
		assertEquals("123456789", result);
		
	}
	
	@Test
	public void testSolveEquationBig_B() {
		
		Main main = new Main();
		int result = main.solveEquation("123456789");
		assertEquals(123456789, result);
		
	}
	
	@Test
	public void testSolveEquationBig_Mixed() {
		
		Main main = new Main();
		int result = main.solveEquation("123-4+5+6-789");
		assertEquals(-659, result);
		
	}
	
	@Test
	public void testIsUglyTrue() {
		Main main = new Main();
		assertTrue(main.isUgly(9));
		assertTrue(main.isUgly(14));
		assertTrue(main.isUgly(39));
		assertTrue(main.isUgly(-14));
		assertTrue(main.isUgly(-39));
		assertTrue(main.isUgly(0));

	}
	
	@Test
	public void testIsUglyFalse() {
		Main main = new Main();
		assertFalse(main.isUgly(13));
		assertFalse(main.isUgly(121));
	}

}
