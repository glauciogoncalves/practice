package ggoncalves.codeeval.hard.discountoffers;

import static org.junit.Assert.*;

import org.junit.Test;

public class DiscountOfferMainTest {

	// Jack Abraham,John Evans,Ted Dziuba

//	@Test
	public void testCountVowels() {
		Main main = new Main();
		int n = main.countVowels("Jack Abraham");
		assertEquals(4, n);

		n = main.countVowels("John Evans");
		assertEquals(3, n);

		n = main.countVowels("Ted Dziuba");
		assertEquals(4, n);
		
		n = main.countVowels("iPad 2 - 4-pack");
		assertEquals(3, n);
		
		n = main.countVowels("Half & Half,Colt M1911A1");
		assertEquals(4, n);
	}

//	@Test
	public void testCountConsonants() {
		Main main = new Main();
		int n = main.countConsonants("Jack Abraham");
		assertEquals(7, n);

		n = main.countConsonants("John Evans");
		assertEquals(6, n);

		n = main.countConsonants("Ted Dziuba");
		assertEquals(5, n);
		
		n = main.countConsonants("iPad 2 - 4-pack");
		assertEquals(5, n);
		
		n = main.countConsonants("Half & Half,Colt M1911A1");
		assertEquals(10, n);
	}

	@Test
	public void testDiscountOffer1() {
		Main main = new Main();
		String result = main
		    .discountOffer("Jack Abraham,John Evans,Ted Dziuba;iPad 2 - 4-pack,Girl Scouts Thin Mints,Nerf Crossbow");
		assertEquals("21.00", result);
	}
	
	@Test
	public void testDiscountOffer2() {
		Main main = new Main();
		String result = main
				.discountOffer("Jeffery Lebowski,Walter Sobchak,Theodore Donald Kerabatsos,Peter Gibbons,Michael Bolton,Samir Nagheenanajar;Half & Half,Colt M1911A1,16lb bowling ball,Red Swingline Stapler,Printer paper,Vibe Magazine Subscriptions - 40 pack");
		assertEquals("83.50", result);
	}
	
//	@Test
	public void testDiscountOffer3() {
		Main main = new Main();
		String result = main
				.discountOffer("jkhij, biuh, hui, Jareau Wade, Rob Eroh,Mahmoud Abdelkader,Wenyi Cai,Justin Van Winkle,Gabriel Sinkin,Aaron Adelson;Batman No. 1,Football - Official Size,Bass Amplifying Headphones,Elephant food - 1024 lbs,Three Wolf One Moon T-shirt,Dom Perignon 2000 Vintage, hug, hju");
		assertEquals("71.25", result);
	}
	
//	@Test
	public void testDiscountOffer4() {
		Main main = new Main();
		
		String all = "Jeffery Lebowski,Walter Sobchak,Theodore Donald Kerabatsos,Peter Gibbons,Michael Bolton,Samir Nagheenanajar;Half & Half,Colt M1911A1,16lb bowling ball,Red Swingline Stapler,Printer paper,Vibe Magazine Subscriptions - 40 pack, hjjbjh";
		
		String[] splitSemiColon = all.split(";");

		// Customers.
		String[] customers = splitSemiColon[0].trim().split(",");
		String[] products = splitSemiColon[1].trim().split(",");
		
		for (String customer : customers) {
			for (String product : products) {
				double result = main.calculateSuitabilityScore(customer, product);
				
				System.out.println(customer + " - " + product + " has score: " + result);
				
			}
						
		}
		
	}
	
	public void testDummy() {
		String s = "dsifYUYVj*-(ifYUGYUGdi ";
		s = s.toLowerCase().replaceAll("[^a-z]", "");
		s = s.toLowerCase().replaceAll("[aeiou]", "");
		System.out.println(s);
	}
}
