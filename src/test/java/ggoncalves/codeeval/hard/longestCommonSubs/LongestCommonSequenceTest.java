package ggoncalves.codeeval.hard.longestCommonSubs;

import static org.junit.Assert.*;

import org.junit.Test;

public class LongestCommonSequenceTest {

	@Test
	public void testSite1() {
		Main main = new Main();
		String result = main.printLongest("XMJYAUZ;MZJAWXU");
		assertEquals("MJAU", result);
	}

}
