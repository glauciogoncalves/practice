package ggoncalves.codeeval.moderate.passtriangle;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class PassTriangleTest {

	@Test
	public void testPassTriangleOne() {
		PassTriangle pt = new PassTriangle();
		
		ArrayList<Integer> lastLineList = new ArrayList<Integer>();
		lastLineList.add(1);
		
		ArrayList<Integer> lineList = new ArrayList<Integer>();
		lineList.add(5);
		lineList.add(6);
		
		ArrayList<Integer> result = pt.passTriangle(lastLineList, lineList);
		
		assertEquals(6, result.get(0).intValue());
		assertEquals(7, result.get(1).intValue());
	}
	
	@Test
	public void testPassTriangleTwo() {
		PassTriangle pt = new PassTriangle();
		
		ArrayList<Integer> lastLineList = new ArrayList<Integer>();
		lastLineList.add(5);
		lastLineList.add(6);
		
		ArrayList<Integer> lineList = new ArrayList<Integer>();
		lineList.add(8);
		lineList.add(9);
		lineList.add(10);
		
		//  5  6
		// 8  9  10
		
		// 13 15 16
		ArrayList<Integer> result = pt.passTriangle(lastLineList, lineList);
		
		assertEquals(13, result.get(0).intValue());
		assertEquals(15, result.get(1).intValue());
		assertEquals(16, result.get(2).intValue());
	}
	
	@Test
	public void testPassTriangleThree() {
		PassTriangle pt = new PassTriangle();
		
		ArrayList<Integer> lastLineList = new ArrayList<Integer>();
		lastLineList.add(5);
		lastLineList.add(6);
		lastLineList.add(8);
		
		ArrayList<Integer> lineList = new ArrayList<Integer>();
		lineList.add(8);
		lineList.add(9);
		lineList.add(10);
		lineList.add(11);
		
		//  5   6   8
		// 8  9  10  11
		
		// 13 15 18 19
		ArrayList<Integer> result = pt.passTriangle(lastLineList, lineList);
		
		assertEquals(13, result.get(0).intValue());
		assertEquals(15, result.get(1).intValue());
		assertEquals(18, result.get(2).intValue());
		assertEquals(19, result.get(3).intValue());
	}

}
