package ggoncalves.codeeval.moderate.reverseandadd;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReverseAndAddTest {

	public void assertReverse(int original, int expected) {
		ReverseAndAdd raa = new ReverseAndAdd();
		int result = raa.reverse(original);
		assertEquals(expected, result);
	}

	@Test
	public void testReverse() {
		assertReverse(4566, 6654);
	}

	@Test
	public void testReverseSingle() {
		assertReverse(4, 4);
	}

	@Test
	public void testReverseOdd() {
		assertReverse(43424, 42434);
	}

	@Test
	public void testIsPalindromeTrue() {
		assertTrue(new ReverseAndAdd().isPalindrome(121));
	}

	@Test
	public void testIsPalindromeTrue2() {
		assertTrue(new ReverseAndAdd().isPalindrome(1));
	}

	@Test
	public void testIsPalindromeTrue3() {
		assertTrue(new ReverseAndAdd().isPalindrome(45454));
	}

	@Test
	public void testIsPalindromeTrue4() {
		assertTrue(new ReverseAndAdd().isPalindrome(454454));
	}
	
	@Test
	public void testIsPalindromeTrue5() {
		assertTrue(new ReverseAndAdd().isPalindrome(78633687));
	}
	
	@Test
	public void testReverseAndAdd() {
		ReverseAndAdd raa = new ReverseAndAdd();
		String result = raa.reverseAndAdd("195");
		assertEquals("4 9339", result);
	}

}
