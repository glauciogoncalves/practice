package ggoncalves.codeeval.moderate.lowestcommonancestor;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ggoncalves on 22/06/15.
 */
public class LowestCommonAncestorTest {

    public void assertLowestCommonAncestor(String expected, String line) {
        LowestCommonAncestor lca = new LowestCommonAncestor();
        String result = lca.lowestCommonAncestor(line);
        assertEquals(expected, result);
    }

    @Test
    public void testLowestCommonAncestor_3_29() {
        assertLowestCommonAncestor("8", "3 29");
    }

    @Test
    public void testLowestCommonAncestor_3_20() {
        assertLowestCommonAncestor("8", "3 20");
    }

    @Test
    public void testLowestCommonAncestor_8_52() {
        assertLowestCommonAncestor("30", "8 52");
    }

    @Test
    public void testLowestCommonAncestor_52_8() {
        assertLowestCommonAncestor("30", "52 8");
    }

    @Test
    public void testLowestCommonAncestor_8_20() {
        assertLowestCommonAncestor("8", "8 20");
    }

    @Test
    public void testLowestCommonAncestor_10_20() {
        assertLowestCommonAncestor("20", "10 20");
    }

    @Test
    public void testLowestCommonAncestor_10_29() {
        assertLowestCommonAncestor("20", "10 29");
    }

    @Test
    public void testLowestCommonAncestor_20_29() {
        assertLowestCommonAncestor("20", "20 29");
    }

    @Test
    public void testLowestCommonAncestor_3_10() {
        assertLowestCommonAncestor("8", "3 10");
    }

    @Test
    public void testLowestCommonAncestor_8_10() {
        assertLowestCommonAncestor("8", "8 10");
    }

    @Test
    public void testLowestCommonAncestor_8_30() {
        assertLowestCommonAncestor("30", "8 30");
    }

    @Test
    public void testLowestCommonAncestor_52_30() {
        assertLowestCommonAncestor("30", "52 30");
    }

    @Test
    public void testLowestCommonAncestor_52_29() {
        assertLowestCommonAncestor("30", "52 29");
    }

    @Test
    public void testLowestCommonAncestor_8_8() {
        assertLowestCommonAncestor("8", "8 8");
    }

    @Test
    public void testLowestCommonAncestor_10_10() {
        assertLowestCommonAncestor("10", "10 10");
    }

    @Test
    public void testLowestCommonAncestor_10_30() {
        assertLowestCommonAncestor("30", "10 30");
    }

    @Test
    public void testLowestCommonAncestor_30_30() {
        assertLowestCommonAncestor("30", "30 30");
    }

    @Test
    public void testLowestCommonAncestor_1_45() {
        assertLowestCommonAncestor("", "1 45");
    }

    @Test
    public void testLowestCommonAncestor_1_30() {
        assertLowestCommonAncestor("", "1 30");
    }

    @Test
    public void testLowestCommonAncestor_52_52() {
        assertLowestCommonAncestor("52", "52 52");
    }

    @Test
    public void testLowestCommonAncestor_8_29() {
        assertLowestCommonAncestor("8", "8 29");
    }
}