package ggoncalves.codeeval.moderate.consecutiveprimes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ConsecutivePrimesTest {

	@Test
	public void testIsPrime() {
		Main cp = new Main();
		assertTrue(cp.isPrime(5));
		assertTrue(cp.isPrime(3));
		assertTrue(cp.isPrime(7));
		assertTrue(cp.isPrime(11));
		assertTrue(cp.isPrime(13));
	}
	
	@Test
	public void testIsPrimeFalse() {
		Main cp = new Main();
		assertFalse(cp.isPrime(2));
		assertFalse(cp.isPrime(4));
		assertFalse(cp.isPrime(9));
		assertFalse(cp.isPrime(18));
		assertFalse(cp.isPrime(21));
	}
	
	@Test
	public void testConsecutivePrimesOdd() {
		Main cp = new Main();
		String result = cp.consecutivePrimes("5");
		assertEquals("0", result);
	}
	
	@Test
	public void testConsecutivePrimes4() {
		Main cp = new Main();
		String result = cp.consecutivePrimes("4");
		assertEquals("2", result);
	}
	
	@Test
	public void testConsecutivePrimes2() {
		Main cp = new Main();
		String result = cp.consecutivePrimes("2");
		assertEquals("1", result);
	}
	
	public void testPermutation() {
		Main cp = new Main();
		List<String> list = new ArrayList<String>();
		cp.permutation(list, "", "123456789098765443");
		
		for (String string : list) {
	    System.out.println(string);
    }
	}
	
	

}
