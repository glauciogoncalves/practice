package ggoncalves.codeeval.moderate.largestprimepalindrome;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrimeTest {

	@Test
	public void testPalidromeOne() {
		Main main = new Main();
		assertTrue(main.isPalidrome(2));
	}
	
	@Test
	public void testPalidromeTwo() {
		Main main = new Main();
		assertTrue(main.isPalidrome(22));
	}
	
	@Test
	public void testPalidromeTwoFalse() {
		Main main = new Main();
		assertFalse(main.isPalidrome(23));
	}
	
	@Test
	public void testPalidromeTreeFalse() {
		Main main = new Main();
		assertFalse(main.isPalidrome(523));
	}
	
	@Test
	public void testPalidromeTreeTrue() {
		Main main = new Main();
		assertTrue(main.isPalidrome(323));
	}

}
