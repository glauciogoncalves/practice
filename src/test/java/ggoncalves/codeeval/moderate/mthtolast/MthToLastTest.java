package ggoncalves.codeeval.moderate.mthtolast;

import static org.junit.Assert.*;

import org.junit.Test;

public class MthToLastTest {

	// a b c d 4
	// a
	@Test
	public void testSiteSample1() {
		Main main = new Main();
		String result = main.mthToLast("a b c d 4");
		assertEquals("a", result);
	}
	
	// e f g h 2
	// g
	@Test
	public void testSiteSample2() {
		Main main = new Main();
		String result = main.mthToLast("e f g h 2");
		assertEquals("g", result);
	}
	
	// e f g h 2
	// g
	@Test
	public void testSiteLast2() {
		Main main = new Main();
		String result = main.mthToLast("e e d a f g h 2");
		assertEquals("g", result);
	}
	
	@Test
	public void testEmpty() {
		Main main = new Main();
		String result = main.mthToLast("");
		assertEquals("", result);
	}
	
	@Test
	public void testOneExactly() {
		Main main = new Main();
		String result = main.mthToLast("e 1");
		assertEquals("e", result);
	}
	
	@Test
	public void testOneNext() {
		Main main = new Main();
		String result = main.mthToLast("e 1");
		assertEquals("e", result);
	}
	
	@Test
	public void testOneSeveral() {
		Main main = new Main();
		String result = main.mthToLast("e 15");
		assertEquals("", result);
	}
	
	@Test
	public void testOneSeveralTwo() {
		Main main = new Main();
		String result = main.mthToLast("e 2");
		assertEquals("", result);
	}
	
	@Test
	public void testTwoExactly() {
		Main main = new Main();
		String result = main.mthToLast("e a 1");
		assertEquals("a", result);
	}
	
	@Test
	public void testTwoExactly2() {
		Main main = new Main();
		String result = main.mthToLast("e a 1");
		assertEquals("a", result);
	}
	
	@Test
	public void testTwoSeveral1() {
		Main main = new Main();
		String result = main.mthToLast("e a 15");
		assertEquals("", result);
	}
	
	@Test
	public void testTwoSeveral2() {
		Main main = new Main();
		String result = main.mthToLast("e a 14");
		assertEquals("", result);
	}

}
