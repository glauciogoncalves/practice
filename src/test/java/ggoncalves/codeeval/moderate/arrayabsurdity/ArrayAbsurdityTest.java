package ggoncalves.codeeval.moderate.arrayabsurdity;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ggoncalves on 28/06/15.
 */
public class ArrayAbsurdityTest {

    @Test
    public void testArrayAbsurdityEmptyCase() throws Exception {
        ArrayAbsurdity aa = new ArrayAbsurdity();
        String result = aa.arrayAbsurdity("");
        assertTrue(result.isEmpty());
    }

    @Test
    public void testArrayAbsurditySiteCase1() throws Exception {
        ArrayAbsurdity aa = new ArrayAbsurdity();
        String result = aa.arrayAbsurdity("5;0,1,2,3,0");
        assertEquals("0", result);
    }

    @Test
    public void testArrayAbsurditySiteCase2() throws Exception {
        ArrayAbsurdity aa = new ArrayAbsurdity();
        String result = aa.arrayAbsurdity("20;0,1,10,3,2,4,5,7,6,8,11,9,15,12,13,4,16,18,17,14");
        assertEquals("4", result);
    }
}