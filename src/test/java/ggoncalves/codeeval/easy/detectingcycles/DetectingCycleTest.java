package ggoncalves.codeeval.easy.detectingcycles;

import static org.junit.Assert.*;
import ggoncalves.codeeval.easy.detectingcycles.Main;

import org.junit.Test;

public class DetectingCycleTest {

	// 2 0 6 3 1 6 3 1 6 3 1
	@Test
	public void testSiteCase1() throws Exception {
		Main fbMain = new Main();
		String result = fbMain.detectCycle("2 0 6 3 1 6 3 1 6 3 1");
		assertEquals("6 3 1", result);
	}
	
	// 3 4 8 0 11 9 7 2 5 6 10 1 49 49 49 49
	@Test
	public void testSiteCase2() throws Exception {
		Main fbMain = new Main();
		String result = fbMain.detectCycle("3 4 8 0 11 9 7 2 5 6 10 1 49 49 49 49");
		assertEquals("49", result);
	}
	
	// 1 2 3 1 2 3 1 2 3
	@Test
	public void testSiteCase3() throws Exception {
		Main fbMain = new Main();
		String result = fbMain.detectCycle("1 2 3 1 2 3 1 2 3");
		assertEquals("1 2 3", result);
	}


}
