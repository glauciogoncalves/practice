package ggoncalves.codeeval.easy.fizzbuzz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class FizzBuzzTest {

	
	
	// 3 5 10
	@Test
	public void testSiteCase1() throws Exception {
		Main fbMain = new Main();
		String result = fbMain.fizzBuzz("3 5 10");
		assertEquals("1 2 F 4 B F 7 8 F B", result);
	}
	
	// 2 7 15
	@Test
	public void testSiteCase2() {
		Main fbMain = new Main();
		String result = fbMain.fizzBuzz("2 7 15");
		assertEquals("1 F 3 F 5 F B F 9 F 11 F 13 FB 15", result);
	}

}
