package ggoncalves.others.kthlargestelementstream;

import org.junit.jupiter.api.Test;

import java.util.PriorityQueue;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class KthLargestElementStream {

  class KthLargest {

    // private Queue<Integer> lower = new PriorityQueue<>((a,b) -> Integer.compare(b,a));
    private final Queue<Integer> lower = new PriorityQueue<>();
    // private Queue<Integer> higher = new PriorityQueue<>();
    private final int k;

    public KthLargest(int k, int[] nums) {
      this.k = k;
      for (int num : nums) offer(num);
    }

    public int add(int val) {
      offer(val);
      return lower.peek();
    }

    private void offer(int val) {
      if (lower.size() < k) lower.offer(val);
      else {
        if (val > lower.peek()) {
          lower.poll();
          lower.offer(val);
        }
      }
      // if (lower.size() > k) lower.poll();
      // lower.offer(val);
    }
  }

  @Test
  void leetCodetest() {
    KthLargest k = new KthLargest(3, new int[]{4, 5, 8, 2});
    assertEquals(4, k.add(3));
    assertEquals(5, k.add(5));
    assertEquals(5, k.add(10));
  }
}
