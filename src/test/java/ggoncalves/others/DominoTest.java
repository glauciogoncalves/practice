package ggoncalves.others;

import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DominoTest {

  @Test
  public void testNotValidArrangement() {
    Domino domino = new Domino();

    List<Pair<Integer, Integer>> arrangementList = new ArrayList<>();
    arrangementList.add(new Pair<>(2, 3));
    arrangementList.add(new Pair<>(4, 2));
    arrangementList.add(new Pair<>(2, 2));
    arrangementList.add(new Pair<>(3, 5));

    assertFalse(domino.isValidArrangement(arrangementList));
  }

  @Test
  public void testValidArrangement() {
    Domino domino = new Domino();

    List<Pair<Integer, Integer>> arrangementList = new ArrayList<>();
    arrangementList.add(new Pair<>(2, 3));
    arrangementList.add(new Pair<>(3, 5));
    arrangementList.add(new Pair<>(5, 4));
    arrangementList.add(new Pair<>(4, 2));

    assertTrue(domino.isValidArrangement(arrangementList));
  }

  @Test
  public void testValidArrangement2() {
    Domino domino = new Domino();

    List<Pair<Integer, Integer>> arrangementList = new ArrayList<>();
    arrangementList.add(new Pair<>(2, 3));
    arrangementList.add(new Pair<>(3, 5));
    arrangementList.add(new Pair<>(2, 4));
    arrangementList.add(new Pair<>(4, 6));
    arrangementList.add(new Pair<>(4, 5));
    arrangementList.add(new Pair<>(6, 1));

    assertTrue(domino.isValidArrangement(arrangementList));
  }

}