package ggoncalves.others.wordsearch;

import javafx.util.Pair;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class WordSearchTest {

  @Test
  void dive() {
    char[][] board = {{'A', 'B', 'C', 'E'}, {'S', 'F', 'E', 'S'}, {'A', 'D', 'E', 'E'}};
    String word = "ABCESEEEFS";
    assertTrue(exist(board, word));
  }

  public boolean exist(char[][] board, String word) {
    char c = word.charAt(0);
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[i].length; j++) {
        if (board[i][j] == c) {
          if (dive(board, i, j, word, 0, new HashSet<Pair>())) return true;
        }
      }
    }
    return false;
  }

  private boolean dive(char[][] board, int i, int j, String word, int index, Set<Pair> visited) {
    if (i < 0 || j < 0 || i >= board.length || j >= board[i].length) return false;
    if (isVisited(visited, i, j)) return false;
    char c = word.charAt(index);
    if (board[i][j] == c) {
      if (index == word.length() - 1) return true;
      index++;
      Pair p = new Pair(i, j);
      visited.add(p);
      if (dive(board, i - 1, j, word, index, visited) ||
          dive(board, i, j - 1, word, index, visited) ||
          dive(board, i + 1, j, word, index, visited) ||
          dive(board, i, j + 1, word, index, visited)) return true;
      visited.remove(p);
      return false;

    }
    return false;
  }

  private boolean isVisited(Set<Pair> visited, int i, int j) {
    return visited.contains(new Pair(i, j));
  }
}
