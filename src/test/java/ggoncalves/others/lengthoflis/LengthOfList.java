package ggoncalves.others.lengthoflis;

import java.util.ArrayList;
import java.util.List;

public class LengthOfList {

  public int lengthOfLIS(int[] nums) {
    List<Integer> list = new ArrayList<>(nums.length);

    list.add(nums[0]);

    for (int i = 1; i < nums.length; i++) {

      int index = binarySearch(list, nums[i], 0, list.size());
      if (index == list.size()) list.add(nums[i]);
      else if (list.get(index) < nums[i]) list.set(index + 1, nums[i]);
      else list.set(index, nums[i]);

    }

    return list.size();
  }

  private int binarySearch(List<Integer> list, int num, int lo, int hi) {
    if (lo >= hi) return lo;
    int mid = lo + (hi - lo) / 2;
    int comp = Integer.compare(num, list.get(mid));
    if (comp > 0) return binarySearch(list, num, mid + 1, hi);
    else if (comp < 0) return binarySearch(list, num, lo, mid - 1);
    return mid;
  }

  public int lengthOfLIS2(int[] nums) {
    if (nums.length == 1) return 1;

    int[] dp = new int[nums.length];

    for (int i = 0; i < dp.length; i++) dp[i] = 1;

    int lo = 0, hi = lo + 1;

    int high = 0;

    while (hi < dp.length) {

      if (lo == hi) {
        lo = 0;
        hi++;
        continue;
      }

      if (nums[hi] > nums[lo]) {
        dp[hi] = Math.max(dp[hi], dp[lo] + 1);
        high = Math.max(high, dp[hi]);
      }
      lo++;

    }
    return high;
  }

  public static void main(String[] args) {
//    int[] r = new int[] {0,1,0,3,2,3};
    int[] r = new int[]{10, 9, 2, 5, 3, 4};
    System.out.println("Result: " + new LengthOfList().lengthOfLIS(r));
  }
}
