package ggoncalves.others.ispalindrome;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class IsPalindromeTest {

  @Test
  void palimdrome() {
    assertTrue(isPalindrome(
        121));
  }

  public boolean isPalindrome(int x) {
    if (x < 0) return false;
    if (x < 10) return true;
//    int reverseNumber = reverse(x);
    int revertedNumber = 0;
    while (x > revertedNumber) {
      revertedNumber = revertedNumber * 10 + x % 10;
      x /= 10;
    }
    return x == revertedNumber || x == revertedNumber / 10;
  }

  private int reverse(int x) {
    int revertedNumber = 0;
    while (x > revertedNumber) {
      revertedNumber = revertedNumber * 10 + x % 10;
      x /= 10;
    }
    return revertedNumber;
  }
}
