package ggoncalves.others.findpythagoreantriplets;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FindPythagoreanTripletsTest {


  @Test
  void find() {
    PythagoreanTriplets p = new PythagoreanTriplets();
    List<int[]> pythagoreanTriplets = p.findPythagoreanTriplets(new int[]{4, 16, 1, 2, 3, 5, 6, 8, 25, 10});

    StringBuilder sb = new StringBuilder();

    for (int[] arr : pythagoreanTriplets) {
      for (int n : arr) sb.append(n).append(" ");
      sb.append("\n");
    }

    System.out.println("Result: " + sb.toString());
  }

  class PythagoreanTriplets {
    List<int[]> findPythagoreanTriplets(int[] arr) {
      List<int[]> triplets = new ArrayList<int[]>();
      for (int i = 0; i < arr.length; i++) {
        twoSum(triplets, arr, 0, (int) Math.pow(arr[i], 2));
      }
      return triplets;
    }

    void twoSum(List<int[]> res, int[] arr, int lo, int target) {

      Set<Integer> set = new HashSet<>();

      for (int i = lo; i < arr.length; i++) {
        int num = arr[i];
        int numPow = (int) Math.pow(num, 2);
        if (set.contains(target - numPow)) {
          int min = Math.min(numPow, target - numPow);
          int max = Math.max(numPow, target - numPow);
          int[] triplet = {(int) Math.sqrt(min), (int) Math.sqrt(max), (int) Math.sqrt(target)};
          res.add(triplet);
        } else {
          set.add(numPow);
        }
      }

    }
  }
}
