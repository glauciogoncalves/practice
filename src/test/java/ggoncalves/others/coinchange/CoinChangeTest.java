package ggoncalves.others.coinchange;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CoinChangeTest {

  @Test
  void coinchange() {
    int[] coins = {18, 41, 8, 4};
    assertEquals(7, coinChange(coins, 125));
  }

  public int coinChange(int[] coins, int amount) {
    if (amount == 0) return 0;

    //     1  2  3  4  5  6  7  8  9  10  11

    int[] dp = new int[amount + 1];
    Arrays.fill(dp, amount + 1);
    dp[0] = 0;

    // iterate over possible amount
    for (int i = 1; i <= amount; i++) {

      // iterate over coins
      for (int j = 0; j < coins.length; j++) {

        if (coins[j] <= i) {
          dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
        }
      }

    }

    return (dp[amount] > amount) ? -1 : dp[amount];
  }
}
