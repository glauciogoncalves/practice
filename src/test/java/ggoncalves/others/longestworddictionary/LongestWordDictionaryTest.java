package ggoncalves.others.longestworddictionary;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongestWordDictionaryTest {

  public String longestWord(String[] words) {
    Set<String> dictSet = Arrays.stream(words).collect(Collectors.toSet());
    Arrays.sort(words, (a, b) -> {
      if (a.length() != b.length()) return -1 * Integer.compare(a.length(), b.length());
      return a.compareTo(b);
    });
    for (String w : words) {
      if (isValid(dictSet, w)) return w;
    }
    return "";
  }

  private boolean isValid(Set<String> dictSet, String w) {
    if (w.length() == 1) return dictSet.contains(w);
    if (!dictSet.contains(w)) return false;
    return isValid(dictSet, w.substring(0, w.length() - 1));
  }

  @Test
  void world() {
    String[] s = {"w", "wo", "wor", "worl", "world"};
    assertEquals("world", longestWord(s));
  }
}
