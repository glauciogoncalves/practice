package ggoncalves.others.numberofislands;

import java.util.HashSet;
import java.util.Set;

class NumberOfIslands {

  class UnionFind {

    int[] ids;
    int[] size;
    int union = 0;

    public UnionFind(int n) {
      ids = new int[n];
      size = new int[n];
      for (int i = 0; i < n; i++) {
        ids[i] = -1 * i;
        size[i] = 1;
      }
    }

    public int find(int i) {
      i = Math.abs(i);
      while (i != Math.abs(ids[i])) {
        ids[i] = Math.abs(ids[Math.abs(ids[i])]);
        i = Math.abs(ids[i]);
      }
      return i;
    }

    public boolean isConnected(int i, int j) {
      return find(i) == find(j);
    }

    public void union(int p, int q) {
      int i = find(p);
      int j = find(q);

      if (i == j) return;
      union++;

      if (size[i] < size[j]) {
        ids[i] = j;
        size[j] += size[i];
      } else {
        ids[j] = i;
        size[i] += size[j];
      }

    }

    public void markIsland(int i) {
      union++;
      if (ids[i] < 0) {
        ids[i] = -1 * ids[i];
      }
    }

    public int distinct() {
      if (union == 0) return 0;
      Set<Integer> set = new HashSet<>();
      for (int i = 0; i < ids.length; i++) {
        System.out.println(ids[i]);
        if (ids[i] > 0) set.add(find(ids[i]));
      }
      return set.size();
    }

  }

  private int g2i(char[][] grid, int i, int j) {
    return (i * grid[i].length + j + 1);
  }

  public int numIslands(char[][] grid) {
    UnionFind uf = new UnionFind((grid.length * grid[0].length) + 1);

    for (int i = 0; i < grid.length; i++) {

      for (int j = 0; j < grid[0].length; j++) {

        if (grid[i][j] == '0') continue;

        uf.markIsland(g2i(grid, i, j));

        // 2 cases (compare to righ and bottom)
        if (j < grid[0].length - 1) {
          if (grid[i][j + 1] == '1') uf.union(g2i(grid, i, j), g2i(grid, i, j + 1));
        }
        if (i < grid.length - 1) {
          if (grid[i + 1][j] == '1') uf.union(g2i(grid, i, j), g2i(grid, i + 1, j));
        }

      }

    }
    return uf.distinct();
  }

  public static void main(String[] args) {
    char[][] grid = {
        {'1', '0', '1', '1', '1'},
        {'1', '0', '1', '0', '1'},
        {'1', '1', '1', '0', '1'}};

    NumberOfIslands noi = new NumberOfIslands();
    System.out.println("Result: " + noi.numIslands(grid));
  }
}