package ggoncalves.others.maxAreaIsland;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MaxAreaIslandTest {

  @Test
  void maxAreaOfIsland() {
    int[][] grid = {{0, 1}};
    assertEquals(1, maxAreaOfIsland(grid));
  }

  public int maxAreaOfIsland(int[][] grid) {

    int maxArea = 0;
    for (int i = 0; i < grid.length; i++) {
      for (int j = 0; j < grid[i].length; j++) {
        if (grid[i][j] == 1) {
          int area = eraseIsland(grid, i, j, 0);
          maxArea = Math.max(maxArea, area);
        }
      }
    }
    return maxArea;
  }

  private int eraseIsland(int[][] grid, int i, int j, int area) {
    if (i < 0 || j < 0 || i > grid.length - 1 || j > grid[i].length - 1) return area;

    if (grid[i][j] == 0) return area;

    area += 1;
    grid[i][j] = 0;

    area += eraseIsland(grid, i - 1, j, 0);
    area += eraseIsland(grid, i, j - 1, 0);
    area += eraseIsland(grid, i + 1, j, 0);
    area += eraseIsland(grid, i, j + 1, 0);

    return area;

  }
}
