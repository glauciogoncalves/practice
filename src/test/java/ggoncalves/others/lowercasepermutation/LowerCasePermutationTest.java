package ggoncalves.others.lowercasepermutation;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LowerCasePermutationTest {

  @Test
  void lowercase() {
    List<String> list = letterCasePermutation("a1b2");
    for (String s : list) System.out.println(s);
  }

  public List<String> letterCasePermutation(String S) {
    List<String> res = new ArrayList<>();
    perm(res, S.toCharArray(), 0, new StringBuilder());
    return res;
  }

  private void perm(List<String> res, char[] arr, int index, StringBuilder sb) {
    if (sb.length() == arr.length) {
      res.add(sb.toString());
      return;
    }
    char c = arr[index];
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
      sb.append(c);
      perm(res, arr, index + 1, sb);
      sb.setLength(sb.length() - 1);
      char newC = (Character.isLowerCase(c)) ? Character.toUpperCase(c) : Character.toLowerCase(c);
      sb.append(newC);
      perm(res, arr, index + 1, sb);
      sb.setLength(sb.length() - 1);
    } else {
      sb.append(c);
      perm(res, arr, index + 1, sb);
      sb.setLength(sb.length() - 1);
    }
  }
}
