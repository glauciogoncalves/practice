package ggoncalves.sedgewick.helpers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayHelperTest {

  @Test
  void isLess() {
    Integer[] arr = {2, 3};
    assertTrue(ArrayHelper.isLess(arr, 0, 1));
    assertFalse(ArrayHelper.isLess(arr, 1, 0));
  }

  @Test
  void exchange() {
    Integer[] arr = {2, 3};

    ArrayHelper.exchange(arr, 0, 1);

    Integer[] expectedArr = {3, 2};
    assertArrayEquals(arr, expectedArr);
  }

  @Test
  void exchangeSame() {
    Integer[] arr = {2, 3};

    ArrayHelper.exchange(arr, 0, 0);

    Integer[] expectedArr = {2, 3};
    assertArrayEquals(arr, expectedArr);
  }
}