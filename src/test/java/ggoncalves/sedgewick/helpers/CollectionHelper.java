package ggoncalves.sedgewick.helpers;

import java.util.Collection;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class CollectionHelper {

  public static <E> void assertCollectionContains(Collection<E> c, E... elements) {
    assertEquals(elements.length, c.size());

    Iterator<E> iterator = c.iterator();
    for (int i = 0; i < elements.length; i++) {
      if (!elements[i].equals(iterator.next())) {
        String list1 = getListString(c);
        String list2 = getListString(elements);
        fail(String.format("Lists are not equal: \n%s\n%s", list1, list2));
      }
    }
  }

  private static <E> String getListString(E... elements) {

    StringBuilder sb = new StringBuilder();
    sb.append("[ ");
    for (int i = 0; i < elements.length; i++) {
      sb.append(" ").append(elements[i]).append(",");
    }
    sb.substring(0, sb.length() - 1);
    sb.append(" ]");

    return sb.toString();

  }
}
