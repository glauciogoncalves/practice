package ggoncalves.sedgewick.helpers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeapHelperTest {

  @Test
  void isHeapfiedNPE() {
    assertThrows(NullPointerException.class, () -> HeapHelper.isHeapfied((Comparable[]) null));
  }

  @Test
  void isHeapfiedSingleNotHeap() {
    Integer[] arr = {1};
    assertFalse(HeapHelper.isHeapfied(arr));
  }

  @Test
  void isHeapfiedSingle() {
    Integer[] arr = {0, 1};
    assertTrue(HeapHelper.isHeapfied(arr));
  }

  @Test
  void isHeapfied() {
    Integer[] arr = {0, 70, 56, 62, 44, 48, 61, 59};
    assertTrue(HeapHelper.isHeapfied(arr));
  }

  @Test
  void isHeapfiedWithNull() {
    Integer[] arr = {0, 70, 56, 62, 44, 48, 61, 59, null, null, 10, 12};
    assertTrue(HeapHelper.isHeapfied(arr));
  }

  @Test
  void isHeapfiedFalse1() {
    Integer[] arr = {0, 70, 56, 62, 44, 48, 61, 59, null, null, 10, 72};
    assertFalse(HeapHelper.isHeapfied(arr));
  }

  @Test
  void isHeapfiedFalse2() {
    Integer[] arr = {0, 70, 56, 72, 44, 48, 61, 59, null, null, 10, 12};
    assertFalse(HeapHelper.isHeapfied(arr));
  }
}