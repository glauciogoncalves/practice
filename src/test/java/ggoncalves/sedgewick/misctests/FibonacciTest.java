package ggoncalves.sedgewick.misctests;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class FibonacciTest {

  Map<Integer, Integer> memoization = new HashMap<>();

  int getFibonacci(int n) {
    if (n == 1) return 0;
    if (n == 2 || n == 3) return 1;
    Integer result = memoization.get(n);
    if (result != null) return result;
    result = getFibonacci(n - 1) + getFibonacci(n - 2);
    memoization.put(n, result);
    return result;
  }

  @Test
  void getFibonacci() {
    System.out.println(getFibonacci(40));
  }
}
