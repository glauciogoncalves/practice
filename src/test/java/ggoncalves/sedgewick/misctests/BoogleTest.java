package ggoncalves.sedgewick.misctests;

import java.util.*;

public class BoogleTest {

  class Pair {
    Integer first;
    Integer second;

    public Pair(Integer first, Integer second) {
      this.first = first;
      this.second = second;
    }
  }

  Set<String> visited = new HashSet<>();
  private final char[][] grid;
  private final Set<String> dictionary;
  private final Map<Character, List<Pair>> mapPositions = new HashMap<>();

  public BoogleTest(char[][] grid, HashSet<String> dictionary) {
    //TODO: Write - Your - Code
    this.grid = grid;
    this.dictionary = dictionary;
    initializeMapPositions();
  }

  void initializeMapPositions() {
    for (int i = 0; i < grid.length; i++) {
      for (int j = 0; j < grid.length; j++) {
        char c = grid[i][j];
        List<Pair> positions = mapPositions.get(c);
        if (positions == null) positions = new ArrayList<Pair>();
        positions.add(new Pair(i, j));
        mapPositions.put(c, positions);
      }

    }
  }

  public HashSet<String> findAllWords() {
    //TODO: Write - Your - Code
    HashSet<String> result = new HashSet<String>();
    for (String word : dictionary) {
      char c = word.charAt(0);
      if (!mapPositions.containsKey(c)) continue;
      visited.clear();
      for (Pair pair : mapPositions.get(c)) {
        if (findWord(result, word, 0, pair.first, pair.second)) break;
        visited.clear();
      }
    }
    return result;
  }

  boolean findWord(HashSet<String> result, String currWord, int d, int i, int j) {
    if (currWord.length() == d) return false;
    char c = currWord.charAt(d);
    if (grid[i][j] != c) return false;
    if (currWord.length() - 1 == d) {
      result.add(currWord);
      return true;
    }
    visit(i, j);
    d++;
    c = currWord.charAt(d);
    if (!mapPositions.containsKey(c)) return false;
    for (Pair pair : mapPositions.get(c)) {
      if (!isVisited(pair.first, pair.second) && isAdjacent(i, j, pair.first, pair.second)) {
        if (findWord(result, currWord, d, pair.first, pair.second)) return true;
      }
    }
    return false;
  }

  boolean isAdjacent(int i, int j, int i2, int j2) {
    if (i == i2 && j == j2) return false;
    int imax = Math.max(i, i2) - Math.min(i, i2);
    if (imax > 1) return false;
    return Math.max(j, j2) - Math.min(j, j2) < 2;
  }

  void visit(int i, int j) {
    visited.add(getKey(i, j));
  }

  String getKey(int i, int j) {
    return "" + i + "-" + j;
  }

  boolean isVisited(int i, int j) {
    return visited.contains(getKey(i, j));
  }

  public static void main(String[] args) {
    char[][] grid = new char[][]{
        {'c', 'a', 't'},
        {'r', 'r', 'e'},
        {'t', 'o', 'n'}
    };

    String[] dict = {"cat", "cater", "cartoon", "art", "toon", "moon", "eat", "ton"};
    HashSet<String> dictionary = new HashSet<String>();
    dictionary.addAll(Arrays.asList(dict));

    BoogleTest b = new BoogleTest(grid, dictionary);
    Long start = System.currentTimeMillis();
    HashSet<String> words = b.findAllWords();
    Long end = System.currentTimeMillis() - start;
    System.out.println("Time Elapsed: " + end);
    for (String s : words) {
      System.out.println(s);
    }
  }
}
