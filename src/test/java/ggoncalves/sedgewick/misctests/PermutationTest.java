package ggoncalves.sedgewick.misctests;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PermutationTest {

  @Test
  void permute() {
    List<Character> characterList = new ArrayList<Character>(Arrays.asList(new Character('a'),
        new Character('b'),
        new Character('c'),
        new Character('d')));
    StringBuilder result = new StringBuilder();
    permute(characterList, "", result);
    System.out.println(result.toString());
  }

  @Test
  void permuteKth() {
    List<Character> characterList = new ArrayList<Character>(Arrays.asList(new Character('a'),
        new Character('b'),
        new Character('c'),
        new Character('d'),
        new Character('e'),
        new Character('f')));
    StringBuilder result = new StringBuilder();
    permuteKth(characterList, result, 12);
    System.out.println(result.toString());
  }

  void permute(List<Character> v, String current, StringBuilder result) {
    if (v.isEmpty()) result.append(current).append("\n");
    else {
      for (int i = 0; i < v.size(); i++) {
        char c = v.remove(i);
        permute(v, current + c, result);
        v.add(i, c);
      }
    }
  }

  void permuteKth(List<Character> v, StringBuilder result, int k) {
    // steps
    // block size = (n-1)!
    // (k-1)/block size = ?
    // k = k - (block size)
    while (!v.isEmpty()) {
      int bs = fatorial(v.size() - 1);
      int index = (k - 1) / bs;
      result.append(v.remove(index));
      k = k - (bs * index);
    }
  }

  int fatorial(int n) {
    if (n == 0 || n == 1) return 1;
    return n * fatorial(n - 1);
  }
}
