package ggoncalves.sedgewick.misctests;

import org.junit.jupiter.api.Test;

public class ReverseTempTest {

  @Test
  void test1() {
    ReverseWords r = new ReverseWords();
    String testCase = "The quick brown fox jumped over the lazy dog.";
    char[] array = testCase.toCharArray();
    r.reverseWords(array);

    StringBuilder result = new StringBuilder();

    result.append("[");
    for (char c : array) result.append(c);
    result.append("]");

    System.out.println(result.toString());
  }

  class ReverseWords {
    public void reverseWords(char[] sentence) {
      //TODO: Write - Your - Code
      if (sentence == null || sentence.length <= 2) return;
      reverse(sentence, 0, sentence.length - 1);

      int start = 0;

      while (start < sentence.length) {

        int end = start + 1;
        while (end < sentence.length && sentence[end] != ' ') end++;
        reverse(sentence, start, end - 1);
        start = end + 1;

      }
    }

    private void reverse(char[] sentence, int lo, int hi) {
      if (lo >= hi) return;
      exch(sentence, lo, hi);
      reverse(sentence, lo + 1, hi - 1);
    }

    private void exch(char[] sentence, int i, int j) {
      char aux = sentence[i];
      sentence[i] = sentence[j];
      sentence[j] = aux;
    }
  }
}
