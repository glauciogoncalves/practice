package ggoncalves.sedgewick.misctests;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CharToArrayIndexTest {

  @Test
  void charsAToZLowercase() {
    assertEquals(0, 'a' - 97);
    assertEquals(25, 'z' - 97);
  }

  @Test
  void charsAToZUppercase() {
    assertEquals(0, 'A' - 65);
    assertEquals(25, 'Z' - 65);
  }

  @Test
  void chars0To9() {
    assertEquals(0, '0' - '0');
    assertEquals(9, '9' - '0');
  }

}
