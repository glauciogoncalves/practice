package ggoncalves.sedgewick.misctests;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.PriorityQueue;

public class PriorityQueueTest {

  /**
   * MinHeap is the PriorityQueue as is, in the natural order.
   */
  @Test
  void minHeap() {
    PriorityQueue<Integer> minHeap = new PriorityQueue<>();
    minHeap.offer(4);
    minHeap.offer(10);
    minHeap.offer(1);
    minHeap.offer(-1);
    minHeap.offer(0);
    minHeap.offer(1);
    while (!minHeap.isEmpty()) System.out.println(minHeap.poll());
  }

  /**
   * MaxHeap is the PriorityQueue using a comparator to invert the natural order.
   */
  @Test
  void maxHeap() {
    PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        return -1 * o1.compareTo(o2);
      }
    });
    maxHeap.offer(4);
    maxHeap.offer(10);
    maxHeap.offer(1);
    maxHeap.offer(-1);
    maxHeap.offer(0);
    maxHeap.offer(1);
    while (!maxHeap.isEmpty()) System.out.println(maxHeap.poll());
  }
}
