package ggoncalves.sedgewick.misctests;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UniquePathsIITest {

  @Test
  void uniquePathsWithObstacles() {
    int[][] g = new int[][]{
        {0, 1, 0, 0, 0}, //
        {1, 0, 0, 0, 0}, //
        {0, 0, 0, 0, 0}, //
        {0, 0, 0, 0, 0} //
//        {0, 0, 0}
    };
    assertEquals(0, uniquePathsWithObstacles(g));
  }

  public int uniquePathsWithObstacles(int[][] g) {
    int m = g.length;
    int n = g[0].length;
    int[][] grid = new int[m][n];

    boolean foundObstacle = false;
    for (int j = 0; j < n; j++) {
      if (!foundObstacle) {
        if (g[0][j] == 1) {
          g[0][j] = -1;
          foundObstacle = true;
        } else g[0][j] = 1;
      } else g[0][j] = 0;
    }

    for (int i = 1; i < m; i++) {
      g[i][0] = (g[i][0] == 1) ? -1 : (g[i - 1][0] == -1) ? 0 : g[i - 1][0];
      for (int j = 1; j < n; j++) {
        if (g[i][j] == 1) g[i][j] = -1;
        else {
          if (g[i - 1][j] == -1) g[i][j] = g[i][j - 1];
          else if (g[i][j - 1] == -1) g[i][j] = g[i - 1][j];
          else g[i][j] = g[i - 1][j] + g[i][j - 1];
        }
      }
    }
    if (g[m - 1][n - 1] == -1) return 0;
    return g[m - 1][n - 1];
  }
}
