package ggoncalves.sedgewick.datastructure.trie;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrieTest {

  private Trie<Integer> trie;

  @BeforeEach
  void setUp() {
    trie = new Trie<>();
  }

  @Test
  void constructor() {
    Trie<Integer> t = new Trie<>();
    assertNotNull(t.getRoot());
    for (Trie.Node n : t.getRoot().getNext()) assertNull(n);
  }

  @Test
  void putAndGet() {
    trie.put("by", 4);
    assertEquals(4, trie.get("by"));
  }

  @Test
  void getNull() {
    assertNull(trie.get("by"));
  }

  @Test
  void contains() {
    assertFalse(trie.contains("by"));
    trie.put("by", 4);
    assertTrue(trie.contains("by"));
  }

  @Test
  void delete() {
    assertFalse(trie.contains("by"));
    trie.put("by", 4);
    assertTrue(trie.contains("by"));
    trie.delete("by");
    assertFalse(trie.contains("by"));
  }

  @Test
  void deleteTwo() {
    trie.put("baaaaaayaaaaaa", 4);
    trie.put("baaaaaayaaaaaab", 5);
    trie.delete("baaaaaayaaaaaab");
    assertTrue(trie.contains("baaaaaayaaaaaa"));
    assertFalse(trie.contains("baaaaaayaaaaaab"));
  }
}