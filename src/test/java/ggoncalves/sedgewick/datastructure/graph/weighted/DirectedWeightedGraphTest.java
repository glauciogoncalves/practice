package ggoncalves.sedgewick.datastructure.graph.weighted;

import ggoncalves.sedgewick.datastructure.graph.AbstractGraphTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DirectedWeightedGraphTest extends AbstractGraphTest {

  @Test
  void constructorIllegal() {
    assertThrows(IllegalArgumentException.class, () -> new DirectedWeightedGraph(0));
    assertThrows(IllegalArgumentException.class, () -> new DirectedWeightedGraph(-1));
  }

  @Test
  void constructor() {
    DirectedWeightedGraph graph = new DirectedWeightedGraph(10);
    assertNotNull(graph);
    assertEquals(10, graph.getVerticeSize());
    assertEquals(0, graph.getEdgeSize());
  }

  @Test
  void getVerticeSize() {
    assertEquals(1, new DirectedWeightedGraph(1).getVerticeSize());
    assertEquals(10, new DirectedWeightedGraph(10).getVerticeSize());
    assertEquals(1000, new DirectedWeightedGraph(1000).getVerticeSize());
  }

  @Test
  void getEdgeSizeEmpty() {
    assertEquals(0, new DirectedWeightedGraph(1).getEdgeSize());
  }

  @Test
  void getEdgeSize() {
    DirectedWeightedGraph graph = createDirectedWeightedGraph(10);
    graph.addEdge(new DirectedEdge(0, 1, 3.0));
    graph.addEdge(new DirectedEdge(8, 9, 2.0));
    assertEquals(2, graph.getEdgeSize());
  }

  @Test
  void addEdge() {
    DirectedWeightedGraph graph = createDirectedWeightedGraph(10);
    graph.addEdge(new DirectedEdge(0, 1, 3.0));
    assertEquals(1, graph.getEdgeSize());
    assertAdjVContainsAllW(graph, 0, createCollection(1));
  }

  // Duplicated accepted in weighted graph implementations.
  @Test
  void addEdgeDuplicated() {
    DirectedWeightedGraph graph = createDirectedWeightedGraph(10);
    graph.addEdge(new DirectedEdge(0, 1, 3.0));
    graph.addEdge(new DirectedEdge(1, 0, 3.0));
    graph.addEdge(new DirectedEdge(0, 2, 3.0));
    graph.addEdge(new DirectedEdge(0, 2, 3.0));
    graph.addEdge(new DirectedEdge(2, 0, 3.0));
    assertEquals(5, graph.getEdgeSize());
    assertAdjVContainsAllW(graph, 0, createCollection(1, 2, 2));
    assertAdjVContainsAllW(graph, 1, createCollection(0));
    assertAdjVContainsAllW(graph, 2, createCollection(0));
    assertEquals(3, graph.degree(0));
    assertEquals(1, graph.degree(1));
    assertEquals(1, graph.degree(2));
  }

  @Test
  void degree() {
    DirectedWeightedGraph graph = createDirectedWeightedGraph(5);
    graph.addEdge(new DirectedEdge(0, 1, 3.0));
    assertEquals(1, graph.degree(0));
    assertEquals(0, graph.degree(1));
    assertEquals(0, graph.degree(2));
    assertEquals(0, graph.degree(3));
    assertEquals(0, graph.degree(4));
  }

  @Test
  void maxDegreeEmpty() {
    DirectedWeightedGraph graph = createDirectedWeightedGraph(5);
    assertEquals(0, graph.maxDegree().getKey());
    assertEquals(0, graph.maxDegree().getValue());
  }

  @Test
  void maxDegree() {
    DirectedWeightedGraph graph = createDirectedWeightedGraph(5);
    graph.addEdge(new DirectedEdge(2, 3, 3.0));
    graph.addEdge(new DirectedEdge(2, 4, 3.0));
    graph.addEdge(new DirectedEdge(2, 1, 3.0));
    graph.addEdge(new DirectedEdge(0, 1, 3.0));
    assertEquals(2, graph.maxDegree().getKey());
    assertEquals(3, graph.maxDegree().getValue());
  }

}