package ggoncalves.sedgewick.datastructure.graph.weighted;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EdgeTest {

  @Test
  void constructor() {
    Edge edge = new Edge(1, 2, 3.0);
    assertEquals(1, edge.either());
    assertEquals(2, edge.other(1));
    assertEquals(3.0, edge.getWeight());
  }

  @Test
  void constructorIllegalVertices() {
    assertThrows(IllegalArgumentException.class, () -> new Edge(-1, 0, 0.1d));
    assertThrows(IllegalArgumentException.class, () -> new Edge(0, -1, 0.1d));
  }

  @Test
  void other() {
    Edge edge = new Edge(1, 2, 3.0);
    assertEquals(2, edge.other(1));
    assertEquals(1, edge.other(2));
  }

  @Test
  void compareToTest() {
    Edge edge = new Edge(1, 2, 3.0);
    assertEquals(-1, edge.compareTo(new Edge(2, 4, 4.0)));
    assertEquals(1, edge.compareTo(new Edge(2, 4, 2.0)));
    assertEquals(0, edge.compareTo(new Edge(2, 4, 3.0)));
  }

  @Test
  void toStringTest() {
    Edge edge = new Edge(1, 2, 3.0);
    assertEquals("Edge { v:1 ---- 3.0 -----> w:2", edge.toString());
  }
}