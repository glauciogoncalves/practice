package ggoncalves.sedgewick.datastructure.graph.weighted;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DirectedEdgeTest {

  @Test
  void constructor() {
    DirectedEdge edge = new DirectedEdge(1, 2, 3.0);
    assertEquals(1, edge.from());
    assertEquals(2, edge.to());
    assertEquals(3.0, edge.getWeight());
  }

  @Test
  void constructorIllegalVertices() {
    assertThrows(IllegalArgumentException.class, () -> new DirectedEdge(-1, 0, 0.1d));
    assertThrows(IllegalArgumentException.class, () -> new DirectedEdge(0, -1, 0.1d));
  }

  @Test
  void compareToTest() {
    DirectedEdge edge = new DirectedEdge(1, 2, 3.0);
    assertEquals(-1, edge.compareTo(new DirectedEdge(2, 4, 4.0)));
    assertEquals(1, edge.compareTo(new DirectedEdge(2, 4, 2.0)));
    assertEquals(0, edge.compareTo(new DirectedEdge(2, 4, 3.0)));
  }

  @Test
  void toStringTest() {
    DirectedEdge edge = new DirectedEdge(1, 2, 3.0);
    assertEquals("Edge { v:1 ---- 3.0 -----> w:2", edge.toString());
  }
}