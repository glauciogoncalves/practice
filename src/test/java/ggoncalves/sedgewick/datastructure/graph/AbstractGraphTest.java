package ggoncalves.sedgewick.datastructure.graph;

import ggoncalves.sedgewick.datastructure.graph.unweighted.DirectedGraph;
import ggoncalves.sedgewick.datastructure.graph.unweighted.Graph;
import ggoncalves.sedgewick.datastructure.graph.unweighted.UndirectedGraph;
import ggoncalves.sedgewick.datastructure.graph.weighted.DirectedEdge;
import ggoncalves.sedgewick.datastructure.graph.weighted.DirectedWeightedGraph;
import ggoncalves.sedgewick.datastructure.graph.weighted.Edge;
import ggoncalves.sedgewick.datastructure.graph.weighted.UndirectedWeightedGraph;
import org.junit.jupiter.api.BeforeAll;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.fail;

public class AbstractGraphTest {

  // Sedgewick sample Graph from UndirectedGraphs.pdf page 19 (first tiny Graph).
  protected static final UndirectedGraph undirectedSampleGraph = new UndirectedGraph(13);

  // Sedgewick sample Graph from DirectedGraphs.pdf page 36.
  protected static final DirectedGraph directedSampleGraph = new DirectedGraph(13);

  // Sedgewick sample Graph from DirectedGraphs.pdf page 42.
  protected static final DirectedGraph directedAcyclicGraph = new DirectedGraph(7);

  @BeforeAll
  static void beforeAll() {
    createUndirectedSampleGraph();
    createDirectedSampleGraph();
    createDirectedAcyclicGraph();
  }

  private static void createUndirectedSampleGraph() {
    undirectedSampleGraph.addEdge(0, 5);
    undirectedSampleGraph.addEdge(4, 3);
    undirectedSampleGraph.addEdge(0, 1);
    undirectedSampleGraph.addEdge(9, 12);
    undirectedSampleGraph.addEdge(6, 4);
    undirectedSampleGraph.addEdge(5, 4);
    undirectedSampleGraph.addEdge(0, 2);
    undirectedSampleGraph.addEdge(11, 12);
    undirectedSampleGraph.addEdge(9, 10);
    undirectedSampleGraph.addEdge(0, 6);
    undirectedSampleGraph.addEdge(7, 8);
    undirectedSampleGraph.addEdge(9, 11);
    undirectedSampleGraph.addEdge(5, 3);
  }

  private static void createDirectedSampleGraph() {
    directedSampleGraph.addEdge(0, 5);
    directedSampleGraph.addEdge(0, 1);
    directedSampleGraph.addEdge(2, 0);
    directedSampleGraph.addEdge(2, 3);
    directedSampleGraph.addEdge(3, 2);
    directedSampleGraph.addEdge(3, 5);
    directedSampleGraph.addEdge(4, 3);
    directedSampleGraph.addEdge(4, 2);
    directedSampleGraph.addEdge(5, 4);
    directedSampleGraph.addEdge(6, 0);
    directedSampleGraph.addEdge(6, 4);
    directedSampleGraph.addEdge(6, 8);
    directedSampleGraph.addEdge(6, 9);
    directedSampleGraph.addEdge(7, 6);
    directedSampleGraph.addEdge(7, 9);
    directedSampleGraph.addEdge(8, 6);
    directedSampleGraph.addEdge(9, 10);
    directedSampleGraph.addEdge(9, 11);
    directedSampleGraph.addEdge(10, 12);
    directedSampleGraph.addEdge(11, 12);
    directedSampleGraph.addEdge(11, 4);
    directedSampleGraph.addEdge(12, 9);
  }

  private static void createDirectedAcyclicGraph() {
    directedAcyclicGraph.addEdge(0, 5);
    directedAcyclicGraph.addEdge(0, 1);
    directedAcyclicGraph.addEdge(0, 2);
    directedAcyclicGraph.addEdge(0, 2);
    directedAcyclicGraph.addEdge(1, 4);
    directedAcyclicGraph.addEdge(3, 6);
    directedAcyclicGraph.addEdge(3, 5);
    directedAcyclicGraph.addEdge(3, 4);
    directedAcyclicGraph.addEdge(3, 2);
    directedAcyclicGraph.addEdge(5, 2);
    directedAcyclicGraph.addEdge(6, 4);
  }

  protected UndirectedGraph createUndirectedGraph(Integer verticeSize) {
    return new UndirectedGraph(verticeSize);
  }

  protected UndirectedWeightedGraph createUndirectedWeightedGraph(Integer verticeSize) {
    return new UndirectedWeightedGraph(verticeSize);
  }

  protected DirectedWeightedGraph createDirectedWeightedGraph(Integer verticeSize) {
    return new DirectedWeightedGraph(verticeSize);
  }

  protected DirectedGraph createDirectedGraph(Integer verticeSize) {
    return new DirectedGraph(verticeSize);
  }

  protected void assertAdjVContainsAllW(Graph<Integer> graph, Integer v, Collection<Integer> wCol) {
    for (Integer adj : graph.adj(v)) {
      if (!wCol.contains(adj)) fail("Test didnt expected for " + adj + " value test case");
      wCol.remove(adj);
    }
    if (!wCol.isEmpty()) {
      StringBuilder sb = new StringBuilder();
      wCol.stream().distinct().forEach((i) -> sb.append(" ").append(i));
      fail("Elements not found at adjacencies:[" + sb.toString() + " ]");
    }
  }

  protected void assertAdjVContainsAllW(UndirectedWeightedGraph graph, Integer v, Collection<Integer> wCol) {
    for (Edge edge : graph.adj(v)) {
      if (!wCol.contains(edge.other(v))) fail("Test didnt expected for " + edge + " value test case");
      wCol.remove(edge.other(v));
    }
    if (!wCol.isEmpty()) {
      StringBuilder sb = new StringBuilder();
      wCol.stream().distinct().forEach((i) -> sb.append(" ").append(i));
      fail("Elements not found at adjacencies:[" + sb.toString() + " ]");
    }
  }

  protected void assertAdjVContainsAllW(DirectedWeightedGraph graph, Integer v, Collection<Integer> wCol) {
    for (DirectedEdge edge : graph.adj(v)) {
      if (!wCol.contains(edge.to())) fail("Test didnt expected for " + edge + " value test case");
      wCol.remove(edge.to());
    }
    if (!wCol.isEmpty()) {
      StringBuilder sb = new StringBuilder();
      wCol.stream().distinct().forEach((i) -> sb.append(" ").append(i));
      fail("Elements not found at adjacencies:[" + sb.toString() + " ]");
    }
  }

  protected Collection<Integer> createCollection(Integer... elements) {
    return Arrays.stream(elements).collect(Collectors.toList());
  }
}
