package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.AbstractGraphTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CycleCheckTest extends AbstractGraphTest {

  @Test
  void orderAcyclic() {
    CycleCheck cycleCheck = new CycleCheck(directedAcyclicGraph);
    assertFalse(cycleCheck.hasCycle());
  }

  @Test
  void orderCyclic() {
    CycleCheck cycleCheck = new CycleCheck(directedSampleGraph);
    assertTrue(cycleCheck.hasCycle());
  }
}