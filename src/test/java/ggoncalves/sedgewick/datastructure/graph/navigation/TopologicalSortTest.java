package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.AbstractGraphTest;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static ggoncalves.sedgewick.helpers.CollectionHelper.assertCollectionContains;

class TopologicalSortTest extends AbstractGraphTest {

  @Test
  void order() {
    TopologicalSort ts = new TopologicalSort(directedAcyclicGraph);
    ts.run();
    Collection<Integer> c = ts.order();
    assertCollectionContains(c, 3, 6, 0, 1, 4, 5, 2);
  }
}