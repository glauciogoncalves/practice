package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.AbstractGraphTest;
import ggoncalves.sedgewick.datastructure.graph.unweighted.Graph;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class AbstractNavigationGraphTest extends AbstractGraphTest {

  abstract Paths createSearchPath(Graph<Integer> graph, Integer s);

  @Test
  void contructor() {
    Paths path = createSearchPath(createUndirectedGraph(5), 0);
    assertFalse(path.hasPathTo(0));
    assertFalse(path.hasPathTo(1));
    assertFalse(path.hasPathTo(2));
    assertFalse(path.hasPathTo(3));
    assertFalse(path.hasPathTo(4));
  }

  @Test
  void hasPathTo() {
    Paths path = createSearchPath(undirectedSampleGraph, 0);
    path.run();
    assertTrue(path.hasPathTo(0));
    assertTrue(path.hasPathTo(1));
    assertTrue(path.hasPathTo(2));
    assertTrue(path.hasPathTo(3));
    assertTrue(path.hasPathTo(4));
    assertTrue(path.hasPathTo(5));
    assertTrue(path.hasPathTo(6));
    assertFalse(path.hasPathTo(7));
    assertFalse(path.hasPathTo(8));
    assertFalse(path.hasPathTo(9));
    assertFalse(path.hasPathTo(10));
    assertFalse(path.hasPathTo(11));
    assertFalse(path.hasPathTo(12));
  }

  @Test
  void pathTo() {
    Paths path = createSearchPath(undirectedSampleGraph, 0);
    path.run();
    for (Integer p : path.pathTo(4)) {
      System.out.println(p);
    }
  }


}
