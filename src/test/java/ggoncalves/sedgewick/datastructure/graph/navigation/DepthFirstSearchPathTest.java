package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.unweighted.Graph;

class DepthFirstSearchPathTest extends AbstractNavigationGraphTest {

  @Override
  Paths createSearchPath(Graph<Integer> graph, Integer s) {
    return new DepthFirstSearchPath(graph, s);
  }

}