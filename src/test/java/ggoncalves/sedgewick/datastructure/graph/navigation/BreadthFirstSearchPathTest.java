package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.unweighted.Graph;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BreadthFirstSearchPathTest extends AbstractNavigationGraphTest {

  @Override
  Paths createSearchPath(Graph<Integer> graph, Integer s) {
    return new BreadthFirstSearchPath(graph, s);
  }

  @Test
  void distTo() {
    BreadthFirstSearchPath bfs = new BreadthFirstSearchPath(undirectedSampleGraph, 0);
    bfs.run();
    assertEquals(0, bfs.distTo(0));
    assertEquals(1, bfs.distTo(1));
    assertEquals(1, bfs.distTo(2));
    assertEquals(1, bfs.distTo(6));
    assertEquals(1, bfs.distTo(5));
    assertEquals(2, bfs.distTo(3));
    assertEquals(2, bfs.distTo(4));
  }
}