package ggoncalves.sedgewick.datastructure.graph.unweighted;

import ggoncalves.sedgewick.datastructure.graph.AbstractGraphTest;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;

import static org.junit.jupiter.api.Assertions.*;

class DirectedGraphTest extends AbstractGraphTest {

  @Test
  void constructorIllegal() {
    assertThrows(IllegalArgumentException.class, () -> new DirectedGraph(0));
    assertThrows(IllegalArgumentException.class, () -> new DirectedGraph(null));
    assertThrows(IllegalArgumentException.class, () -> new DirectedGraph(-1));
  }

  @Test
  void constructor() {
    DirectedGraph graph = new DirectedGraph(10);
    assertNotNull(graph);
    assertEquals(10, graph.getVerticeSize());
    assertEquals(0, graph.getEdgeSize());
  }

  @Test
  void getVerticeSize() {
    assertEquals(1, new DirectedGraph(1).getVerticeSize());
    assertEquals(10, new DirectedGraph(10).getVerticeSize());
    assertEquals(1000, new DirectedGraph(1000).getVerticeSize());
  }

  @Test
  void getEdgeSizeEmpty() {
    assertEquals(0, new DirectedGraph(1).getEdgeSize());
  }

  @Test
  void getEdgeSize() {
    DirectedGraph graph = createDirectedGraph(10);
    graph.addEdge(0, 1);
    graph.addEdge(8, 9);
    assertEquals(2, graph.getEdgeSize());
  }

  @Test
  void addEdge() {
    DirectedGraph graph = createDirectedGraph(10);
    graph.addEdge(0, 1);
    assertEquals(1, graph.getEdgeSize());
    assertAdjVContainsAllW(graph, 0, createCollection(1));
  }

  @Test
  void addEdgeDuplicated() {
    DirectedGraph graph = createDirectedGraph(10);
    graph.addEdge(0, 1);
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);
    graph.addEdge(0, 2);
    assertEquals(2, graph.getEdgeSize());
    assertAdjVContainsAllW(graph, 0, createCollection(1, 2));
    assertEquals(2, graph.degree(0));
    assertEquals(0, graph.degree(1));
    assertEquals(0, graph.degree(2));
  }

  @Test
  void degree() {
    DirectedGraph graph = createDirectedGraph(5);
    graph.addEdge(0, 1);
    assertEquals(1, graph.degree(0));
    assertEquals(0, graph.degree(1));
    assertEquals(0, graph.degree(2));
    assertEquals(0, graph.degree(3));
    assertEquals(0, graph.degree(4));
  }

  @Test
  void maxDegreeEmpty() {
    DirectedGraph graph = createDirectedGraph(5);
    assertEquals(0, graph.maxDegree().getKey());
    assertEquals(0, graph.maxDegree().getValue());
  }

  @Test
  void maxDegree() {
    DirectedGraph graph = createDirectedGraph(5);
    graph.addEdge(2, 3);
    graph.addEdge(2, 4);
    graph.addEdge(2, 1);
    graph.addEdge(0, 1);
    assertEquals(2, graph.maxDegree().getKey());
    assertEquals(3, graph.maxDegree().getValue());
  }

  @Test
  void addEdgeAlreadyAdded() {
    DirectedGraph graph = createDirectedGraph(10);
    graph.addEdge(0, 1);
  }

  @Test
  void addEdgeIllegalV() {
    DirectedGraph graph = createDirectedGraph(10);
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(-1, 1));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(null, 1));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(graph.getVerticeSize(), 1));
  }

  @Test
  void addEdgeIllegalW() {
    DirectedGraph graph = createDirectedGraph(10);
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, -1));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, null));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, graph.getVerticeSize()));
  }

  @Test
  void sampleGraph() {
    assertEquals(13, directedSampleGraph.getVerticeSize());
    assertEquals(22, directedSampleGraph.getEdgeSize());
    AbstractMap.SimpleEntry<Integer, Integer> maxDegree = directedSampleGraph.maxDegree();
    assertNotNull(maxDegree);
    assertEquals(6, maxDegree.getKey());
    assertEquals(4, maxDegree.getValue());
  }

}
