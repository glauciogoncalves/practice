package ggoncalves.sedgewick.datastructure.graph.unweighted;

import ggoncalves.sedgewick.datastructure.graph.AbstractGraphTest;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;

import static org.junit.jupiter.api.Assertions.*;

class UndirectedGraphTest extends AbstractGraphTest {

  @Test
  void constructorIllegal() {
    assertThrows(IllegalArgumentException.class, () -> new UndirectedGraph(0));
    assertThrows(IllegalArgumentException.class, () -> new UndirectedGraph(null));
    assertThrows(IllegalArgumentException.class, () -> new UndirectedGraph(-1));
  }

  @Test
  void constructor() {
    UndirectedGraph graph = new UndirectedGraph(10);
    assertNotNull(graph);
    assertEquals(10, graph.getVerticeSize());
    assertEquals(0, graph.getEdgeSize());
  }

  @Test
  void getVerticeSize() {
    assertEquals(1, new UndirectedGraph(1).getVerticeSize());
    assertEquals(10, new UndirectedGraph(10).getVerticeSize());
    assertEquals(1000, new UndirectedGraph(1000).getVerticeSize());
  }

  @Test
  void getEdgeSizeEmpty() {
    assertEquals(0, new UndirectedGraph(1).getEdgeSize());
  }

  @Test
  void getEdgeSize() {
    UndirectedGraph graph = createUndirectedGraph(10);
    graph.addEdge(0, 1);
    graph.addEdge(8, 9);
    assertEquals(2, graph.getEdgeSize());
  }

  @Test
  void addEdge() {
    UndirectedGraph graph = createUndirectedGraph(10);
    graph.addEdge(0, 1);
    assertEquals(1, graph.getEdgeSize());
    assertAdjVContainsAllW(graph, 0, createCollection(1));
    assertAdjVContainsAllW(graph, 1, createCollection(0));
  }

  @Test
  void addEdgeDuplicated() {
    UndirectedGraph graph = createUndirectedGraph(10);
    graph.addEdge(0, 1);
    graph.addEdge(1, 0);
    graph.addEdge(0, 2);
    graph.addEdge(0, 2);
    graph.addEdge(2, 0);
    assertEquals(2, graph.getEdgeSize());
    assertAdjVContainsAllW(graph, 0, createCollection(1, 2));
    assertAdjVContainsAllW(graph, 1, createCollection(0));
    assertAdjVContainsAllW(graph, 2, createCollection(0));
    assertEquals(2, graph.degree(0));
    assertEquals(1, graph.degree(1));
    assertEquals(1, graph.degree(2));
  }

  @Test
  void degree() {
    UndirectedGraph graph = createUndirectedGraph(5);
    graph.addEdge(0, 1);
    assertEquals(1, graph.degree(0));
    assertEquals(1, graph.degree(1));
    assertEquals(0, graph.degree(2));
    assertEquals(0, graph.degree(3));
    assertEquals(0, graph.degree(4));
  }

  @Test
  void maxDegreeEmpty() {
    UndirectedGraph graph = createUndirectedGraph(5);
    assertEquals(0, graph.maxDegree().getKey());
    assertEquals(0, graph.maxDegree().getValue());
  }

  @Test
  void maxDegree() {
    UndirectedGraph graph = createUndirectedGraph(5);
    graph.addEdge(2, 3);
    graph.addEdge(2, 4);
    graph.addEdge(2, 1);
    graph.addEdge(0, 1);
    assertEquals(2, graph.maxDegree().getKey());
    assertEquals(3, graph.maxDegree().getValue());
  }

  @Test
  void addEdgeIllegalV() {
    UndirectedGraph graph = createUndirectedGraph(10);
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(-1, 1));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(null, 1));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(graph.getVerticeSize(), 1));
  }

  @Test
  void addEdgeIllegalW() {
    UndirectedGraph graph = createUndirectedGraph(10);
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, -1));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, null));
    assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, graph.getVerticeSize()));
  }

  @Test
  void sampleGraph() {
    assertEquals(13, undirectedSampleGraph.getVerticeSize());
    assertEquals(13, undirectedSampleGraph.getEdgeSize());
    AbstractMap.SimpleEntry<Integer, Integer> maxDegree = undirectedSampleGraph.maxDegree();
    assertNotNull(maxDegree);
    assertEquals(0, maxDegree.getKey());
    assertEquals(4, maxDegree.getValue());
  }

}
