package ggoncalves.sedgewick.datastructure.heap;

import ggoncalves.sedgewick.helpers.HeapHelper;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.PriorityQueue;

import static ggoncalves.sedgewick.helpers.HeapHelper.isHeapfied;
import static ggoncalves.sedgewick.helpers.HeapHelper.printArray;
import static org.junit.jupiter.api.Assertions.*;

class HeapTest {

  @Test
  void constructor() {
    assertConstructor(new Heap<>(10), 11);
  }

  @Test
  void constructorDefault() {
    assertConstructor(new Heap<>(), Heap.DEFAULT_INITIAL_CAPACITY + 1);
  }

  @Test
  void constructorArray() {
    Integer[] array = {4, 70, 8, 1, 90, 1, 20};
    Heap<Integer> maxHeap = new Heap<>(array);
    assertFalse(maxHeap.isEmpty());
    assertTrue(isHeapfied(maxHeap));
    Comparable[] internalArray = maxHeap.getArray();
    assertEquals(array.length * 2 + 1, internalArray.length);
    printArray(maxHeap);
  }

  @Test
  void isEmpty() {
    Heap<Integer> maxHeap = new Heap<>(10);
    assertTrue(maxHeap.isEmpty());
    maxHeap.add(4);
    assertFalse(maxHeap.isEmpty());
  }

  @Test
  void addOrdered() {
    PriorityQueue<Integer> p = new PriorityQueue<>(new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        return 0;
      }
    });
    Heap<Integer> maxHeap = new Heap<>(10);
    assertAddElements(maxHeap, 0, 1, 2, 3, 4);
  }

  @Test
  void addUnordered() {
    Heap<Integer> maxHeap = new Heap<>(10);
    assertAddElements(maxHeap, 4, 3, 2, 1, 0);
  }

  @Test
  void addRandomOrder() {
    Heap<Integer> maxHeap = new Heap<>(10);
    assertAddElements(maxHeap, 40, 350, 20, 100, 47);
  }

  @Test
  void poll() {
    Heap<Integer> maxHeap = new Heap<>(10);
    maxHeap.add(3);
    maxHeap.add(40);
    maxHeap.add(41);
    maxHeap.add(2);
    maxHeap.add(50);
    assertTrue(isHeapfied(maxHeap));
    assertEquals(50, maxHeap.poll());
    assertEquals(41, maxHeap.poll());
    assertEquals(40, maxHeap.poll());
    assertEquals(3, maxHeap.poll());
    assertEquals(2, maxHeap.poll());
    assertNull(maxHeap.poll());
  }

  void assertAddElements(Heap<Integer> heap, Integer... elements) {
    for (Integer element : elements) {
      heap.add(element);
      HeapHelper.printArray(heap);
    }
    assertTrue(isHeapfied(heap));
  }

  void assertConstructor(Heap<Integer> heap, int expectedCapacity) {
    assertTrue(isHeapfied(heap));
    Comparable[] array = heap.getArray();
    assertEquals(expectedCapacity, array.length);
    assertTrue(heap.isEmpty());
  }
}