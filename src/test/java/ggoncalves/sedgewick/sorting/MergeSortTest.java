package ggoncalves.sedgewick.sorting;

public class MergeSortTest extends AbstractSortTest {

  @Override
  Sorter<Integer> createSorter() {
    return new MergeSort<Integer>();
  }
}