package ggoncalves.sedgewick.sorting;

public class OptimizedMergeSortTest extends AbstractSortTest {

  @Override
  Sorter<Integer> createSorter() {
    return new OptimizedMergeSort<Integer>();
  }
}