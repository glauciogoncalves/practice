package ggoncalves.sedgewick.sorting;

public class InsertionSortTest extends AbstractSortTest {

  @Override
  Sorter<Integer> createSorter() {
    return new InsertionSort<Integer>();
  }
}