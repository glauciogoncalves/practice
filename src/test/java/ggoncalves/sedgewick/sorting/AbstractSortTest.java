package ggoncalves.sedgewick.sorting;

import ggoncalves.sedgewick.helpers.ArrayHelper;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertTrue;

public abstract class AbstractSortTest<T extends Comparable<T>> {

  private final Random random = new Random();

  private static final int MEDIUM_SIZE = 500;
  private static final int BIG_SIZE = MEDIUM_SIZE * 10;

  @Test
  public void sortSimple() {
    assertSortSimple(createIntegerArray());
  }

  @Test
  public void sortMedium() {
    assertSortSimple(createIntegerArray(MEDIUM_SIZE));
  }

  @Test
  public void sortMediumWithDuplicates() {
    assertSortSimple(createIntegerArrayWithDuplicates(MEDIUM_SIZE));
  }

  @Test
  public void sortBig() {
    assertSortSimple(createIntegerArray(BIG_SIZE));
  }

  @Test
  public void sortBigWithDuplicates() {
    assertSortSimple(createIntegerArrayWithDuplicates(BIG_SIZE));
  }

  private void assertSortSimple(Integer[] arr) {
    assertSortSimple(arr, false);
  }

  private void assertSortSimple(Integer[] arr, boolean printArray) {
    if (printArray) printArray(arr);
    Sorter<Integer> sorter = createSorter();
    Long start = System.currentTimeMillis();
    sorter.sort(arr);
    Long timeElapsed = System.currentTimeMillis() - start;
    if (printArray) printArray(arr);
    assertTrue(isSorted(arr));
    System.out.println("Size: " + arr.length + " in " + timeElapsed + "ms");
  }

  abstract Sorter<Integer> createSorter();

  protected <T extends Comparable<T>> void printArray(T[] arr) {
    if (arr == null) {
      System.out.println("Array is null");
    } else if (arr.length == 0) {
      System.out.println("Array is empty");
    } else {
      StringBuilder sb = new StringBuilder();
      for (T t : arr) {
        sb.append(t.toString());
        sb.append(" , ");
      }
      System.out.println("Array [ " + sb.substring(0, sb.length() - 3) + " ] isSorted: " + isSorted(arr));
    }
  }

  protected <T extends Comparable<T>> boolean isSorted(T[] arr) {
    if (arr == null || arr.length <= 1) {
      return true;
    }

    for (int i = 0; i < arr.length - 1; i++) {
      if (arr[i].compareTo(arr[i + 1]) > 0) {
        return false;
      }
    }
    return true;
  }

  protected Integer[] createIntegerArray() {
    return new Integer[]{3, 2, 5, 6, 1, 10, 3, 1};
  }

  protected Integer[] createIntegerArray(int size) {
    Integer[] arr = new Integer[size];

    for (int i = 0; i < size; i++) {
      arr[i] = i;

      if (i > 0) {
        int randomIndex = random.nextInt(i + 1);
        if (randomIndex != i) {
          ArrayHelper.exchange(arr, i, randomIndex);
        }
      }
    }

    return arr;
  }

  protected Integer[] createIntegerArrayWithDuplicates(int size) {
    Integer maxElement = (size / 2) + 1;

    Integer[] arr = new Integer[size];

    for (int i = 0; i < size; i++) {
      arr[i] = random.nextInt(maxElement);
    }
    return arr;
  }


}