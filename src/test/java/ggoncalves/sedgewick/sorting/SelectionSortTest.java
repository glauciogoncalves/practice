package ggoncalves.sedgewick.sorting;

public class SelectionSortTest extends AbstractSortTest {

  @Override
  Sorter<Integer> createSorter() {
    return new SelectionSort<Integer>();
  }
}