package ggoncalves.sedgewick.sorting;

public class ThreeWayQuickSortTest extends AbstractSortTest {

  @Override
  Sorter<Integer> createSorter() {
    return new ThreeWayQuickSort<Integer>();
  }
}