package ggoncalves.sedgewick.sorting;

public class QuickSortTest extends AbstractSortTest {

  @Override
  Sorter<Integer> createSorter() {
    return new QuickSort<Integer>();
  }
}