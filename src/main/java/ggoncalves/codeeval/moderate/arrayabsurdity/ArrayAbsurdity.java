package ggoncalves.codeeval.moderate.arrayabsurdity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ArrayAbsurdity {

    public ArrayAbsurdity() {
    }

    void handleLine(String line) {
        String result = arrayAbsurdity(line);

        if (!result.isEmpty()) {
            printResult(result);
        }
    }

    String arrayAbsurdity(String line) {
        if (line == null || line.trim().isEmpty()) {
            return "";
        }

        String[] splitLine = line.split(";");
        int n = Integer.parseInt(splitLine[0]);

        boolean[] hash = new boolean[n-1];
        for (String s : splitLine[1].split(",")) {
            int num = Integer.parseInt(s);
            if (hash[num]) {
                return s;
            }
            hash[num] = true;
        }
        return "";
    }

    void printResult(String result) {
        System.out.println(result);
    }

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file));
            String line;
            ArrayAbsurdity arrayAbsurdity = new ArrayAbsurdity();
            while ((line = buffer.readLine()) != null) {
                arrayAbsurdity.handleLine(line.trim());
            }
        } finally {
            if (buffer != null) {
                buffer.close();
            }
        }
    }
}
