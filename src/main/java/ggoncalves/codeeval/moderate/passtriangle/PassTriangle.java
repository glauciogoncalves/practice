package ggoncalves.codeeval.moderate.passtriangle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class PassTriangle {

	public PassTriangle(LinkedList<ArrayList<Integer>> list) {

		boolean isFirst = true;
		ArrayList<Integer> lastLineList = new ArrayList<Integer>();

		for (ArrayList<Integer> lineList : list) {

			if (isFirst) {
				lastLineList = new ArrayList<Integer>(lineList);
				isFirst = false;
				continue;
			}

			lastLineList = passTriangle(lastLineList, lineList);

		}

		printResult(lastLineList);
	}

	public PassTriangle() {
	}

	ArrayList<Integer> passTriangle(ArrayList<Integer> lastLineList,
	    ArrayList<Integer> lineList) {
		
		for (int i = 0; i < lineList.size(); i++) {
	    
			Integer element = 0;
			
			if (i == 0) {
				element = lineList.get(i) + lastLineList.get(i);
			}
			
			else if (i == lineList.size() - 1) {
				element = lineList.get(i) + lastLineList.get(lastLineList.size()-1);
			}
			
			else {
				element = lineList.get(i) + Math.max(lastLineList.get(i-1), lastLineList.get(i));
			}
			
			lineList.set(i, element);
			
    }

		return lineList;

	}

	void printResult(ArrayList<Integer> lastLineList) {
		Integer max = Integer.MIN_VALUE;
		
		for (Integer integer : lastLineList) {
	    max = Math.max(max, integer);
    }
		
		System.out.println(max);
	}

	private static ArrayList<Integer> parseToList(String line) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		String[] parts = line.trim().split(" ");
		for (int i = 0; i < parts.length; i++) {
			list.add(Integer.parseInt(parts[i]));
		}
		return list;
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			LinkedList<ArrayList<Integer>> list = new LinkedList<ArrayList<Integer>>();
			while ((line = buffer.readLine()) != null) {
				list.add(parseToList(line));
			}
			new PassTriangle(list);
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
