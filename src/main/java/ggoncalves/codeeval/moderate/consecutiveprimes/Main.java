package ggoncalves.codeeval.moderate.consecutiveprimes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public Main(String line) {
		String result = consecutivePrimes(line);

		if (!result.isEmpty()) {
			printResult(result);
		}
	}
	
	public Main() {
  }

	String consecutivePrimes(String line) {
		
		int value = Integer.parseInt(line);
		
		if (value % 2 != 0) {
			return "0";
		}
		
		if (value < 2 || value > 18) {
			return "";
		}

        List<Integer> evenList = createEvenList(value);
        List<Integer> oddList = createOddList(value);

		List<String> result = new ArrayList<String>();
		
		permutation(result, new ArrayList<Integer>(), evenList, oddList);

        System.out.println(result);

        return "" + result.size();
		
	}

	private List<Integer> createEvenList(int value) {
		List<Integer> l = new ArrayList<Integer>();

        for (int i = 1; i <= value; i++) {
            if (i%2 == 0) {
                l.add(i);
            }
        }

		return l;
	}

	private List<Integer> createOddList(int value) {
		List<Integer> l = new ArrayList<Integer>();

        for (int i = 1; i <= value; i++) {
            if (i%2 != 0) {
                l.add(i);
            }
        }

		return l;
	}

	private String createInitialString(int n) {
		String s = "";
		for (int i = 0; i < n; i++) {
	    s += "" + i;
    }
		return s;
	}
	
	boolean isPrime(int integer) {
		if (integer == 2) {
			return false;
		}
		
		int middle = integer/2;
		
		for (int i = 2; i<=middle; i++) {
			if (integer % i == 0) {
				return false;
			}
		}
		
		return true;
	}
	
	void permutation(List<String> result, String prefix, String s) {
		if (s.length() == 0) {
			if (isPrime(Integer.parseInt("" + prefix.charAt(0)) + Integer.parseInt("" + prefix.charAt(prefix.length() - 1)))) {
				result.add(prefix);
			}
		}
		else {
			for (int i=0; i<s.length(); i++) {
				if (prefix.isEmpty()) {
					permutation(result, prefix + s.charAt(i), s.substring(0, i) + s.substring(i + 1, s.length()));
				}
				else if (isPrime(Integer.parseInt("" + prefix.charAt(prefix.length()-1)) + Integer.parseInt("" + s.charAt(i)))) {
					permutation(result, prefix + s.charAt(i), s.substring(0, i) + s.substring(i + 1, s.length()));
				}
			}
		}
	}

	void permutation(List<String> result, List<Integer> prefix, List<Integer> evenList, List<Integer> oddList) {
		if (evenList.size() == 0 && oddList.size() == 0) {

            if (isPrime(prefix.get(0) + prefix.get(prefix.size() - 1))) {
                StringBuilder sb = new StringBuilder();
                for (Integer integer : prefix) {
                    sb.append(integer);
                }
                result.add(sb.toString());
            }
		}
		else {
            // Copia elementos dentro de um novo array.
            LinkedList<Integer> linkedList = new LinkedList<Integer>();
            linkedList.addAll(evenList);
            linkedList.addAll(oddList);

            for (Integer integer : linkedList) {

                if (prefix.isEmpty()) {
                    evenList.remove(integer);
                    oddList.remove(integer);
                    prefix.add(integer);
                    permutation(result, prefix, evenList, oddList);
                }
                else {
                    int lastI = prefix.get(prefix.size()-1);
                    if (lastI % 2 == integer % 2) {
                        continue;
                    }
                    if (isPrime(lastI + integer)) {
                        evenList.remove(integer);
                        oddList.remove(integer);
                        prefix.add(integer);
                        permutation(result, prefix, evenList, oddList);
                    }
                }

            }
		}
	}

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				new Main(line.trim());
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
