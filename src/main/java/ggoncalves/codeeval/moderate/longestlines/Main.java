package ggoncalves.codeeval.moderate.longestlines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Main {

	private static PriorityQueue<String> createQueue() {
		return new PriorityQueue<String>(11, new Comparator<String>() {

			@Override
      public int compare(String o1, String o2) {
				
				Integer l1 = o1.length();
				Integer l2 = o2.length();
				
	      return l2.compareTo(l1);
      }
		});
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line = null;
			boolean isFirst = true;
			
			int returnSize = 0;
			
			PriorityQueue<String> queue = createQueue();
			
			while ((line = buffer.readLine()) != null) {
				
				if (isFirst) {
					returnSize = Integer.parseInt(line);
					isFirst = false;
				}
				else {
					queue.add(line);
				}
			}
			
			while (returnSize-- > 0) {
	      
				System.out.println(queue.poll());
				
      }
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
