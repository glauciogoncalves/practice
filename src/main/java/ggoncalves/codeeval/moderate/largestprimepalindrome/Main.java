package ggoncalves.codeeval.moderate.largestprimepalindrome;

import java.io.IOException;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Main {

	public Main(Integer max) {
		String result = getLargestPrimePalindrome(max);

		if (!result.isEmpty()) {
			printResult(result);
		}
	}
	
	public Main() {
  }

	String getLargestPrimePalindrome(Integer max) {
		
		PriorityQueue<Integer> pQueue = new PriorityQueue<Integer>(10, new Comparator<Integer>() {

			@Override
      public int compare(Integer o1, Integer o2) {
	      // TODO Auto-generated method stub
	      return o2.compareTo(o1);
      }
		});
		
		for (int i = 3; i < max; i++) {
	    
			Integer number = i;
			if (isPrime(number) && isPalidrome(number)) {
				pQueue.add(number);
			}
			
    }
		
		return "" + pQueue.poll();
		
	}
	
	boolean isPalidrome(Integer number) {
		
	  String n = "" + number;
	  
	  
	  for (int i = 0, j = n.length() - 1; i < n.length()/2 ; i++, j--) {
	    
	  	if (n.charAt(i) != n.charAt(j)) {
	  		return false;
	  	}
	  	
    }
		return true;
	}

	private boolean isPrime(Integer number) {
	  
		for (int i = 2; i < number/2 + 1; i++) {
	    if (number%i == 0) {
	    	return false;
	    }
    }
		return true;
		
  }

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		new Main(1000);
	}
}
