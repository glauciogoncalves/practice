package ggoncalves.codeeval.moderate.mthtolast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public Main(String line) {
		String result = mthToLast(line);

		if (!result.isEmpty()) {
			printResult(result);
		}
	}
	
	public Main() {
  }

	String mthToLast(String line) {
		
		if (line == null || line.trim().isEmpty()) {
			return "";
		}
		
		String[] elements = line.trim().split(" ");
		
		int mth = Integer.parseInt(elements[elements.length - 1]);
		
		if (mth > elements.length-1) {
			return "";
		}
		
		int size = elements.length;

		int index = size - mth - 1;
		
		return elements[index];
		
	}

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				new Main(line);
				System.out.println("");
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
