package ggoncalves.codeeval.moderate.reverseandadd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReverseAndAdd {

	public ReverseAndAdd(String line) {
		String result = reverseAndAdd(line);

		if (!result.isEmpty()) {
			printResult(result);
		}
	}
	
	public ReverseAndAdd() {
  }

	String reverseAndAdd(String line) {
		Integer integer = Integer.parseInt(line);
		
		if (isPalindrome(integer)) {
			return "0 " + integer;
		}
		
		int count = 1;
		
		integer += reverse(integer);
		
		while (count < 100 && !isPalindrome(integer)) {
			
			integer += reverse(integer);
			count++;
			
		}
		
		return "" + count + " " + integer;
		
	}
	
	int reverse(int input) {
		int reversedNum = 0;
		while (input != 0) {
	    reversedNum = reversedNum * 10 + input % 10;
	    input = input / 10;   
	}
		return reversedNum;
	}
	
	boolean isPalindrome(String s) {
		return false;
	}
	
	boolean isPalindrome(Integer integer) {
		return integer == reverse(integer);
	}

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				new ReverseAndAdd(line.trim());
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
