package ggoncalves.codeeval.moderate.lowestcommonancestor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public class LowestCommonAncestor {

    private Node root;

    private HashSet<Byte> elementSet;

    public void printLowestAncestor(String line) {
        String result = lowestCommonAncestor(line);

        if (!result.isEmpty()) {
            printResult(result);
        }
    }

    public LowestCommonAncestor() {
    }

    private LowestCommonAncestor.Node findLowest(LowestCommonAncestor.Node node, int n1, int n2) {

        if (node.value == n1 || node.value == n2) {
            return node;
        }

        if (node.value > n1 && node.value < n2) {
            return node;
        }
        else if (node.value < n1 && node.value < n2) {
            if (node.rightChild == null) {
                return node;
            }
            return findLowest(node.rightChild, n1, n2);
        }
        else if (node.value > n1 && node.value > n2) {
            if (node.leftChild == null) {
                return node;
            }
            return findLowest(node.leftChild, n1, n2);
        }
        else if (node.value == n1 || node.value == n2) {
            return (node.parent == null) ? node : node.parent;
        }

        return null;
    }

    String lowestCommonAncestor(String line) {

        if (line == null || line.trim().isEmpty()) {
            return "";
        }

        String[] elements = line.trim().split(" ");

        int n1 = Integer.parseInt(elements[0]);
        int n2 = Integer.parseInt(elements[1]);

        if (n1 > n2) {
            int aux = n1;
            n1 = n2;
            n2 = aux;
        }

        if (!getElementSet().contains((byte)n1) || !getElementSet().contains((byte)n2)) {
            return "";
        }

        Node node = findLowest(getRootNode(), n1, n2);

        if (node == null) {
            return "";
        }
        return "" + (int)node.value;
    }

    private HashSet<Byte> getElementSet() {
        if (elementSet == null) {
            elementSet = new HashSet<Byte>();
            elementSet.add((byte) 10);
            elementSet.add((byte) 29);
            elementSet.add((byte) 20);
            elementSet.add((byte) 3);
            elementSet.add((byte) 8);
            elementSet.add((byte) 52);
            elementSet.add((byte) 30);
        }
        return elementSet;
    }


    Node getRootNode() {
        if (root == null) {
            Node n10 = new Node((byte)10);
            Node n29 = new Node((byte)29);
            Node n20 = new Node((byte)20);
            Node n3 = new Node((byte) 3);
            Node n8 = new Node((byte) 8);
            Node n52 = new Node((byte) 52);
            root = new Node((byte) 30);

            root.setLeftChild(n8);
            root.setRightChild(n52);

            n8.setLeftChild(n3);
            n8.setRightChild(n20);

            n20.setLeftChild(n10);
            n20.setRightChild(n29);

            n10.setParent(n20);
            n29.setParent(n20);

            n3.setParent(n8);
            n20.setParent(n8);

            n8.setParent(root);
            n52.setParent(root);
        }
        return root;
    }

    void printResult(String result) {
        System.out.println(result);
    }

    class Node {
        int value;
        Node parent;
        Node leftChild;
        Node rightChild;

        public Node(byte value) {
            this.value = value;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public void setLeftChild(Node leftChild) {
            this.leftChild = leftChild;
        }

        public void setRightChild(Node rightChild) {
            this.rightChild = rightChild;
        }
    }

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file));
            String line;
            LowestCommonAncestor lca = new LowestCommonAncestor();
            while ((line = buffer.readLine()) != null) {
                lca.printLowestAncestor(line.trim());
            }
        } finally {
            if (buffer != null) {
                buffer.close();
            }
        }
    }
}
