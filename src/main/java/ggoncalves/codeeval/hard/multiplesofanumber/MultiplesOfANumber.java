package ggoncalves.codeeval.hard.multiplesofanumber;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Implementação do problema:
 * <p/>
 * https://www.codeeval.com/open_challenges/18/
 *
 * @author glaucio.c.goncalves@gmail.com
 */
public class MultiplesOfANumber {

    public MultiplesOfANumber() {
    }

    void handleLine(String line) {
        String result = multiplesOfANumber(line);

        if (!result.isEmpty()) {
            printResult(result);
        }
    }

    String multiplesOfANumber(String line) {
        if (line == null || line.trim().isEmpty()) {
            return "";
        }

        String[] splitLine = line.split(",");

        int n1 = Integer.parseInt(splitLine[0]);
        int n2 = Integer.parseInt(splitLine[1]);

        int zeros = 0;
        do {
            n2 = n2 >>> 1;
            zeros++;
        }
        while (n2 > 1);

        n2 = 0b1 << (zeros + 1);

        if (n2 > n1) {
            return "" + n2;
        }

        int count = 1;
        while (n2 < n1) {
            n2 = (0b10 + count++) << zeros;
        }

        return "" + n2;
    }

    void printResult(String result) {
        System.out.println(result);
    }

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file));
            String line;
            MultiplesOfANumber main = new MultiplesOfANumber();
            while ((line = buffer.readLine()) != null) {
                main.handleLine(line.trim());
            }
        } finally {
            if (buffer != null) {
                buffer.close();
            }
        }
    }
}
