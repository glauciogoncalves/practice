package ggoncalves.codeeval.hard.crimehouse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

/**
 * Problema resolvido ainda de forma parcial.
 */
public class CrimeHouse {

    public static final String CRIME_TIME = "CRIME TIME";

    public CrimeHouse() {
    }

    private void handleLine(String line) {
        String result = crimeHouse(line);

        if (!result.isEmpty()) {
            printResult(result);
        }
    }

    String crimeHouse(String line) {
        // 3; E 5|L 0|E 5

        String[] splitCases = line.split(";")[1].trim().split("\\|");

        int inHouse = 0;
        HashSet<Integer> eSuspectSet = new HashSet<Integer>();
        HashSet<Integer> lSuspectSet = new HashSet<Integer>();

        for (String splitCase : splitCases) {
            boolean isEntrance = (splitCase.charAt(0) == 'E');
            int suspectNumber = Character.getNumericValue(splitCase.charAt(2));

            // Enter.
            if (isEntrance) {
                if (suspectNumber == 0) {
                    lSuspectSet.clear();
                } else {
                    if (eSuspectSet.contains(suspectNumber)) return CRIME_TIME;
                    eSuspectSet.add(suspectNumber);
                    lSuspectSet.remove(suspectNumber);
                }
                inHouse++;
            }

            // Left
            else {
                if (suspectNumber == 0) {
                    eSuspectSet.clear();
                } else {
                    if (lSuspectSet.contains(suspectNumber)) return CRIME_TIME;
                    eSuspectSet.remove(suspectNumber);
                    lSuspectSet.add(suspectNumber);
                }
                inHouse = Math.max(inHouse - 1, 0);
            }
        }
        return "" + inHouse;
    }

    private void printResult(String result) {
        System.out.println(result);
    }

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file));
            String line;
            CrimeHouse crimeHouse = new CrimeHouse();
            while ((line = buffer.readLine()) != null) {
                crimeHouse.handleLine(line.trim());
            }
        } finally {
            if (buffer != null) {
                buffer.close();
            }
        }
    }
}
