package ggoncalves.codeeval.hard.discountoffers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Main {

	private Map<String, Double> ssMap;
	private Map<String, Double> gcMap;
	private Map<String, List<LinkedList<Integer>>> permutationMap;

	public Main(String line) {
		String result = discountOffer(line);

		if (!result.trim().isEmpty()) {
			printResult(result);
		}
	}

	private int getGCD(int a, int b) {
		if (b == 0) {
			return a;
		}
		else {
			return getGCD(b, a % b);
		}
	}

	private double findCommonFactor(int a, int b) {// basically a GCD finder
		String key = "" + a + b;
		if (getGCMap().containsKey(key)) {
			getGCMap().get(key);
		}
		int gcd;
		gcd = getGCD(a, b);
		if (gcd > 1) {
			getGCMap().put(key, 1.5d);
			return 1.5;
		}
		getGCMap().put(key, 1.0d);
		return 1.0;
	}

	String discountOffer(String line) {
		if (line == null || line.trim().length() == 0) {
			return "";
		}

		String[] splitSemiColon = line.split(";");

		// Customers.
		String[] customers = splitSemiColon[0].trim().split(",");
		String[] products = splitSemiColon[1].trim().split(",");

		double maxOffer = 0;
		if (customers.length == products.length) {

			List<LinkedList<Integer>> prodPermList = createIndexPermutation(
			    products.length, customers.length - products.length);

			for (LinkedList<Integer> prodPermutation : prodPermList) {

				// String permutation = queue.poll();

				double offer = 0;
				for (int i = 0; i < customers.length; i++) {
					int index = prodPermutation.get(i);
					offer += calculateSuitabilityScore(customers[i], products[index]);

				}
				if (offer > maxOffer) {
					maxOffer = offer;
				}
			}

		}
		else if (customers.length > products.length) {

			List<LinkedList<Integer>> custPermList = createIndexPermutation(
			    customers.length, customers.length - products.length);

			for (LinkedList<Integer> custPermutation : custPermList) {

				// String permutation = queue.poll();

				double offer = 0;
				for (int i = 0; i < products.length; i++) {
					int index = custPermutation.get(i);
					offer += calculateSuitabilityScore(customers[index], products[i]);

				}
				if (offer > maxOffer) {
					maxOffer = offer;
				}
			}
		}
		else {
			// Case 3.
			List<LinkedList<Integer>> prodPermList = createIndexPermutation(
			    products.length, products.length - customers.length);

			for (LinkedList<Integer> prodPermutation : prodPermList) {

				// String permutation = queue.poll();

				double offer = 0;
				for (int i = 0; i < customers.length; i++) {
					int index = prodPermutation.get(i);
					offer += calculateSuitabilityScore(customers[i], products[index]);

				}
				if (offer > maxOffer) {
					maxOffer = offer;
				}
			}

		}

		return String.format("%.2f", maxOffer).replace(',', '.');
	}

	private Map<String, Double> getSSMap() {
		if (ssMap == null) {
			ssMap = new HashMap<String, Double>();
		}
		return ssMap;
	}

	private Map<String, Double> getGCMap() {
		if (gcMap == null) {
			gcMap = new HashMap<String, Double>();
		}
		return gcMap;
	}
	
	private Map<String, List<LinkedList<Integer>>> getPermutationMap() {
		if (permutationMap == null) {
			permutationMap = new HashMap<String, List<LinkedList<Integer>>>();
		}
		return permutationMap;
	}

	double calculateSuitabilityScore(String customer, String product) {

		String key = customer + product;

		if (getSSMap().containsKey(key)) {
			return getSSMap().get(key);
		}

		customer = customer.toLowerCase().replaceAll("[^a-z]", "");
		product = product.toLowerCase().replaceAll("[^a-z]", "");
		double ss = 0;

		int productLetters = product.length();
		String _cust = customer;
		int customerVowels = _cust.replaceAll("[^aeiouy]", "").length();
		_cust = customer;
		int customerConsonants = _cust.replaceAll("[aeiouy]", "").length();
		int customerLetters = customer.length();

		if (productLetters % 2 == 0) {
			ss = customerVowels * 1.5d;
		}
		else {
			ss = customerConsonants;
		}

		double multplier = findCommonFactor(customerLetters, productLetters);
		double result = ss * multplier;

		getSSMap().put(key, result);
		return result;

	}
	
	private List<LinkedList<Integer>> createIndexPermutation(int n, int ones) {
		String key = "" + n + "," + ones;
		if (getPermutationMap().containsKey(key)) {
			return getPermutationMap().get(key);
		}
		int[] num = new int[n];
		for (int i = 0; i < n; i++) {
			num[i] = i;
		}
		List<LinkedList<Integer>> result = permute(num, ones);
		getPermutationMap().put(key, result);
		return result;
	}

	public Main() {
	}

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				new Main(line);
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}

	public static List<LinkedList<Integer>> permute(int[] num, int ones) {
		List<LinkedList<Integer>> result = new LinkedList<LinkedList<Integer>>();
		permute(num, 0, result, ones);
		return result;
	}

	static void permute(int[] num, int start, List<LinkedList<Integer>> result,
	    int ones) {

		if (start >= num.length) {
			LinkedList<Integer> item = convertArrayToList(num, ones);
			result.add(item);
		}

		for (int j = start; j <= num.length - 1; j++) {
			swap2(num, start, j);
			permute(num, start + 1, result, ones);
			swap2(num, start, j);
		}
	}

	private static LinkedList<Integer> convertArrayToList(int[] num, int ones) {
		LinkedList<Integer> item = new LinkedList<Integer>();
		for (int h = 0; h < num.length - ones; h++) {
			item.add(num[h]);
		}
		return item;
	}

	private static void swap2(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public int countLetters(String s) {
		int letters = 0;

		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (isLetter(ch)) {
				letters++;
			}

		}
		return letters;
	}

	public int countConsonants(String s) {
		int consonants = 0;

		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (!isVowel(ch)) {
				consonants++;
			}

		}
		return consonants;
	}

	private boolean isLetter(char ch) {
		if ((ch == ' ') || (ch == '\n') || (ch == '\t')) {
			return false;
		}
		if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
			return true;
		}
		return false;
	}

	public int countVowels(String s) {
		int vowels = 0;

		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (isVowel(ch)) {
				vowels++;
			}

		}
		return vowels;
	}

	private boolean isVowel(char c) {
		return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y');
	}
}
