package ggoncalves.codeeval.hard.stringpermutations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

public class StringPermutationMain {
	
	private Queue<String> queue;

	public StringPermutationMain(String line) {
		String result = stringPermutation(line);
		
		
  	if (!result.trim().isEmpty()) {
  		printResult(result);
  	}
  }
	
	String stringPermutation(String line) {
		if (line == null || line.length() == 0) {
			return "";
		}
		
		permutation("", line);
		
		StringBuilder sb = new StringBuilder();
		while (!getResultQueue().isEmpty()) {
			sb.append(getResultQueue().poll() + ",");
		}
		
    return sb.toString().substring(0, sb.toString().length()-1);
	}
	
	private Queue<String> getResultQueue() {
		if (queue == null) {
			
	    queue = new PriorityQueue<String>();
	    
    }
    return queue;
	}
	
	private void permutation(String prefix, String s) {
		int n = s.length();
		if (n == 0) {
			getResultQueue().add(prefix);
//			System.out.println(prefix);
		}
		else {
			for (int i = 0; i < n; i++)
				permutation(prefix + s.charAt(i), s.substring(0, i) + s.substring(i + 1, n));
		}

	}
	
	
	
	public StringPermutationMain() {
  }

	void printResult(String result) {
		System.out.println(result);
	}
	
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				new StringPermutationMain(line);
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
