package ggoncalves.codeeval.hard.robotmove;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Tenta resolver o problema "Robot Movements".
 * <p/>
 * https://www.codeeval.com/public_sc/56/
 */
public class RobotMoveMain {

    private Map<Character, Set<Character>> movementsMap;

    public RobotMoveMain() {
        System.out.println(calculate());
    }

    // a  b  c  d
    // e  f  g  h
    // i  j  k  l
    // m  n  o  p
    int calculate() {
        boolean[] passed = new boolean[16];
        for (int i = 0; i < passed.length; i++) {
            passed[i] = false;
        }
        AtomicInteger ai = new AtomicInteger();
        permute('a', ai, passed);
        return ai.get();
    }

    private void permute(Character current, AtomicInteger atomicInteger, boolean[] passed) {
        if (current == 'p') {
            atomicInteger.addAndGet(1);
        } else {
            for (Character c : getMovementsMap().get(current)) {
                int index = getIndexFor(c);
                if (!passed[index]) {
                    passed[index] = true;
                    permute(c, atomicInteger, passed);
                    passed[index] = false;
                }
            }
        }
    }

    int getIndexFor(char c) {
        return (int) c - 97;
    }

    private Map<Character, Set<Character>> getMovementsMap() {
        if (movementsMap == null) {
            movementsMap = createMovementsMap();
        }
        return movementsMap;
    }

    private Map<Character, Set<Character>> createMovementsMap() {
        Map<Character, Set<Character>> map = new HashMap<Character, Set<Character>>();

        map.put('a', setFor('b', 'e'));
        map.put('b', setFor('c', 'f'));
        map.put('c', setFor('b', 'd', 'g'));
        map.put('d', setFor('c', 'h'));
        map.put('e', setFor('f', 'i'));
        map.put('f', setFor('b', 'e', 'g', 'j'));
        map.put('g', setFor('f', 'c', 'k', 'h'));
        map.put('h', setFor('g', 'd', 'l'));
        map.put('i', setFor('e', 'j', 'm'));
        map.put('j', setFor('i', 'f', 'n', 'k'));
        map.put('k', setFor('j', 'g', 'o', 'l'));
        map.put('l', setFor('k', 'h', 'p'));
        map.put('m', setFor('i', 'n'));
        map.put('n', setFor('m', 'j', 'o'));
        map.put('o', setFor('n', 'k', 'p'));

        return map;
    }

    private Map<Character, Set<Character>> createMovementsMap2() {
        Map<Character, Set<Character>> map = new HashMap<Character, Set<Character>>();

        map.put('a', setFor('b', 'c'));
        map.put('b', setFor('d'));
        map.put('c', setFor('d'));

        return map;
    }

    private Map<Character, Set<Character>> createMovementsMap3() {
        Map<Character, Set<Character>> map = new HashMap<Character, Set<Character>>();
        // a  b  c
        // d  e  f
        // g  h  i
        map.put('a', setFor('b', 'd'));
        map.put('b', setFor('c', 'e'));
        map.put('c', setFor('b', 'f'));
        map.put('d', setFor('g', 'e'));
        map.put('e', setFor('d', 'b', 'f', 'h'));
        map.put('f', setFor('e', 'c', 'i'));
        map.put('g', setFor('d', 'h'));
        map.put('h', setFor('g', 'e', 'i'));

        return map;
    }

    private Set<Character> setFor(char... a) {
        Set<Character> set = new HashSet<Character>();
        for (Character c : a) {
            set.add(c);
        }
        return set;
    }

    public static void main(String[] args) {
        new RobotMoveMain();
    }
}
