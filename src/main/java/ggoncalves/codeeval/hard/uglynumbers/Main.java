package ggoncalves.codeeval.hard.uglynumbers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Main {

	private List<String> resultList;
	private char[] c = { '-', '+', 'b' };

	public Main(String line) {
		String result = uglyNumbersCount(line);
		
		if (!result.trim().isEmpty()) {
			printResult(result);
		}
	}

	public Main() {

	}

	String uglyNumbersCount(String line) {
		if (line.length() == 1) {
			int i = Integer.parseInt(line);
			return "" + (( isUgly(i) ) ? 1 : 0); 
		}
		populatePermutation(c, line.length()-1, "");
		int uglyCount = 0;
		for (String permutation : getResultList()) {
	    String equation = mountEquation(line, permutation);
	    int res = solveEquation(equation);
	    if (isUgly(res)) {
	    	uglyCount++;
	    }
    }
		
		
		return "" + uglyCount;
	}

	void printResult(String result) {
		System.out.println(result);
	}
	
	private List<String> getResultList() {
		if (resultList == null) {
			resultList = new LinkedList<String>();
		}
		return resultList;
	}

	private void populatePermutation(char[] c, int n, String start) {
		if (start.length() >= n) {
			getResultList().add(start);
			return;
		}
		else {
			for (char x : c) {
				populatePermutation(c, n, start + x);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				new Main(line);
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}

	String mountEquation(String original, String mask) {
		
		StringBuilder sb = new StringBuilder();
		
		int strIndex = 0;
		sb.append(original.charAt(strIndex++));
		
		for (int i = 0; i < mask.length(); i++) {
	    
			if (mask.charAt(i) == 'b') {
				sb.append(original.charAt(strIndex++));
			}
			else {
				sb.append(mask.charAt(i));
				sb.append(original.charAt(strIndex++));
			}
			
    }
	  return sb.toString();
  }

	int solveEquation(String equation) {
		
		String[] strSplit = equation.split("\\+|\\-");
		
		String signals = "";
		for (char c : equation.toCharArray()) {
			if (c == '+' || c == '-') {
				signals = signals + c;
			}
		}
		
		int acc = Integer.parseInt(strSplit[0]);
		
		int signalIndex = 0;
		for (int i = 1; i < strSplit.length; i++) {
	    if (signals.charAt(signalIndex++) == '+') {
	    	acc += Integer.parseInt(strSplit[i]);
	    }
	    else {
	    	acc -= Integer.parseInt(strSplit[i]);
	    }
    }
		
		return acc;
		
  }

	boolean isUgly(int number) {
		if (number == 0) {
			return true;
		}
		
		if (number%2 == 0 || number%3 == 0 || number%5 == 0 || number%7 == 0) {
			return true;
		}
		
	  return false;
  }
}
