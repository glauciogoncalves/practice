package ggoncalves.codeeval.hard.longestCommonSubs;

import java.io.*;
public class Main {
	
    public Main(String line) {
    	String result = printLongest(line);

    	if (!result.trim().isEmpty()) {
    		printResult(result);
    	}
    }
    
    public Main() {
    	
    }
    
    String printLongest(String line) {
        String split[] = line.split(";");
        
        String minWord = (split[0].length() < split[1].length()) ? split[0] : split[1];
        String maxWord = (split[0].length() >= split[1].length()) ? split[0] : split[1];
        
        StringBuilder sb = new StringBuilder();
        
        for (int i=0; i<minWord.length(); i++) {
        	
        	// Case 1, equals():
        	if (minWord.charAt(i) == maxWord.charAt(i)) {
        		sb.append(minWord.charAt(i));
        	}
        	
        	// Case 2, right up cross.
        	else if (i + 1 < maxWord.length() && minWord.charAt(i) == maxWord.charAt(i+1)) {
        		sb.append(minWord.charAt(i));
        	}
        	
        	// Case 3, right down cross.
        	else if (i + 1 < minWord.length() && minWord.charAt(i+1) == maxWord.charAt(i)) {
        		sb.append(maxWord.charAt(i));
        	}
        	
        	
        }
        
        
        return sb.toString();
    }
    
    void printResult(String result) {
  		System.out.println(result);
  	}

  	public static void main(String[] args) throws IOException {
  		File file = new File(args[0]);
  		BufferedReader buffer = null;
  		try {
  			buffer = new BufferedReader(new FileReader(file));
  			String line;
  			while ((line = buffer.readLine()) != null) {
  				line = line.trim();
  				new Main(line);
  			}
  		}
  		finally {
  			if (buffer != null) {
  				buffer.close();
  			}
  		}
  	}
}
