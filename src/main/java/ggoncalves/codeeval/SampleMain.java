package ggoncalves.codeeval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SampleMain {

    public SampleMain() {
    }

    void handleLine(String line) {
        String result = sampleMethod(line);

        if (!result.isEmpty()) {
            printResult(result);
        }
    }

    String sampleMethod(String line) {
        return null;
    }

    void printResult(String result) {
        System.out.println(result);
    }

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file));
            String line;
            SampleMain main = new SampleMain();
            while ((line = buffer.readLine()) != null) {
                main.handleLine(line.trim());
            }
        } finally {
            if (buffer != null) {
                buffer.close();
            }
        }
    }
}
