package ggoncalves.codeeval.easy.detectingcycles;

import java.io.*;
import java.util.LinkedList;

public class Main {

	public Main(String line) {
		String result = detectCycle(line);

		printResult(result);
	}
	
	public Main() {
  }

	String detectCycle(String line) {
		
		// Parse to an int array
		int[] array = toIntegerArray(line);
		
		LinkedList<Integer> read = new LinkedList<Integer>();
		LinkedList<Integer> lList = new LinkedList<Integer>();
		
		for (int i = 0; i < array.length; i++) {
			lList.add(array[i]);
		}
		
		for (int i = 0; i < lList.size(); i++) {
	    
			if (!read.isEmpty() && lList.get(i).equals(read.getLast())) {
				return "" + lList.get(i);
			}
			
			
			// Elemento não está na lista.
			
			if (!read.contains(lList.get(i))) {
				
				read.add(lList.get(i));
				
			}
			
			else {

				int indexRead = read.indexOf(lList.get(i));
				
				if (read.size() - indexRead <= 50) {
					
					// look ahead.
					int lIndex = i;
					int firstIndex = indexRead;
					int count = 0;
					while (indexRead < read.size()) {
						if (read.get(indexRead++).equals(lList.get(lIndex++))) {
							count++;
						}
					}
					if (count == read.size() - firstIndex) {
						return createSublist(read, firstIndex);
					}
					
				}
				
			}
			
    }
		
		return "";
	}
	
	private String createSublist(LinkedList<Integer> read, int fromIndex) {
		StringBuilder sb = new StringBuilder();
		for (int i=fromIndex; i<read.size(); i++) {
			sb.append(read.get(i) + " ");
		}
		return sb.toString().trim();
	}

	// Subir se precisar
	private int[] toIntegerArray(String line) {
	  String[] split = line.split(" ");
	  int[] array = new int[split.length];
	  for (int i = 0; i < split.length; i++) {
	    array[i] = Integer.parseInt(split[i]);
    }
	  return array;
  }

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				new Main(line);
				System.out.println("");
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
