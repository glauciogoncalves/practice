package ggoncalves.codeeval.easy.detectingcycles;

public class Solution {
    public boolean canJump(int[] nums) {
        if (nums.length == 0) return false;
        if (nums.length == 1) return true;
        int next = 0;
        int index = next;
        int lastIndex = nums.length-1;
        while (true) {
            if (index >= lastIndex) {
                return true;
            }

            int top = index + nums[index];
            int max = top;
            next = top;
            for (int i=nums[index]; i>0; i--) {
                if (i + index >= lastIndex) {
                    return true;
                }
                if (index + i + nums[i + index] > max) {
                    next = i + index;
                    max = index + i + nums[i = index];
                }
            }
            if (nums[next] == 0) {
                return false;
            }
            index = next;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,1,2,2,0,1,1};
        System.out.println(new Solution().canJump(nums));
    }
}