package ggoncalves.codeeval.easy.fizzbuzz;

import java.io.*;

public class Main {

	public Main(String line) {
		String result = fizzBuzz(line);

		printResult(result);
	}
	
	public Main() {
  }

	String fizzBuzz(String line) {
		
		// Parse to an int array
		int[] array = toIntegerArray(line);
		
		int fizzN = array[0];
		int buzzN = array[1];
		int size = array[2];
		
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i <= size; i++) {

			String r = "" + i;
			
			if (i%fizzN == 0 || i%buzzN == 0 ) {
				
				if (i%fizzN == 0 && i%buzzN == 0 ) {
					r = "FB";
				}
				
				else if (i%fizzN == 0) {
					r = "F";
				}
				else {
					r = "B";
				}
				
			}
			
			sb.append(r + " ");
    }
		return sb.toString().trim();
	}

	// Subir se precisar
	private int[] toIntegerArray(String line) {
	  String[] split = line.split(" ");
	  int[] array = new int[split.length];
	  for (int i = 0; i < split.length; i++) {
	    array[i] = Integer.parseInt(split[i]);
    }
	  return array;
  }

	void printResult(String result) {
		System.out.println(result);
	}

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(file));
			String line;
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				new Main(line);
				System.out.println("");
			}
		}
		finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
}
