package ggoncalves.leetcode.daillychallenge.nov2020.accepted;

import java.util.HashSet;
import java.util.Set;

public class UniqueMorseCodeWords {

  public int uniqueMorseRepresentations(String[] words) {
    String[] charset = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

    int offset = 'a';

    Set<String> uniqueMorseSet = new HashSet<>();

    for (String word : words) {

      StringBuilder sb = new StringBuilder();

      for (int i = 0; i < word.length(); i++) {

        int index = word.charAt(i) - offset;
        sb.append(charset[index]);

      }
      uniqueMorseSet.add(sb.toString());
    }
    return uniqueMorseSet.size();
  }

}
