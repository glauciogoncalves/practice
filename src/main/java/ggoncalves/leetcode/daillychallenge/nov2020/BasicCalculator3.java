package ggoncalves.leetcode.daillychallenge.nov2020;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BasicCalculator3 {


  Stack<Integer> stack = new Stack<>();
  Stack<Integer> multiStack = new Stack<>();

  public int calculate(String s) {
    String exp = s.replaceAll(" ", "");

    List<Integer> numberList = new ArrayList<>();
    List<Character> signalList = new ArrayList<>();
    Stack<Integer> index2DeleteStack = new Stack<>();

    Integer currentNumber = 0;

    for (int i = 0; i < exp.length(); i++) {

      Character c = exp.charAt(i);
      if (c == '+' || c == '-' || c == '/' || c == '*') {
        numberList.add(currentNumber);
        signalList.add(c);
        currentNumber = 0;
      } else {
        currentNumber = currentNumber * 10 + Integer.parseInt(c.toString());
      }
    }
    numberList.add(currentNumber);

    for (int i = 0; i < signalList.size(); i++) {

      Character signal = signalList.get(i);

      if (signal.equals('*') || signal.equals('/')) {

        Integer n1 = numberList.get(i);
        Integer n2 = numberList.get(i + 1);

        n1 = (signal.equals('*')) ? n1 * n2 : n1 / n2;
        numberList.set(i, n1);
        numberList.set(i + 1, n1);
        index2DeleteStack.push(i);
      }
    }

    while (!index2DeleteStack.isEmpty()) {
      int index = index2DeleteStack.pop();
      numberList.remove(index);
      signalList.remove(index);
    }

    for (int i = 0; i < signalList.size(); i++) {

      Character signal = signalList.get(i);

      if (signal.equals('+') || signal.equals('-')) {
        Integer n1 = numberList.get(i);
        Integer n2 = numberList.get(i + 1);

        n1 = (signal.equals('+')) ? n1 + n2 : n1 - n2;
        numberList.set(i, n1);
        numberList.set(i + 1, n1);
        index2DeleteStack.push(i);
      }

    }

    while (!index2DeleteStack.isEmpty()) {
      numberList.remove((int) index2DeleteStack.pop());
    }

    return numberList.get(0);
  }

  // Should be
  public int calculate2(String s) {
    if (s == null || s.length() == 0) return 0;
    char sign = '+';
    int start = 0, res = 0;
    Stack<Integer> stack = new Stack<>();
    while (start < s.length()) {
      if (Character.isDigit(s.charAt(start))) {
        res *= 10;
        res += s.charAt(start) - '0';
      }
      if ((!Character.isDigit(s.charAt(start)) && s.charAt(start) != ' ') || start == s.length() - 1) {
        if (sign == '+') {
          stack.push(res);
        } else if (sign == '-') {
          stack.push(-res);
        } else if (sign == '*') {
          stack.push(stack.pop() * res);
        } else if (sign == '/') {
          stack.push(stack.pop() / res);
        }
        res = 0;
        sign = s.charAt(start);
      }
      start++;
    }

    while (stack.size() > 0) res += stack.pop();

    return res;
  }

  public static void main(String[] args) {
    int calculate = new BasicCalculator3().calculate("3+2+1");
    System.out.println("Result 3+2+1: " + calculate);

    calculate = new BasicCalculator3().calculate("5-2-1");
    System.out.println("Result 5-2-1: " + calculate);

    calculate = new BasicCalculator3().calculate("3*2*1*2");
    System.out.println("Result 3*2*1*2: " + calculate);

    calculate = new BasicCalculator3().calculate("3*2 + 1*2");
    System.out.println("Result 3*2 + 1*2: " + calculate);

    calculate = new BasicCalculator3().calculate("3+2*2");
    System.out.println("Result 3+2*2: " + calculate);

    calculate = new BasicCalculator3().calculate("3/2");
    System.out.println("Result 3/2: " + calculate);

    calculate = new BasicCalculator3().calculate(" 3+5 / 2 ");
    System.out.println("Result  3+5 / 2 : " + calculate);
  }
}
