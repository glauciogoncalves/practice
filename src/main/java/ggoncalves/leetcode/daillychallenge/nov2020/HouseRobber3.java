package ggoncalves.leetcode.daillychallenge.nov2020;

import java.util.HashMap;
import java.util.Map;

// https://leetcode.com/explore/challenge/card/november-leetcoding-challenge/567/week-4-november-22nd-november-28th/3541/
public class HouseRobber3 {


  public static class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
      this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
      this.val = val;
      this.left = left;
      this.right = right;
    }
  }

  public int rob(TreeNode root) {
    Map<Integer, Integer> layerSum = new HashMap<>();
    postOrder(root, 0, layerSum);

    return findMaxSum(layerSum);
  }

  private int findMaxSum(Map<Integer, Integer> layerSum) {
    Integer[] layers = layerSum.values().toArray(new Integer[0]);

    if (layers.length == 1) {
      return layers[0];
    } else if (layers.length == 2) {
      return Math.max(layers[0], layers[1]);
    }

    for (int i = 2; i < layers.length; i++) {
      layers[i] = Math.max(layers[i - 1], (layers[i] + layers[i - 2]));
    }

    return layers[layers.length - 1];
  }

  private void postOrder(TreeNode node, Integer layer, Map<Integer, Integer> layerSum) {
    if (node == null) {
      return;
    }

    postOrder(node.left, layer + 1, layerSum);
    postOrder(node.right, layer + 1, layerSum);

    Integer currentValue = layerSum.containsKey(layer) ? layerSum.get(layer) : 0;
    layerSum.put(layer, currentValue + node.val);

  }

}


