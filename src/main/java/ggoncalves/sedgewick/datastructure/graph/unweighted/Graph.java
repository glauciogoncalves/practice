package ggoncalves.sedgewick.datastructure.graph.unweighted;

import java.util.AbstractMap;

public interface Graph<T extends Comparable<T>> {

  void addEdge(T v, T w);

  Iterable<T> adj(T v);

  Integer degree(T v);

  AbstractMap.SimpleEntry<T, Integer> maxDegree();

  Integer getVerticeSize();

  Integer getEdgeSize();

}
