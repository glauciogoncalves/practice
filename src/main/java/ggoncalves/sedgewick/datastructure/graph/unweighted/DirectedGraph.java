package ggoncalves.sedgewick.datastructure.graph.unweighted;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class DirectedGraph implements Graph<Integer> {

  private final LinkedList<Integer>[] adj;

  public DirectedGraph(Integer verticeSize) {
    if (verticeSize == null || verticeSize.compareTo(0) <= 0) {
      throw new IllegalArgumentException();
    }
    adj = (LinkedList<Integer>[]) new LinkedList[verticeSize];
    initialize();
  }

  private void initialize() {
    for (int i = 0; i < adj.length; i++) {
      adj[i] = new LinkedList<>();
    }
  }

  @Override
  public void addEdge(Integer v, Integer w) {
    assertVertice(v);
    assertVertice(w);
    if (!adj[v].contains(w)) adj[v].add(w);
  }

  private void assertVertice(Integer vertice) {
    if (vertice == null || vertice.compareTo(0) < 0 || vertice.compareTo(getVerticeSize()) >= 0) {
      throw new IllegalArgumentException("Vertice must be between 0 and " + (getVerticeSize() - 1));
    }
  }

  @Override
  public Iterable<Integer> adj(Integer v) {
    assertVertice(v);
    return adj[v];
  }

  @Override
  public Integer degree(Integer v) {
    assertVertice(v);
    return adj[v].size();
  }

  @Override
  public AbstractMap.SimpleEntry<Integer, Integer> maxDegree() {
    int maxDegreeVertice = -1;
    int maxDegree = -1;
    for (int v = 0; v < adj.length; v++) {
      if (adj[v].size() > maxDegree) {
        maxDegreeVertice = v;
        maxDegree = adj[v].size();
      }
    }
    return new AbstractMap.SimpleEntry<>(maxDegreeVertice, maxDegree);
  }

  @Override
  public Integer getVerticeSize() {
    return adj.length;
  }

  @Override
  public Integer getEdgeSize() {
    // O(E)
    int totalEdges = 0;
    for (int i = 0; i < adj.length; i++) totalEdges += adj[i].size();
    return totalEdges;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int v = 0; v < getVerticeSize(); v++) {

      String separatedNumbers = adj[v].stream()
          .map(Object::toString)
          .collect(Collectors.joining(", "));

      if (separatedNumbers.isEmpty()) separatedNumbers = "empty";

      sb.append(String.format("[%s] = %s\n", v, separatedNumbers));
    }
    return sb.toString();
  }
}
