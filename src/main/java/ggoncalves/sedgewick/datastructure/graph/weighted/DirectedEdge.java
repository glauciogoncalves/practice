package ggoncalves.sedgewick.datastructure.graph.weighted;

public class DirectedEdge extends AbstractWeightedEdge {

  private final int v;
  private final int w;

  DirectedEdge(int v, int w, double weight) {
    super(weight);
    assertVertice(v);
    assertVertice(w);
    this.v = v;
    this.w = w;
  }

  public int from() {
    return this.v;
  }

  public int to() {
    return this.w;
  }

  @Override
  public String toString() {
    return "Edge { v:" + v + " ---- " + getWeight() + " -----> w:" + w;
  }
}
