package ggoncalves.sedgewick.datastructure.graph.weighted;

public abstract class AbstractWeightedEdge implements WeightedEdge, Comparable<AbstractWeightedEdge> {

  private final double weight;

  AbstractWeightedEdge(double weight) {
    this.weight = weight;
  }

  @Override
  public double getWeight() {
    return this.weight;
  }

  public abstract String toString();

  @Override
  public int compareTo(AbstractWeightedEdge o) {
    return Double.compare(this.weight, o.weight);
  }

  protected void assertVertice(int v) {
    if (v < 0) throw new IllegalArgumentException("Negative Vertice Not Allowed");
  }
}
