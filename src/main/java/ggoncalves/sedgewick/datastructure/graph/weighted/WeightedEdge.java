package ggoncalves.sedgewick.datastructure.graph.weighted;

public interface WeightedEdge {

  double getWeight();

  String toString();
}
