package ggoncalves.sedgewick.datastructure.graph.weighted;

public class Edge extends AbstractWeightedEdge {

  private final int v;
  private final int w;

  public Edge(int v, int w, double weight) {
    super(weight);
    assertVertice(v);
    assertVertice(w);
    this.v = v;
    this.w = w;
  }

  public int either() {
    return v;
  }

  public int other(int vertex) {
    assertVertice(vertex);
    if (v == vertex) return w;
    return v;
  }

  @Override
  public String toString() {
    return "Edge { v:" + v + " ---- " + getWeight() + " -----> w:" + w;
  }

}
