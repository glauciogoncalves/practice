package ggoncalves.sedgewick.datastructure.graph.weighted;

public interface WeightedGraph<T extends AbstractWeightedEdge> {

  void addEdge(T edge);

  Iterable<T> adj(int v);

  Integer getVerticeSize();

  Integer getEdgeSize();

}
