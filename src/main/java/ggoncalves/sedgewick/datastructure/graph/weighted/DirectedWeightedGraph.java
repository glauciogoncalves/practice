package ggoncalves.sedgewick.datastructure.graph.weighted;

import java.util.AbstractMap;
import java.util.LinkedList;

public class DirectedWeightedGraph implements WeightedGraph<DirectedEdge> {

  private final LinkedList<DirectedEdge>[] adj;

  public DirectedWeightedGraph(int verticeSize) {
    if (verticeSize <= 0) throw new IllegalArgumentException();
    adj = new LinkedList[verticeSize];
    for (int i = 0; i < verticeSize; i++) adj[i] = new LinkedList<>();
  }

  @Override
  public void addEdge(DirectedEdge edge) {
    adj[edge.from()].add(edge);
  }

  @Override
  public Iterable<DirectedEdge> adj(int v) {
    return adj[v];
  }

  @Override
  public Integer getVerticeSize() {
    return adj.length;
  }

  @Override
  public Integer getEdgeSize() {
    int edgeCount = 0;
    for (LinkedList<DirectedEdge> edges : adj) edgeCount += edges.size();
    return edgeCount;
  }

  public int degree(int v) {
    return adj[v].size();
  }

  public AbstractMap.SimpleEntry<Integer, Integer> maxDegree() {
    int maxDegreeV = -1;
    int maxDegree = -1;
    for (int i = 0; i < adj.length; i++) {
      if (adj[i].size() > maxDegree) {
        maxDegreeV = i;
        maxDegree = adj[i].size();
      }
    }
    return new AbstractMap.SimpleEntry<>(maxDegreeV, maxDegree);
  }
}
