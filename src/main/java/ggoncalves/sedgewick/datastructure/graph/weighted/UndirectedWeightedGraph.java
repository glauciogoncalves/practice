package ggoncalves.sedgewick.datastructure.graph.weighted;

import java.util.AbstractMap;
import java.util.LinkedList;

public class UndirectedWeightedGraph implements WeightedGraph<Edge> {

  private final LinkedList<Edge>[] adj;

  public UndirectedWeightedGraph(int verticeSize) {
    if (verticeSize <= 0) throw new IllegalArgumentException();
    adj = new LinkedList[verticeSize];
    for (int i = 0; i < verticeSize; i++) adj[i] = new LinkedList<>();
  }

  @Override
  public void addEdge(Edge edge) {
    int v = edge.either();
    int w = edge.other(v);
    adj[v].add(edge);
    adj[w].add(edge);
  }

  @Override
  public Iterable<Edge> adj(int v) {
    return adj[v];
  }

  @Override
  public Integer getVerticeSize() {
    return adj.length;
  }

  @Override
  public Integer getEdgeSize() {
    int edgeCount = 0;
    for (LinkedList<Edge> edges : adj) edgeCount += edges.size();
    return edgeCount / 2;
  }

  public int degree(int v) {
    return adj[v].size();
  }

  public AbstractMap.SimpleEntry<Integer, Integer> maxDegree() {
    int maxDegreeV = -1;
    int maxDegree = -1;
    for (int i = 0; i < adj.length; i++) {
      if (adj[i].size() > maxDegree) {
        maxDegreeV = i;
        maxDegree = adj[i].size();
      }
    }
    return new AbstractMap.SimpleEntry<>(maxDegreeV, maxDegree);
  }
}
