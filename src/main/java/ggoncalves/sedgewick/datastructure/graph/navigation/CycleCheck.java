package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.unweighted.DirectedGraph;

import java.util.HashSet;

public class CycleCheck {

  private final DirectedGraph directedGraph;
  private final boolean[] visited;

  public CycleCheck(DirectedGraph directedGraph) {
    this.directedGraph = directedGraph;
    visited = new boolean[directedGraph.getVerticeSize()];
  }

  public boolean hasCycle() {
    HashSet<Integer> set = new HashSet<>();
    for (int i = directedGraph.getVerticeSize() - 1; i >= 0; i--) {
      if (!visited[i]) {
        if (dfs(i, set)) return true;
      }
    }
    return false;
  }

  private boolean dfs(Integer v, HashSet<Integer> currentRun) {
    visited[v] = true;
    currentRun.add(v);

    for (Integer w : directedGraph.adj(v)) {
      if (currentRun.contains(w)) return true;
      if (!visited[w]) {
        if (dfs(w, currentRun)) return true;
      }
    }
    currentRun.remove(v);
    return false;
  }
}
