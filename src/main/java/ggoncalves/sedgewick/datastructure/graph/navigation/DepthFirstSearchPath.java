package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.unweighted.Graph;

import java.util.Stack;

public class DepthFirstSearchPath implements Paths {

  private final Graph<Integer> graph;
  private final boolean[] marked;
  private final int[] edgeTo;
  private final Integer s;

  public DepthFirstSearchPath(Graph<Integer> graph, Integer s) {
    this.graph = graph;
    assertVertice(s);
    this.s = s;
    this.marked = new boolean[graph.getVerticeSize()];
    this.edgeTo = new int[graph.getVerticeSize()];
  }

  private void assertVertice(Integer vertice) {
    if (vertice == null || vertice.compareTo(0) < 0 || vertice.compareTo(graph.getVerticeSize()) >= 0) {
      throw new IllegalArgumentException("Vertice must be between 0 and " + (graph.getVerticeSize() - 1));
    }
  }

  @Override
  public boolean hasPathTo(Integer v) {
    return marked[v];
  }

  @Override
  public Iterable<Integer> pathTo(Integer v) {
    Stack<Integer> st = new Stack<>();
    if (!hasPathTo(v)) return st;

    int k = v;
    while (k != s) {
      st.push(k);
      k = edgeTo[k];
    }
    st.push(s);
    return st;
  }

  @Override
  public void run() {
    dfs(graph, s);
  }

  private void dfs(Graph<Integer> g, Integer v) {
    marked[v] = true;
    for (Integer w : g.adj(v)) {
      if (!marked[w]) {
        dfs(g, w);
        edgeTo[w] = v;
      }
    }
  }
}
