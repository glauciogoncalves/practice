package ggoncalves.sedgewick.datastructure.graph.navigation;

public interface Paths extends Runnable {

  boolean hasPathTo(Integer v);

  Iterable<Integer> pathTo(Integer v);

}
