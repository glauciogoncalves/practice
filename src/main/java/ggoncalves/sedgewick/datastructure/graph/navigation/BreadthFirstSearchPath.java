package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.unweighted.Graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BreadthFirstSearchPath implements Paths {

  private final Graph<Integer> graph;
  private final boolean[] marked;
  private final int[] edgeTo;
  private final int[] distTo;
  private final Integer s;

  public BreadthFirstSearchPath(Graph<Integer> graph, Integer s) {
    this.graph = graph;
    assertVertice(s);
    this.s = s;
    this.marked = new boolean[graph.getVerticeSize()];
    this.edgeTo = new int[graph.getVerticeSize()];
    this.distTo = new int[graph.getVerticeSize()];
  }

  private void assertVertice(Integer vertice) {
    if (vertice == null || vertice.compareTo(0) < 0 || vertice.compareTo(graph.getVerticeSize()) >= 0) {
      throw new IllegalArgumentException("Vertice must be between 0 and " + (graph.getVerticeSize() - 1));
    }
  }

  @Override
  public boolean hasPathTo(Integer v) {
    return marked[v];
  }

  @Override
  public Iterable<Integer> pathTo(Integer v) {
    Stack<Integer> st = new Stack<>();
    if (!hasPathTo(v)) return st;

    int k = v;
    while (k != s) {
      st.push(k);
      k = edgeTo[k];
    }
    st.push(s);
    return st;
  }

  public Integer distTo(Integer v) {
    assertVertice(v);
    return distTo[v];
  }

  @Override
  public void run() {
    bfs(graph, s);
  }

  private void bfs(Graph<Integer> g, Integer s) {
    Queue<Integer> queue = new LinkedList<>();

    int dist = 0;
    distTo[s] = 0;
    queue.offer(s);
    marked[s] = true;

    while (!queue.isEmpty()) {

      Integer v = queue.poll();
      for (Integer w : g.adj(v)) {

        if (!marked[w]) {

          queue.offer(w);
          distTo[w] = distTo[v] + 1;
          edgeTo[w] = v;
          marked[w] = true;
        }
      }
    }
  }
}
