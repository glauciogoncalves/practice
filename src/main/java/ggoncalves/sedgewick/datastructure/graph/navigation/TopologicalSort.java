package ggoncalves.sedgewick.datastructure.graph.navigation;

import ggoncalves.sedgewick.datastructure.graph.unweighted.DirectedGraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;

public class TopologicalSort implements Runnable {

  private final DirectedGraph graph;
  private final boolean[] visited;
  private final Stack<Integer> orderStack = new Stack<>();
  private final List<Integer> order;

  // TODO Create interface for DirectedGraph and DirectedWeithedGraph
  public TopologicalSort(DirectedGraph graph) {
    this.graph = graph;
    this.visited = new boolean[graph.getVerticeSize()];
    order = new ArrayList<>(graph.getVerticeSize() + 2);
  }

  public Collection<Integer> order() {
    return order;
  }

  private void dfs(int v) {
    if (visited[v]) return;
    visited[v] = true;

    for (Integer w : graph.adj(v)) {
      if (!visited[w]) dfs(w);
    }
    orderStack.push(v);
  }

  @Override
  public void run() {
    for (int i = 0; i < graph.getVerticeSize(); i++) dfs(i);
    while (!orderStack.isEmpty()) order.add(orderStack.pop());
  }
}
