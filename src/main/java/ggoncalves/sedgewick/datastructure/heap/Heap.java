package ggoncalves.sedgewick.datastructure.heap;

import static ggoncalves.sedgewick.helpers.ArrayHelper.exchange;
import static ggoncalves.sedgewick.helpers.ArrayHelper.isLess;

public final class Heap<T extends Comparable<T>> {

  static final int DEFAULT_INITIAL_CAPACITY = 100;

  private final T[] array;

  private int N = 0;

  public Heap(T[] elements) {
    array = (T[]) new Comparable[2 * elements.length + 1];
    System.arraycopy(elements, 0, array, 1, elements.length);
    N = elements.length;
    heapify();
  }

  public Heap(int capacity) {
    array = (T[]) new Comparable[capacity + 1];
  }

  public Heap() {
    this(DEFAULT_INITIAL_CAPACITY);
  }

  public int size() {
    return N;
  }

  public void add(T element) {
    array[++N] = element;
    swin(N);
  }

  public T peek() {
    if (N == 0) return null;
    return array[1];
  }

  public T poll() {
    if (N == 0) return null;
    T e = array[1];
    exchange(array, 1, N--);
    array[N + 1] = null;
    sink(1);
    return e;
  }

  public T[] getArray() {
    return array;
  }

  void heapify() {
    for (int k = N / 2; k >= 1; k--) {
      sink(k);
    }
  }

  private void swin(int k) {
    while (k > 1 && isLess(array, k / 2, k)) {
      exchange(array, k, k / 2);
      k = k / 2;
    }
  }

  private void sink2(int k) {
    while (2 * k <= N) {
//    while (k <= N/2) {
      int j = 2 * k;
      if (j < N && isLess(array, j, j + 1)) j++;
      if (!isLess(array, k, j)) break;
      exchange(array, k, j);
      k = j;
    }
  }

  private void sink(int k) {
    while (k <= N / 2) {

      if (2 * k > N) continue;

      int c1 = 2 * k;
      int c2 = 2 * k + 1;

      T child1 = array[c1];
      T child2 = null;

      if (c2 <= N) child2 = array[c2];

      if (child1 == null && child2 == null) break;

      if (child1 == null || child2 == null) {

        if (child1 != null && isLess(array, k, c1)) {
          exchange(array, k, c1);
          k = c1;
        } else if (child2 != null && isLess(array, k, c2)) {
          exchange(array, k, c2);
          k = c2;
        } else break;

      } else if (isLess(array, k, c1) || isLess(array, k, c2)) {
        if (isLess(array, c2, c1)) {
          exchange(array, k, c1);
          k = c1;
        } else {
          exchange(array, k, c2);
          k = c2;
        }
      } else break;
    }
  }

  public boolean isEmpty() {
    return N == 0;
  }
}
