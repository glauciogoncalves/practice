package ggoncalves.sedgewick.datastructure.trie;

public class Trie<T> {

  private static final int RADIX = 26;

  private final Node<T> root;

  public Trie() {
    root = new Node();
  }

  Node getRoot() {
    return root;
  }

  public void put(String key, T val) {
    put(root, key, 0, val);
  }

  private Node<T> put(Node<T> x, String key, Integer d, T val) {
    if (x == null) x = new Node<>();
    if (d == key.length() - 1) {
      x.val = val;
      return x;
    }
    char c = key.charAt(d);
    x.next[c - 97] = put(x.next[c - 97], key, d + 1, val);
    return x;
  }

  public boolean contains(String key) {
    return (get(key) != null);
  }

  public T get(String key) {
    Node node = get(root, key, 0);
    return (node == null) ? null : (T) node.val;

  }

  private Node get(Node x, String key, int d) {
    if (x == null) return null;
    char c = key.charAt(d);
    if (d == key.length() - 1) return x;
    return get(x.next[c - 97], key, d + 1);
  }

  private Node delete(Node x, String key, int d) {
    if (x == null) return null;
    char c = key.charAt(d);
    if (d == key.length() - 1) {
      if (x.hasNext()) {
        x.val = null;
        return x;
      }
      return null;
    }
    x.next[c - 97] = delete(x.next[c - 97], key, d + 1);
    return (x.val == null && !x.hasNext()) ? null : x;
  }

  public Node delete(String key) {
    return delete(root, key, 0);
  }


  class Node<T> {
    private T val;
    private final Node[] next = new Node[RADIX];

    Node[] getNext() {
      return next;
    }

    boolean hasNext() {
      for (Node n : next) if (n != null) return true;
      return false;
    }
  }

}
