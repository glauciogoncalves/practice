package ggoncalves.sedgewick.client;

import java.util.Random;

public class ExerciseShuffler {

  private final String[] exercises;

  private final Random random = new Random();

  public ExerciseShuffler() {
    this.exercises = new String[]{
        "QuickSort",
        "QuickSelect",
        "MergeSort",
        "CountingSort",
        "Min/Max Heap",
        "3-Way QuickSort",
        "Topological Order",
        "Heapify",
        "Tries"
    };
  }

  private String today() {
    return exercises[random.nextInt(exercises.length)];
  }

  private String week() {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < exercises.length; i++) exch(exercises, i, random.nextInt(exercises.length));
    for (int i = 0; i < exercises.length; i++) sb.append((i + 1)).append(": ").append(exercises[i]).append("\n");
    return sb.toString();
  }

  private void exch(String[] arr, int i, int j) {
    if (i == j) return;
    String aux = arr[i];
    arr[i] = arr[j];
    arr[j] = aux;
  }

  public static void main(String[] args) {
    ExerciseShuffler shuffler = new ExerciseShuffler();
    System.out.println(shuffler.today());
    System.out.println(shuffler.week());
  }
}
