package ggoncalves.sedgewick.sorting;

public abstract class AbstractSorter<T extends Comparable<T>> implements Sorter<T> {

  protected <T extends Comparable<T>> void exchange(T[] arr, int i, int j) {
    T aux = arr[i];
    arr[i] = arr[j];
    arr[j] = aux;
  }

}
