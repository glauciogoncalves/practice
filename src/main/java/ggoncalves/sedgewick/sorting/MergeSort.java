package ggoncalves.sedgewick.sorting;

public class MergeSort<T extends Comparable<T>> extends AbstractSorter<T> {

  // N log N guaranted
  // Stable
  @Override
  public void sort(T[] arr) {
    T[] aux = (T[]) new Comparable[arr.length]; // <- ugly cast
    mergeSort(arr, aux, 0, arr.length - 1);
  }

  private void mergeSort(T[] arr, T[] aux, int lo, int hi) {
    if (lo >= hi) return;
    int mid = lo + (hi - lo) / 2;
    mergeSort(arr, aux, lo, mid);
    mergeSort(arr, aux, mid + 1, hi);
    if (arr[mid].compareTo(arr[mid + 1]) < 0) return;
    merge(arr, aux, lo, mid, hi);
  }

  private void merge(T[] arr, T[] aux, int lo, int mid, int hi) {

    for (int k = lo; k <= hi; k++) {
      aux[k] = arr[k];
    }

    int i = lo;
    int j = mid + 1;
    for (int k = lo; k <= hi; k++) {

      if (i > mid) arr[k] = aux[j++];
      else if (j > hi) arr[k] = aux[i++];
      else if (aux[i].compareTo(aux[j]) > 0) arr[k] = aux[j++];
      else arr[k] = aux[i++];
    }

  }
}
