package ggoncalves.sedgewick.sorting;

public class SelectionSort<T extends Comparable<T>> extends AbstractSorter<T> {

  // N2 Running time at all cases
  // Linear N exchanges guaranted
  @Override
  public void sort(T[] arr) {

    for (int i = 0; i < arr.length - 1; i++) {

      int min = i;

      for (int j = i + 1; j < arr.length; j++) {

        if (arr[j].compareTo(arr[min]) < 0) {
          min = j;
        }

      }
      exchange(arr, i, min);
    }

  }
}
