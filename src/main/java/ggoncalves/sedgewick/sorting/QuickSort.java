package ggoncalves.sedgewick.sorting;

import java.util.Random;

public class QuickSort<T extends Comparable<T>> extends AbstractSorter<T> {

  private final Random random = new Random();

  // N log N guaranted
  // Stable
  @Override
  public void sort(T[] arr) {
    quickSort(arr, 0, arr.length - 1);
  }

  private void quickSort(T[] arr, int lo, int hi) {
    if (lo >= hi) return;
    int pivot = chosePivot(arr, lo, hi);
    int pivotPos = partition(arr, lo, hi, pivot);
    quickSort(arr, lo, pivotPos - 1);
    quickSort(arr, pivotPos + 1, hi);
  }

  private int chosePivot(T[] arr, int lo, int hi) {
    // random chose
    return lo + random.nextInt(hi - lo + 1);
  }

  private int partition(T[] arr, int lo, int hi, int pivot) {
    if (pivot != lo) {
      exchange(arr, lo, pivot);
    }
    int i = lo;
    int j = hi + 1;
    while (true) {

      while (arr[++i].compareTo(arr[lo]) <= 0) {
        if (i == hi) break;
      }

      while (arr[--j].compareTo(arr[lo]) >= 0) {
        if (j == lo) break;
      }

      if (i < j) {
        exchange(arr, i, j);
      } else break;

    }

    exchange(arr, lo, j);
    return j;
  }


}
