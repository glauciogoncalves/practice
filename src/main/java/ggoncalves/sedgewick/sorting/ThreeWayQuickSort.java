package ggoncalves.sedgewick.sorting;

import java.util.Random;

public class ThreeWayQuickSort<T extends Comparable<T>> extends AbstractSorter<T> {

  private final Random random = new Random();

  // N log N guaranted
  // Stable
  @Override
  public void sort(T[] arr) {
    quickSort(arr, 0, arr.length - 1);
  }

  private void quickSort(T[] arr, int lo, int hi) {
    if (lo >= hi) return;

    int lt = lo;
    int gt = hi;
    T v = arr[lo];
    int i = lo;

    while (i <= gt) {

      int comp = arr[i].compareTo(v);
      if (comp < 0) exchange(arr, lt++, i++);
      else if (comp > 0) exchange(arr, i, gt--);
      else i++;

    }
    quickSort(arr, lo, lt - 1);
    quickSort(arr, gt + 1, hi);

  }


}
