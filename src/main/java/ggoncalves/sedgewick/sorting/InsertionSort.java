package ggoncalves.sedgewick.sorting;

public class InsertionSort<T extends Comparable<T>> extends AbstractSorter<T> {

  // Super fast when array almost sorted
  // Super fast on small arrays
  // Worse case when inverted sorted
  @Override
  public void sort(T[] arr) {

    int i = 0, j = 1;
    while (j < arr.length) {
      while (j > 0 && arr[j - 1].compareTo(arr[j]) > 0) exchange(arr, j - 1, j--);
      j = ++i + 1;
    }

  }

  public void sort2(T[] arr) {

    for (int i = 0; i < arr.length; i++) {

      for (int j = i; j > 0; j--) {
        if (arr[j].compareTo(arr[j - 1]) < 0) {
          exchange(arr, j, j - 1);
        } else break;
      }
    }

  }
}
