package ggoncalves.sedgewick.helpers;

import ggoncalves.sedgewick.datastructure.heap.Heap;

public final class HeapHelper {

  public static <T extends Comparable<T>> boolean isHeapfied(Heap<T> heap) {
    Comparable[] array = heap.getArray();
    return isHeapfied(array, heap.size());
  }

  public static <T extends Comparable<T>> boolean isHeapfied(T[] array) {
    if (array.length < 2) {
      return false;
    }
    return isHeapfied(array, array.length);
  }

  public static <T extends Comparable<T>> void printArray(Heap heap) {
    if (heap == null) {
      System.out.println("Heap is null");
    } else if (heap.size() == 0) {
      System.out.println("Heap is empty");
    } else {
      StringBuilder sb = new StringBuilder();
      Comparable[] array = heap.getArray();
      for (int i = 0; i <= heap.size(); i++) {
        Comparable t = array[i];
        sb.append((t == null) ? "null" : t.toString());
        sb.append(" , ");
      }
      System.out.println("Array [ " + sb.substring(0, sb.length() - 3) + " ]");
    }
  }


  private static <T extends Comparable<T>> boolean isHeapfied(T[] array, int size) {
    for (int i = 1; i <= size / 2; i++) {

      if (array[i] == null) continue;

      int c = 2 * i;

      if (c >= size) continue;

      if (array[c] != null && ArrayHelper.isLess(array, i, c)) return false;

      if (c + 1 >= size) continue;

      if (array[c + 1] != null && ArrayHelper.isLess(array, i, c + 1)) return false;

    }
    return true;
  }

}
