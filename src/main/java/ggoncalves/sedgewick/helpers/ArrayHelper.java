package ggoncalves.sedgewick.helpers;

public final class ArrayHelper {

  public static <T extends Comparable<T>> boolean isLess(T[] arr, int oneIndex, int anotherIndex) {
    return arr[oneIndex].compareTo(arr[anotherIndex]) < 0;
  }

  public static <T extends Comparable<T>> void exchange(T[] arr, int i, int j) {
    T aux = arr[i];
    arr[i] = arr[j];
    arr[j] = aux;
  }

}
