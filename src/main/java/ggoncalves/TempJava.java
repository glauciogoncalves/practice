package ggoncalves;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class TempJava {

  // Definition for singly-linked list.
  class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
      val = x;
    }
  }

  private TempJava() {

//    ListNode head = new ListNode(1);
//    ListNode node2 = new ListNode(2);
//    ListNode node3 = new ListNode(3);
//    ListNode node4 = new ListNode(4);
//    ListNode node5 = new ListNode(5);
//
//    head.next = node2;
//    node2.next = node3;
//    node3.next = node4;
//    node4.next = node5;
//    node5.next = null;
//
//    printListNode(head);
//
//    ListNode newHead = reverseList(head);
//
//    printListNode(newHead);

    int[] nums = {2, 3, 4, 1, 5};

    System.out.println("Result:" + minimumSwaps(nums));

  }

  private static int minimumSwaps(int[] arr) {

    Map<Integer, Integer> map = new HashMap<>();

    for (int i = 0; i < arr.length; i++) {
      map.put(arr[i] - 1, i);
    }

    int s = 0;

    for (int i = 0; i < arr.length; i++) {

      if (arr[i] == i + 1) continue;

      if (arr[arr[i] - 1] == i + 1) {
        int aux = arr[i];
        int auxPos = arr[i] - 1;
        arr[i] = arr[arr[i] - 1];
        map.put(arr[i] - 1, i);
        arr[auxPos] = aux;
        map.put(arr[auxPos] - 1, auxPos);
        s++;
      } else {
        int aux = arr[i];
        int auxPos = map.get(i);
        arr[i] = arr[auxPos];
        map.put(arr[i] - 1, i);
        arr[auxPos] = aux;
        map.put(arr[auxPos] - 1, auxPos);
        s++;
      }

    }
    return s;
  }


  public int pivotIndex(int[] nums) {


    int[] sums = new int[nums.length];

    sums[0] = nums[0];

    for (int i = 1; i < nums.length; i++) {

      sums[i] = nums[i] + sums[i - 1];

    }

    int index = -1;

    int total = sums[sums.length - 1];

    for (int i = sums.length - 2; i > 0; i--) {

      int v1 = total - sums[i];
      if (v1 == sums[i - 1]) {
        index = i;
      }
//      else if (v1 > sums[i-1]) {
//        break;
//      }

    }

    return index;

  }

  private void printListNode(ListNode head) {

    ListNode node = head;

    while (node != null) {
      System.out.print(node.val + " ");
      node = node.next;
    }

    System.out.println();


  }


  public ListNode reverseList(ListNode head) {

    // 1->2->3->4->5->NULL
    // 5->4->3->2->1->NULL

    return reverse(head);


  }

  private ListNode reverse(ListNode head) {

    Stack<ListNode> stack = new Stack<>();

    while (head != null) {

      stack.push(head);
      head = head.next;

    }

    ListNode newHead = stack.peek();
    ListNode node = stack.pop();

    while (!stack.isEmpty()) {

      node.next = stack.pop();
      node = node.next;

    }

    node.next = null;

    return newHead;
  }

  public static void main(String[] args) {
    new TempJava();
  }

}


