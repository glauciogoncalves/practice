package ggoncalves.hackerrank.dynamicarray.sortedarray;

/**
 * Created by ggoncalves on 15/03/17.
 */
public class SortedArray {

    public SortedArray() {

        // 2 1 4 3

        int[] intArray = {2, 1, 4, 5, 3};

        Node head = null;

        for (int element : intArray) {

            head = SortedInsert(head, element);
            printHead(head);

            System.out.println("next");

        }


    }

    private void printHead(Node head) {
        Node curr = head;
        while (curr != null) {

            System.out.println(curr);
            curr = curr.next;

        }
    }

    /*
      Insert Node at the end of a linked list
      head pointer input could be NULL as well for empty list
      Node is defined as */
    class Node {
        int data;
        Node next;
        Node prev;

        @Override
        public String toString() {
            return "Node{" +
                    "data = " + data +
                    ", next = " + ((next == null) ? "null" : next.data) + '}';
        }
    }

    Node SortedInsert(Node head, int data) {
        if (head == null) {
            head = new Node();
            head.data = -1;
        }


        Node curr = head.next;

        if (curr == null) {
            curr = new Node();
            curr.data = data;
            curr.prev = head;
            head.next = curr;
            return head;
        } else if (curr.data > data) {
            Node newNode = new Node();
            newNode.data = data;
            newNode.prev = head;
            newNode.next = curr;
            head.next = newNode;
            curr.prev = newNode;
            return head;
        }
        Node nextNode = curr.next;
        while (nextNode != null && nextNode.data <= data) {
            curr = curr.next;
            nextNode = nextNode.next;
        }


        if (nextNode == null) {
            nextNode = new Node();
            nextNode.data = data;
            nextNode.prev = curr;
            curr.next = nextNode;
        } else {
            Node newNode = new Node();
            newNode.data = data;
            newNode.next = nextNode;
            nextNode.prev = newNode;
            newNode.prev = curr;
            curr.next = newNode;
        }

        return head;
    }

    public static void main(String[] args) {
        new SortedArray();

    }

}
