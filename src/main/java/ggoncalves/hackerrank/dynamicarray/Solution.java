package ggoncalves.hackerrank.dynamicarray;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class Solution {

    public static void main(String[] args) throws IOException {
//        System.out.println("aaaaaa");
        File f = new File(Solution.class.getClassLoader().getResource("input03.txt").getFile());

        byte[] bytesfile = Files.readAllBytes(f.toPath());

        ByteArrayInputStream bais = new ByteArrayInputStream(bytesfile);

        System.setIn(bais);

        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner scanner = new Scanner(System.in);
        try {
            int N = scanner.nextInt();
            int Q = scanner.nextInt();

            int lastAns = 0;
            Map<Integer, List<Integer>> mapList = new HashMap<Integer, List<Integer>>();

            while (Q-- > 0) {

                int type = scanner.nextInt();
                int x = scanner.nextInt();
                int y = scanner.nextInt();

                if (type == 1) {

                    int key = (x ^ lastAns) % N;
                    if (mapList.containsKey(key)) {
                        mapList.get(key).add(y);
                    } else {
                        List<Integer> list = new ArrayList<Integer>();
                        list.add(y);
                        mapList.put(key, list);
                    }

                } else {
                    int key = (x ^ lastAns) % N;
                    if (mapList.containsKey(key)) {
                        int element = y % mapList.get(key).size();
                        int newAns = mapList.get(key).get(element);
                        lastAns = newAns;
                        System.out.println(lastAns);
                    }
                }

            }


        } finally {
            scanner.close();
        }
    }
}
