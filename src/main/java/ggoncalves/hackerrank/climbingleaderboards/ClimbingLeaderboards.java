package ggoncalves.hackerrank.climbingleaderboards;

import java.util.*;

//https://www.hackerrank.com/challenges/climbing-the-leaderboard/problem
public class ClimbingLeaderboards {

  // Complete the climbingLeaderboard function below.
  static int[] climbingLeaderboard(int[] scores, int[] alice) {

    Map<Integer, Integer> map = new HashMap<>();
    TreeSet<Integer> scoreTree = new TreeSet<>(new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
      }
    });

    Set<Integer> scoreSet = new HashSet<>();

    for (int score : scores) {
      scoreSet.add(score);
    }

    for (int score : scoreSet) {
      scoreTree.add(score);
    }

    int rank = 1;
    for (int score : scoreTree) {
      map.put(score, rank++);
    }

    int[] result = new int[alice.length];
    for (int i = 0; i < alice.length; i++) {

      Integer higher = scoreTree.lower(alice[i]);
      if (higher == null) {
        result[i] = 1;
      } else {
        if (higher == alice[i]) {
          result[i] = map.get(higher);
        } else {
          result[i] = map.get(higher) + 1;
        }
      }
    }
    return result;
  }

//      7
//      100 100 50 40 40 20 10
//      4
//      5 25 50 120

  public static void main(String[] args) {
    int[] scores = {100, 90, 90, 80, 75, 60};
    int[] alices = {50, 65, 77, 90, 102};
    int[] result = ClimbingLeaderboards.climbingLeaderboard(scores, alices);
    Arrays.stream(result).forEach(System.out::println);

  }

}
