package ggoncalves.hackerrank.componentsingraph;

import java.util.*;

public class Solution {

    class TreeUnionFind {

        Map<Integer, Integer> valueMap = new HashMap<>();

        Map<Integer, HashSet<Integer>> tset = new HashMap<>();

        private void copyFromToAnother(Set<Integer> fromSet, Integer fromIndex, Set<Integer> toSet, Integer toIndex) {

            toSet.addAll(fromSet);

            HashSet<Integer> changeKey = tset.get(valueMap.get(fromIndex));

            int keyFromIndex = valueMap.get(fromIndex);

            for (Integer key : changeKey) {

                valueMap.remove(key);
                valueMap.put(key, valueMap.get(toIndex));

            }

            tset.remove(keyFromIndex);
        }

        void checkAndUnion(Integer v1, Integer v2) {

            if (v1.equals(v2)) {
                return;
            }

            HashSet<Integer> hash1 = find(v1);
            HashSet<Integer> hash2 = find(v2);

            if (hash1 == null || hash2 == null) {

                if (hash1 != null) {
                    hash1.add(v2);
                    valueMap.put(v2, valueMap.get(v1));
                } else if (hash2 != null) {
                    hash2.add(v1);
                    valueMap.put(v1, valueMap.get(v2));
                } else {
                    HashSet<Integer> hash = new HashSet<Integer>();
                    hash.add(v1);
                    hash.add(v2);
                    int uniqueIndex = System.identityHashCode(hash);

                    valueMap.put(v1, uniqueIndex);
                    valueMap.put(v2, uniqueIndex);

                    tset.put(uniqueIndex, hash);
                }

            } else {

                if (hash1 == hash2) return;

                if (hash1.size() >= hash2.size()) {
                    copyFromToAnother(hash2, v2, hash1, v1);
                } else {
                    copyFromToAnother(hash1, v1, hash2, v2);
                }

            }

        }

        HashSet<Integer> find(Integer value) {

            if (tset.isEmpty()) return null;

            Integer hashValue = valueMap.get(value);

            if (hashValue == null) return null;

            return tset.get(hashValue);


        }

        Integer[] smallestAndBiggest() {
            int small = Integer.MAX_VALUE;
            int big = Integer.MIN_VALUE;

            for (HashSet<Integer> h : tset.values()) {
                small = Math.min(small, h.size());
                big = Math.max(big, h.size());
            }

            Integer[] result = {small, big};
            return result;
        }


    }

    public Solution() {
        Scanner s = new Scanner(System.in);

        int N = s.nextInt();

        TreeUnionFind uf = new TreeUnionFind();

        while (N-- > 0) {

            int a = s.nextInt();
            int b = s.nextInt();

            uf.checkAndUnion(a, b);


        }

        Integer[] r = uf.smallestAndBiggest();
        System.out.print(r[0] + " " + r[1]);
        s.close();
    }

    public static void main(String[] args) {
        new Solution();


    }
}
