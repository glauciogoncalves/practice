package ggoncalves.hackerrank.floatpoint;

import java.math.BigDecimal;
import java.math.RoundingMode;

// https://www.hackerrank.com/challenges/lowest-triangle/problem
public class MinimumHeightTriangle {

  static int lowestTriangle(int base, int area) {

    // formula
    // h = 2(a/b)

    BigDecimal db = new BigDecimal(area);
    BigDecimal res = db.divide(new BigDecimal(base), 2, RoundingMode.UP).multiply(BigDecimal.valueOf(2));
    return res.setScale(0, RoundingMode.UP).intValue();


  }

  public static void main(String[] args) {
    int result = MinimumHeightTriangle.lowestTriangle(17, 100);
    System.out.println(result);
  }

}
