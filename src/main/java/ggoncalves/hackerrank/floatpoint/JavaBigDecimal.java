package ggoncalves.hackerrank.floatpoint;


import java.math.BigDecimal;
import java.util.*;

// https://www.hackerrank.com/challenges/java-bigdecimal/problem
public class JavaBigDecimal {

  static void printOrder(String... numbers) {

    List<String> order = new ArrayList<>();
    Arrays.stream(numbers).forEach((s) -> order.add(s));

    List<String> sorted = new ArrayList<>();
    Arrays.stream(numbers).forEach((s) -> sorted.add(s));

    Collections.sort(sorted, new Comparator<String>() {
      @Override
      public int compare(String s1, String s2) {

        BigDecimal o1 = new BigDecimal(s1);
        BigDecimal o2 = new BigDecimal(s2);

        int result = o2.compareTo(o1);
        if (result == 0) {
          int index1 = order.indexOf(s1);
          int index2 = order.indexOf(s2);
          result = (index1 < index2) ? -1 : 1;
        }
        return result;
      }
    });

    String[] s = new String[sorted.size()];

    int i = 0;
    for (String bd : sorted) {
      s[i++] = bd;
    }

    Arrays.stream(s).forEach(System.out::println);

  }

  public static void main(String[] args) {
    JavaBigDecimal.printOrder("-100", "50", "0", "56.6", "90", "0.12", ".12", "02.34", "000.0000");
  }


}
