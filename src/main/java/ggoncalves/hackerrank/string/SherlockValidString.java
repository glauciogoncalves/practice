package ggoncalves.hackerrank.string;

import java.util.*;

// https://www.hackerrank.com/challenges/sherlock-and-valid-string/problem
public class SherlockValidString {


  // Complete the isValid function below.
  static String isValid(String s) {

    StringBuilder sb = new StringBuilder(s);

    int[] charCount = new int[26];

    for (int i = 0; i < sb.length(); i++) {

      charCount[(sb.charAt(i) - 97)]++;

    }

    int max = Integer.MIN_VALUE;
    int min = 0;

    // 5 0 6 0 5 valid  5 5 6
    // 0 5 5 4   invalid 4 5 5
    // 5 6 valid 5 6
    // 0 5 4 4  valid 4 4 5

    // 4 5 4 5 4 invalid 4 4 4 5

    // 3 4 4 4 5 invalid

    // 7 5 5 5 // 5 5 5 7 invalid

    // 2 2 1  ->  1 2 2 valid -> remove 1

    List<Integer> freq = new ArrayList<>();
    Set<Integer> distincts = new HashSet<>();

    Arrays.stream(charCount).forEach((e) -> {
      if (e != 0) {
        freq.add(e);
        distincts.add(e);
      }
    });

    Collections.sort(freq);

    if (distincts.size() == 1) return "YES";
    if (distincts.size() > 2) return "NO";


    if (freq.get(freq.size() - 1) == freq.get(freq.size() - 2)) {
      if (freq.get(0) == 1 && freq.get(1) > 1) {
        return "YES";
      }
      return "NO";
    }
    if ((freq.get(freq.size() - 1) - freq.get(freq.size() - 2)) > 1) {
      return "NO";
    }

    return "YES";
  }

  public static void main(String[] args) {
    String result = SherlockValidString.isValid("ibfdgaeadiaefgbhbdghhhbgdfgeiccbiehhfcggchgghadhdhagfbahhddgghbdehidbibaeaagaeeigffcebfbaieggabcfbiiedcabfihchdfabifahcbhagccbdfifhghcadfiadeeaheeddddiecaicbgigccageicehfdhdgafaddhffadigfhhcaedcedecafeacbdacgfgfeeibgaiffdehigebhhehiaahfidibccdcdagifgaihacihadecgifihbebffebdfbchbgigeccahgihbcbcaggebaaafgfedbfgagfediddghdgbgehhhifhgcedechahidcbchebheihaadbbbiaiccededchdagfhccfdefigfibifabeiaccghcegfbcghaefifbachebaacbhbfgfddeceababbacgffbagidebeadfihaefefegbghgddbbgddeehgfbhafbccidebgehifafgbghafacgfdccgifdcbbbidfifhdaibgigebigaedeaaiadegfefbhacgddhchgcbgcaeaieiegiffchbgbebgbehbbfcebciiagacaiechdigbgbghefcahgbhfibhedaeeiffebdiabcifgccdefabccdghehfibfiifdaicfedagahhdcbhbicdgibgcedieihcichadgchgbdcdagaihebbabhibcihicadgadfcihdheefbhffiageddhgahaidfdhhdbgciiaciegchiiebfbcbhaeagccfhbfhaddagnfieihghfbaggiffbbfbecgaiiidccdceadbbdfgigibgcgchafccdchgifdeieicbaididhfcfdedbhaadedfageigfdehgcdaecaebebebfcieaecfagfdieaefdiedbcadchabhebgehiidfcgahcdhcdhgchhiiheffiifeegcfdgbdeffhgeghdfhbfbifgidcafbfcd");
    System.out.println(result);
  }

}
