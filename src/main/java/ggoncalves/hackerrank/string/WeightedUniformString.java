package ggoncalves.hackerrank.string;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// https://www.hackerrank.com/challenges/weighted-uniform-string/problem
public class WeightedUniformString {

  static String[] weightedUniformStrings(String s, int[] queries) {

    Set<String> matchSet = allMatches(s);

    Set<Integer> sumSet = new HashSet<>();

    for (String match : matchSet) {

      char c = match.charAt(0);
      for (int i = 1; i <= match.length(); i++) {
        sumSet.add((c - 96) * i);
      }

    }

    String[] result = new String[queries.length];

    for (int i = 0; i < queries.length; i++) {

      result[i] = (sumSet.contains(queries[i])) ? "Yes" : "No";

    }

    return result;
  }

  private static Set<String> allMatches(String s) {

    Pattern p = Pattern.compile("([a-z])(?=\\1)(\\1*)|([a-z])");
    Matcher m = p.matcher(s);
    int i = 0;

    final Set<String> matches = new HashSet<>();
    while (m.find()) {
      matches.add(m.group(0));
    }

    return matches;

  }

  public static void main(String[] args) {
    int[] queries = {1, 3, 12, 5, 9, 10};
    String[] results = WeightedUniformString.weightedUniformStrings("abccddde", queries);
    Arrays.stream(results).forEach(System.out::println);
  }

}
