package ggoncalves.hackerrank.string;

// https://www.hackerrank.com/challenges/pangrams/problem?h_r=next-challenge&h_v=zen
public class Pangrams {

  // Complete the pangrams function below.
  static String pangrams(String s) {

    // 97 = a
    boolean[] charFound = new boolean[26];

    for (char c : s.toCharArray()) {

      if (c == ' ') continue;
      char lowerC = Character.toLowerCase(c);
      charFound[lowerC - 97] = true;

    }

    for (boolean found : charFound) {
      if (!found) return "not pangram";
    }
    return "pangram";
  }

  public static void main(String[] args) {
    String res = Pangrams.pangrams("We promptly judged antique ivory buckles for the next prize");
    System.out.println(res);
  }

}
