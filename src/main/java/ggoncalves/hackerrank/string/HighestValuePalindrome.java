package ggoncalves.hackerrank.string;

public class HighestValuePalindrome {

  // Complete the highestValuePalindrome function below.
  static String highestValuePalindrome(String s, int n, int k) {

    boolean[] changes = new boolean[n];
    for (int i = 0; i < changes.length; i++) {
      changes[i] = false;
    }

    if (n == 1) {
      if (k == 0) return s;
      return "9";
    }

    // k = 0 1001
    // k = 0 1100
    for (int b = 0, e = n - 1; b < e; b++, e--) {

      String first = Character.toString(s.charAt(b));
      String last = Character.toString(s.charAt(e));

      if (!first.equals(last)) {

        if (k-- == 0) {
          return "-1";
        }

        Integer firstNum = Integer.parseInt(first);
        Integer lastNum = Integer.parseInt(last);

        StringBuilder sb = new StringBuilder(s);
        if (firstNum > lastNum) {
          sb.setCharAt(e, first.charAt(0));
        } else {
          sb.setCharAt(b, last.charAt(0));
        }
        changes[b] = changes[e] = true;
        s = sb.toString();
      }


    }

    if (k > 0) {
      for (int b = 0, e = n - 1; b <= e; b++, e--) {

        if (s.charAt(b) == '9') continue;

        if (b == e) {
          StringBuilder sb = new StringBuilder(s);
          sb.setCharAt(b, '9');
          s = sb.toString();
          k--;
          break;
        }

        if (!changes[b]) {
          if (k >= 2) {
            k -= 2;
          } else continue;
        } else k -= 1;

        StringBuilder sb = new StringBuilder(s);
        sb.setCharAt(b, '9');
        sb.setCharAt(e, '9');
        s = sb.toString();

        if (k == 0) break;

      }

    }

    return s;

  }

  public static void main(String[] args) {
    String result = HighestValuePalindrome.highestValuePalindrome("3943", 4, 1);
    System.out.println(result);
  }

}
