package ggoncalves.hackerrank.string;

// https://www.hackerrank.com/challenges/hackerrank-in-a-string/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
public class HackerRankInAString {
  // Complete the hackerrankInString function below.
  static String hackerrankInString(String s) {

    String onlyChars = s.replaceAll("([^hackrenk])", "");
    String lookFor = "hackerrank";

    int i = 0;

    for (char c : onlyChars.toCharArray()) {

      if (lookFor.charAt(i) == c) {
        i++;
      }
      if (i == 10) return "YES";
    }
    return "NO";
  }

  public static void main(String[] args) {
    HackerRankInAString.hackerrankInString("hereiamstackerrank");
  }
}
