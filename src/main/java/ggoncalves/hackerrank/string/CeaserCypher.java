package ggoncalves.hackerrank.string;

// https://www.hackerrank.com/challenges/caesar-cipher-1/problem
public class CeaserCypher {

  // Complete the caesarCipher function below.
  static String caesarCipher(String s, int k) {

    k %= 26;

    StringBuilder resultBuilder = new StringBuilder();
    for (char c : s.toCharArray()) {
      resultBuilder.append(parseChar(c, k));
    }
    return resultBuilder.toString();
  }

  static String parseChar(Character c, int k) {
    if (!Character.isLetterOrDigit(c)) return c.toString();
    int i = (int) c;
    i = (i + k);

    if (Character.isUpperCase(c)) {
      if (i > 90) {
        i %= 90;
        i += 64;
      }
    } else if (i > 122) {
      i = i % 122;
      i += 96;
    }

    char result = (char) i;
    return Character.toString(result);
  }

  public static void main(String[] args) {
    System.out.println(CeaserCypher.caesarCipher("Hello_World!", 4));
  }

}
