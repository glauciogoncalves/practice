package ggoncalves.hackerrank.string;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// https://www.hackerrank.com/challenges/tag-content-extractor/problem
public class TagContentExtractor {

  static void extract(String s) {
    List<String> result = extractTag(s);

    if (result.isEmpty()) {
      System.out.println("None");
    } else {
      for (String r : result) {

        String p = r.replaceAll("<[^>]*>", "");
        System.out.println(p);
      }
    }
  }

  static List<String> extractTag(String s) {

    Pattern p = Pattern.compile("<(.+)>([^<]+)</\\1>");
    Matcher m = p.matcher(s);

    List<String> resultList = new ArrayList<>();

    while (m.find()) {
      resultList.add(m.group(2));
    }

    return resultList;

  }

  public static void main(String[] args) {
    TagContentExtractor.extract("<h1>had<h1>public</h1></h1>");

  }

}
