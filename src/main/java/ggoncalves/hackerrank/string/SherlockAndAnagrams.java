package ggoncalves.hackerrank.string;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem
public class SherlockAndAnagrams {

  // mom
  // m m
  // mo om

  // Complete the sherlockAndAnagrams function below.

  static int sherlockAndAnagrams(String s) {

    Map<String, Integer> map = new HashMap<>();

    StringBuilder sb = new StringBuilder(s);

    int totalCount = 0;

    for (int i = 0; i < sb.length(); i++) {

      for (int j = i + 1; j <= sb.length(); j++) {

        String substring = sb.substring(i, j);
        System.out.println(substring);

        char[] chars = substring.toCharArray();
        Arrays.sort(chars);
        String anagram = String.valueOf(chars);

        Integer count = map.get(anagram);
        if (count == null) {
          count = 1;
        } else {
          totalCount += count;
          count++;
        }
        map.put(anagram, count);


      }

    }

    return totalCount;

  }


  // ifailuahkfaiqq
  // abba
  public static void main(String[] args) {
    Integer res = SherlockAndAnagrams.sherlockAndAnagrams("cdcd");
    System.out.println(res);
  }
}
