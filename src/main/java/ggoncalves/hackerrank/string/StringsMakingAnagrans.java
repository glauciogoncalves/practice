package ggoncalves.hackerrank.string;

// https://www.hackerrank.com/challenges/ctci-making-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
public class StringsMakingAnagrans {

  // Complete the makeAnagram function below.
  static int makeAnagram(String a, String b) {

    char[] charArray1 = new char[26];
    char[] charArray2 = new char[26];

    for (char c : a.toCharArray()) {
      charArray1[c - 97]++;
    }

    for (char c : b.toCharArray()) {
      charArray2[c - 97]++;
    }

    int diff = 0;
    for (int i = 0; i < charArray1.length; i++) {

      if (charArray1[i] != charArray2[i]) {

        diff += Math.max(charArray1[i], charArray2[i]) - Math.min(charArray1[i], charArray2[i]);

      }

    }

    return diff;

  }

  public static void main(String[] args) {
    int res = StringsMakingAnagrans.makeAnagram("abc", "cde");
    System.out.println(res);
  }

}
