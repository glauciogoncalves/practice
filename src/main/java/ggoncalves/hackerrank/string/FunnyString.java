package ggoncalves.hackerrank.string;

public class FunnyString {

  // Complete the funnyString function below.
  static String funnyString(String s) {

    int[] asciiRegularOrder = new int[s.length()];
    int[] asciiInverseOrder = new int[s.length()];

    int i = 0;
    for (char c : s.toCharArray()) {
      asciiRegularOrder[i++] = c;
    }

    i = 0;
    for (int j = asciiRegularOrder.length - 1; j >= 0; j--) {
      asciiInverseOrder[i++] = asciiRegularOrder[j];
    }

    return "incomplete";

  }


}
