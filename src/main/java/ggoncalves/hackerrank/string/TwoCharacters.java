package ggoncalves.hackerrank.string;


import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * In this challenge, you will be given a string. You must remove characters until the string is made up of any two alternating characters. When you choose a character to remove, all instances of that character must be removed. Your goal is to create the longest string possible that contains just two alternating letters.
 * <p>
 * As an example, consider the string abaacdabd. If you delete the character a, you will be left with the string bcdbd. Now, removing the character c leaves you with a valid string bdbd having a length of 4. Removing either b or d at any point would not result in a valid string.
 * <p>
 * Given a string , convert it to the longest possible string  made up only of alternating characters. Print the length of string  on a new line. If no string  can be formed, print  instead.
 * <p>
 * Function Description
 * <p>
 * Complete the alternate function in the editor below. It should return an integer that denotes the longest string that can be formed, or  if it cannot be done.
 * <p>
 * alternate has the following parameter(s):
 * <p>
 * s: a string
 * Input Format
 * <p>
 * The first line contains a single integer denoting the length of .
 * The second line contains string .
 * <p>
 * Constraints
 * <p>
 * Output Format
 * <p>
 * Print a single integer denoting the maximum length of  for the given ; if it is not possible to form string , print  instead.
 * <p>
 * Sample Input
 * <p>
 * 10
 * beabeefeab
 * Sample Output
 * <p>
 * 5
 * Explanation
 * <p>
 * The characters present in  are a, b, e, and f. This means that  must consist of two of those characters and we must delete two others. Our choices for characters to leave are [a,b], [a,e], [a, f], [b, e], [b, f] and [e, f].
 * <p>
 * If we delete e and f, the resulting string is babab. This is a valid  as there are only two distinct characters (a and b), and they are alternating within the string.
 * <p>
 * If we delete a and f, the resulting string is bebeeeb. This is not a valid string  because there are consecutive e's present. Removing them would leave consecutive b's, so this fails to produce a valid string .
 * <p>
 * Other cases are solved similarly.
 * <p>
 * babab is the longest string we can create.
 */
// https://www.hackerrank.com/challenges/two-characters/problem
public class TwoCharacters {


  // beabeefeab
  static int alternate(String string) {

    Set<Character> charSet = new HashSet<>();

    Set<Character> removableSet = new HashSet<>();

    char previous = ' ';
    for (char c : string.toCharArray()) {
      if (c == previous) {
        removableSet.add(c);
        if (removableSet.size() > string.length() - 2) {
          return 0;
        }
      }
      charSet.add(c);
      previous = c;
    }

    List<Pair<Character, Character>> combinationList = createCombinationList(charSet, removableSet);

    int maxLength = 0;

    for (Pair<Character, Character> combinationPair : combinationList) {

      String result = string;
      for (Character removeChar : charSet) {
        if (!combinationPair.getKey().equals(removeChar) &&
            !combinationPair.getValue().equals(removeChar)) {
          result = result.replaceAll(removeChar.toString(), "");
        }
      }
      if (checkExpression(result)) {
        maxLength = Math.max(maxLength, result.length());
      }
    }

    return maxLength;
  }

  static List<Pair<Character, Character>> createCombinationList(Set<Character> charSet, Set<Character> removableSet) {
    List<Pair<Character, Character>> result = new ArrayList<>();

    String string = charSet.stream().map(String::valueOf).collect(Collectors.joining());
    permute(string, "", result, removableSet);

    return result;
  }

  static void permute(String charSet, String current, List<Pair<Character, Character>> result, Set<Character> removableSet) {
    if (current.length() == 2) {
      if ((!removableSet.contains(current.charAt(0)) && !removableSet.contains(current.charAt(1)))) {
        System.out.println(current);
        result.add(new Pair<Character, Character>(current.charAt(0), current.charAt(1)));
      }
    } else for (int i = 0; i < charSet.length(); i++) {
      permute(charSet.substring(i + 1), current + charSet.charAt(i), result, removableSet);
    }
  }

  static void permute2(String charSet, String current) {
    if (current.length() == 2) {
      System.out.println(current);
    } else for (int i = 0; i < charSet.length() - 1; i++) {
      permute2(charSet.substring(i + 1), current + charSet.charAt(i));
    }
  }

  static boolean checkExpression(String expression) {
    Pattern p = Pattern.compile("^([a-z])(?!\\1)([a-z])(\\1\\2)*\\1?$");
    Matcher m = p.matcher(expression);

    return m.matches();
  }
}
