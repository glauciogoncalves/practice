package ggoncalves.hackerrank.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// https://www.hackerrank.com/challenges/reduced-string/problem
public class SuperReducedString {


  // Complete the superReducedString function below.
  static String superReducedString(String s) {

    while (matches(s)) {

      s = s.replaceAll("([a-z])(?=\\1)[a-z]", "");

    }

    if (s.isEmpty()) return "Empty String";
    return s;
  }

  private static boolean matches(String s) {
    Pattern p = Pattern.compile("([a-z])(?=\\1)");
    Matcher m = p.matcher(s);
    return m.find();
  }

  // ([a-z])(?=\1)
  public static void main(String[] args) {
    String res = SuperReducedString.superReducedString("aaabccddd");
    System.out.println(res);
  }
}
