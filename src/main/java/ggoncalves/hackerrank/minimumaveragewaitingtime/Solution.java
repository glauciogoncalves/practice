package ggoncalves.hackerrank.minimumaveragewaitingtime;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;


public class Solution {

    private long totalAcc = 0;

    private PriorityQueue<Order> pqueue = new PriorityQueue<Order>();

    private PriorityQueue<Order> fqueue = new PriorityQueue<Order>(100, new Comparator<Order>() {
        public int compare(Order o1, Order o2) {
            return Integer.compare(o1.t, o2.t);
//            return o1.t.compareTo(o2.t);
        }
    });

    public void findResultAndPrint() {
        Long result = findMinimumAverageWaitingTime();
        System.out.println(result);
    }

    public Long findMinimumAverageWaitingTime() {
        Scanner s = new Scanner(System.in);
        try {

            int N = s.nextInt();

            int n = N;

            while (n-- > 0) {

                int t = s.nextInt();
                int l = s.nextInt();

                Order o = new Order(t, l);
                fqueue.offer(o);
            }

            long currTime = fqueue.peek().t;

            while (!fqueue.isEmpty() || !pqueue.isEmpty()) {

                if (!fqueue.isEmpty() && currTime >= fqueue.peek().t) {
                    pqueue.offer(fqueue.poll());

                } else {
                    if (!pqueue.isEmpty()) {
                        Order o2 = pqueue.poll();
                        currTime = makePizza(currTime, o2.t, o2.l);
                    } else {
                        currTime = fqueue.peek().t;
                    }
                }
            }


            return totalAcc / N;
        } finally {
            s.close();
        }
    }


    public class Order implements Comparable<Order> {
        public int t;
        public int l;

        public Order(int t, int l) {
            this.t = t;
            this.l = l;
        }

        @Override
        public int compareTo(Order o) {
            return Integer.compare(this.l, o.l);

//                    this.l.compareTo(o.l);
        }

        @Override
        public String toString() {
            return "Order{" +
                    "t=" + t +
                    ", l=" + l +
                    '}';
        }
    }


    Long makePizza(long currTime, int t, int l) {
        // 0 + 3 - 0
        // 0 3
        // 283280121 + 782916802 - 283280121
        //  283280121 782916802
        // 961148050, 385599125
        totalAcc += currTime + l - t;
//        totalAcc = Math.abs(totalAcc);
        return currTime + l;
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Solution s = new Solution();
        s.findResultAndPrint();
    }
}
