package ggoncalves.hackerrank.spanningtreefraction;


import java.util.*;

public class Solution {

  public Solution() {

  }

  public Solution(int n, List<Edge> edges) {

    Collections.sort(edges, new Comparator<Edge>() {
      @Override
      public int compare(Edge o1, Edge o2) {

        if (o1.a == o2.a && o1.b == o2.b) return 0;

        // compare ratio
        float r1 = o1.a / o1.b;
        float r2 = o2.a / o2.b;

        if (r1 > r2) return -1;
        else if (r1 < r2) return 1;

        if (o1.a == o1.b && o2.a == o2.b) return Integer.compare(o2.a, o1.a);
        if (o1.a > o1.b && o2.a > o2.b) return Integer.compare(o2.a, o1.a);
        return Integer.compare(o1.a, o2.a);

      }

      @java.lang.Override
      public boolean equals(java.lang.Object obj) {
        return false;
      }
    });

    for (int i = 0; i < edges.size(); i++) {
      System.out.println("" + i + ": " + edges.get(i));
    }

    int size = 0;
    int p = 0;
    int q = 0;

    UnionFind uf = new UnionFind(n);

//        for (int i = 0; i < edges.size() && size < n; i++) { // stopped early
    for (int i = 0; i < edges.size(); i++) {

      Edge edge = edges.get(i);
      if (uf.union(edge.u, edge.v)) {
        p += edge.a;
        q += edge.b;
      }

    }

    String res = mountResult(p, q);
    System.out.println(res);
  }

  String mountResult(int p, int q) {
    if (p == q) {
      return "1/1";
    }
    if (p > q && p % q == 0) {

      p = p / q;
      q = 1;

    }
    return "" + p + "/" + q;
  }

  public class UnionFind {

    private int[] parent;
    private int[] listsize;

    public UnionFind(int size) {
      size++;
      parent = new int[size];
      listsize = new int[size];

      for (int i = 0; i < size; i++) {
        parent[i] = i;
        listsize[i] = 1;
      }
    }

    public int find(int value) {

      while (value != parent[value]) {

        // Path compression
        parent[value] = parent[parent[value]];
        value = parent[value];

      }

      return value;

    }

    public boolean isUnified(int v1, int v2) {
      int parent1 = find(v1);
      int parent2 = find(v2);
      return (parent1 == parent2);
    }

    public boolean union(int v1, int v2) {

      int parent1 = find(v1);
      int parent2 = find(v2);

      if (parent1 == parent2) return false;

      if (listsize[parent1] >= listsize[parent2]) {

        parent[parent2] = parent1;
        listsize[parent1] += listsize[parent2];
        listsize[parent2] = 0;

      } else {

        parent[parent1] = parent2;
        listsize[parent2] += listsize[parent1];
        listsize[parent1] = 0;

      }

      return true;
    }

    public int size(int value) {
      int parent = find(value);
      return listsize[parent];
    }

  }

  public static class Edge {

    int u;
    int v;

    int a;
    int b;

    public Edge(int u, int v, int a, int b) {
      this.u = u;
      this.v = v;
      this.a = a;
      this.b = b;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Edge edge = (Edge) o;

      if (u != edge.u) return false;
      if (v != edge.v) return false;
      if (a != edge.a) return false;
      return b == edge.b;

    }

    @Override
    public int hashCode() {
      int result = u;
      result = 31 * result + v;
      result = 31 * result + a;
      result = 31 * result + b;
      return result;
    }

    @Override
    public String toString() {
      return "Edge{" +
          "u=" + u +
          ", v=" + v +
          ", a=" + a +
          ", b=" + b +
          '}';
    }
  }


  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int m = in.nextInt();

    List<Edge> edges = new ArrayList<>();


    for (int a0 = 0; a0 < m; a0++) {
      int u = in.nextInt();
      int v = in.nextInt();
      int a = in.nextInt();
      int b = in.nextInt();
      // Write Your Code Here

      Edge edge = new Edge(u, v, a, b);
      edges.add(edge);
    }

    new Solution(n, edges);
  }
}

