package ggoncalves.hackerrank.mergingcommunities;


public class UnionFindTemp {

    private int[] parent;
    private int[] listsize;

    public UnionFindTemp(int size) {
        parent = new int[size];
        listsize = new int[size];

        for (int i = 0; i < size; i++) {
            parent[i] = i;
            listsize[i] = 1;
        }
    }

    public int find(int value) {

        while (value != parent[value]) {

            // Path compression
            parent[value] = parent[parent[value]];
            value = parent[value];

        }

        return value;

    }

    public boolean union(int v1, int v2) {

        int parent1 = find(v1);
        int parent2 = find(v2);

        if (parent1 == parent2) return false;

        if (listsize[parent1] >= listsize[parent2]) {

            parent[parent2] = parent1;
            listsize[parent1] += listsize[parent2];
            listsize[parent2] = 0;

        } else {

            parent[parent1] = parent2;
            listsize[parent2] += listsize[parent1];
            listsize[parent1] = 0;

        }

        return true;
    }

    public int size(int value) {
        int parent = find(value);
        return listsize[parent];
    }

}
