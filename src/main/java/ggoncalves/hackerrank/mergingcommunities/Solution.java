package ggoncalves.hackerrank.mergingcommunities;


import java.util.Scanner;

public class Solution {

    public static class UnionFind {

        private int[] parent;
        private int[] listsize;

        public UnionFind(int size) {
            size++;
            parent = new int[size];
            listsize = new int[size];

            for (int i = 0; i < size; i++) {
                parent[i] = i;
                listsize[i] = 1;
            }
        }

        public int find(int value) {

            while (value != parent[value]) {

                // Path compression
                parent[value] = parent[parent[value]];
                value = parent[value];

            }

            return value;

        }

        public boolean union(int v1, int v2) {

            int parent1 = find(v1);
            int parent2 = find(v2);

            if (parent1 == parent2) return false;

            if (listsize[parent1] >= listsize[parent2]) {

                parent[parent2] = parent1;
                listsize[parent1] += listsize[parent2];
                listsize[parent2] = 0;

            } else {

                parent[parent1] = parent2;
                listsize[parent2] += listsize[parent1];
                listsize[parent1] = 0;

            }

            return true;
        }

        public int size(int value) {
            int parent = find(value);
            return listsize[parent];
        }

    }

    public Solution() {
        Scanner s = new Scanner(System.in);

        int N = s.nextInt();
        int Q = s.nextInt();

        UnionFind uf = new UnionFind(N);

        while (Q-- > 0) {

            String q = s.next();

            if (q.equals("M")) {
                int v1 = s.nextInt();
                int v2 = s.nextInt();
                uf.union(v1, v2);
            } else {
                int size = uf.size(s.nextInt());
                System.out.println(size);
            }

        }
    }

    public static void main(String[] args) {

        new Solution();


    }
}
