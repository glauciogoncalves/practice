package ggoncalves.hackerrank.BST;

import java.util.ArrayList;
import java.util.List;

class IsThisABinarySearchTree {

  /* Hidden stub code will pass a root argument to the function below. Complete the function to solve the challenge. Hint: you may want to write one or more helper functions. */

  //The Node class is defined as follows:
  static class Node {
    int data;
    Node left;
    Node right;

    Node(int data) {
      this.data = data;
    }
  }

  boolean checkBST(Node root) {
    return inplace(root, new ArrayList<Integer>());

  }

  boolean inplace(Node node, List<Integer> visited) {
    if (node == null) return true;

    boolean result = inplace(node.left, visited);
    if (!result) return false;

    if (!visited.isEmpty()) {
      int previous = visited.get(visited.size() - 1);
      if (previous >= node.data) return false;
    }
    visited.add(node.data);
    return inplace(node.right, visited);


  }


}
