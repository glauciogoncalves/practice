package ggoncalves.hackerrank.equalstacks;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class EqualStacks {

    public EqualStacks(PriorityQueue<MaxQueue> pqueue) {

        int result = count(pqueue);
        // 2190913
        System.out.println(result);

    }

    private int count(PriorityQueue<MaxQueue> pqueue) {

        do {

            if (allEquals(pqueue)) {
                return pqueue.iterator().next().getMax();
            }


            MaxQueue pollMaxQueue = pqueue.poll();
            pollMaxQueue.poll();
            pqueue.offer(pollMaxQueue);


        } while (true);

    }

    private boolean allEquals(PriorityQueue<MaxQueue> pqueue) {

        if (pqueue.size() == 1) {
            return true;
        }

        Iterator<MaxQueue> queueIterator = pqueue.iterator();

        MaxQueue firstQueue = queueIterator.next();

        while (queueIterator.hasNext()) {

            if (firstQueue.getMax() != queueIterator.next().getMax()) {
                return false;
            }

        }

        return true;
    }

    public static class MaxQueue extends ArrayDeque<Integer> {

        private Integer max = 0;

        public MaxQueue() {
            super(100);
        }

        public Integer getMax() {
            return this.max;
        }

        public void setMax(Integer max) {
            this.max = max;
        }

        @Override
        public Integer poll() {

            Integer v = super.poll();

            if (v != null) {
                max -= v;
                setMax(max);
            }
            return v;
        }
    }

    public static MaxQueue readMaxQueue(Scanner in, int n) {
        MaxQueue maxQueue = new MaxQueue();
        int acc = 0;
        for (int h1_i = 0; h1_i < n; h1_i++) {
            int value = in.nextInt();
            acc += value;
            maxQueue.offerLast(value);
        }


        maxQueue.setMax(acc);
        return maxQueue;
    }

    public static void main(String[] args) throws IOException {
        File f = new File(EqualStacks.class.getClassLoader().getResource("inputequalstacks.txt").getFile());

        byte[] bytesfile = Files.readAllBytes(f.toPath());

        ByteArrayInputStream bais = new ByteArrayInputStream(bytesfile);

        System.setIn(bais);

        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int n2 = in.nextInt();
        int n3 = in.nextInt();


        PriorityQueue<MaxQueue> pqueue = new PriorityQueue<>(100, new Comparator<MaxQueue>() {
            @Override
            public int compare(MaxQueue o1, MaxQueue o2) {
                return o2.getMax().compareTo(o1.getMax());
            }
        });


        MaxQueue maxQueue = readMaxQueue(in, n1);
        MaxQueue maxQueue2 = readMaxQueue(in, n2);
        MaxQueue maxQueue3 = readMaxQueue(in, n3);

        pqueue.offer(maxQueue2);
        pqueue.offer(maxQueue3);
        pqueue.offer(maxQueue);

        new EqualStacks(pqueue);
    }
}

