package ggoncalves.hackerrank.fraudulentactivity;

import java.util.Arrays;
import java.util.LinkedList;

public class Fraudulent {

  private static void swap(int[] array, int i, int j) {
    int aux = array[i];
    array[i] = array[j];
    array[j] = aux;
  }

  private static void exchangeAndSort(int[] array, int index, int newElement) {

    array[index] = newElement;

    if (index < array.length - 1 && array[index] > array[index + 1]) {

      for (int i = index; i < array.length - 1; i++) {
        if (array[i] > array[i + 1]) swap(array, i, i + 1);
        else break;
      }

    } else if (index > 0 && array[index] < array[index - 1]) {

      for (int i = index; i > 0; i--) {
        if (array[i] < array[i - 1]) swap(array, i, i - 1);
        else break;
      }

    }

  }

  private static int medium(int[] avgArray, int d) {

    // Arrays.sort(avgArray);
    if (d % 2 == 1) return avgArray[avgArray.length / 2];

    int v1 = avgArray[avgArray.length / 2];
    int v2 = avgArray[(avgArray.length / 2) + 1];

    return (v1 + v2) / 2;

  }

  // Complete the activityNotifications function below.
  private static int activityNotifications(int[] expenditure, int d) {

    int[] avgArray = new int[d];
    LinkedList<Integer> queue = new LinkedList<>();

    for (int i = 0; i < d; i++) {

      queue.add(expenditure[i]);
      avgArray[i] = expenditure[i];

    }

    Arrays.sort(avgArray);

    int result = 0;

    for (int i = d; i < expenditure.length; i++) {

      int element = expenditure[i];

      if (element >= 2 * (medium(avgArray, d))) result++;

      int index = Arrays.binarySearch(avgArray, queue.removeFirst());

      exchangeAndSort(avgArray, index, element);

      // avgArray[index] = element;

      queue.add(element);

    }

    return result;
  }

  public static void main(String[] args) {
    int[] array = {2, 3, 4, 2, 3, 6, 8, 4, 5};
    System.out.println("Result: " + activityNotifications(array, 5));
  }

}
