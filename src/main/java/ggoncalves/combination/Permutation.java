package ggoncalves.combination;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Permutation {

	private static List<String> resultList;

	// print N! permutation of the characters of the string s (in order)
	public static void perm1(String s) {
		perm1("", s);
	}

	private static void perm1(String prefix, String s) {
		int n = s.length();
		if (n == 0)
			System.out.println(prefix);
		else {
			for (int i = 0; i < n; i++)
				perm1(prefix + s.charAt(i), s.substring(0, i) + s.substring(i + 1, n));
		}

	}

	private static void perm1(List<Integer> prefix, List<Integer> integerList) {
		int n = integerList.size();
		if (n == 0)
			System.out.println(prefix);
		else {
			for (int i = 0; i < n; i++) {
				prefix.add(integerList.get(i));
				List<Integer> newList = new ArrayList<Integer>();
				for (int k = 0; k < i; k++) {
					newList.add(integerList.get(k));
				}
				for (int k = i + 1; k < n; k++) {
					newList.add(integerList.get(k));
				}
				perm1(prefix, newList);
			}
		}

	}

	// print N! permutation of the elements of array a (not in order)
	public static void perm2(String s) {
		int N = s.length();
		char[] a = new char[N];
		for (int i = 0; i < N; i++)
			a[i] = s.charAt(i);
		perm2(a, N);
	}

	private static List<String> getResultList() {
		if (resultList == null) {
			resultList = new LinkedList<String>();
		}
		return resultList;
	}

	public static void permWithRepetition(List<String> result, String prefix,
	    String str) {
		if (prefix.length() == str.length()) {
			result.add(prefix);
			// System.out.println(prefix);
			return;
		}

		for (int i = 0; i < str.length(); i++) {
			permWithRepetition(result, prefix + str.charAt(i), str);
		}
	}

	private static void perm2(char[] a, int n) {
		if (n == 1) {
			System.out.println(a);
			return;
		}
		for (int i = 0; i < n; i++) {
			swap(a, i, n - 1);
			perm2(a, n - 1);
			swap(a, i, n - 1);
		}
	}

	// swap the characters at indices i and j
	private static void swap(char[] a, int i, int j) {
		char c;
		c = a[i];
		a[i] = a[j];
		a[j] = c;
	}

	public static void permutation(char[] c, int n, String start) {
		if (start.length() >= n) {
			System.out.println(start);
			getResultList().add(start);
			return;
		}
		else {
			for (char x : c) {
				permutation(c, n, start + x);
			}
		}
	}

	public static ArrayList<ArrayList<Integer>> permute(int[] num) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		permute(num, 0, result);
		return result;
	}

	static void permute(int[] num, int start, ArrayList<ArrayList<Integer>> result) {

		if (start - 4 == num.length) {
			ArrayList<Integer> item = convertArrayToList(num);
			result.add(item);
		}

		for (int j = start; j <= num.length - 1; j++) {
			swap2(num, start, j);
			permute(num, start + 1, result);
			swap2(num, start, j);
		}
	}

	private static ArrayList<Integer> convertArrayToList(int[] num) {
		ArrayList<Integer> item = new ArrayList<Integer>();
		for (int h = 0; h < num.length; h++) {
			item.add(num[h]);
		}
		return item;
	}

	private static void swap2(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int N = 2;
		String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		// String elements = alphabet.substring(0, N);
		String elements = "+-b+-b";
		List<String> result = new LinkedList<String>();
		// permWithRepetition(result, "", elements);

		int[] intarray = { 1, 2, 3, 4, 5, 6, 7 };
		ArrayList<ArrayList<Integer>> resList = permute(intarray);

		for (ArrayList<Integer> list : resList) {
			System.out.println(list);
		}

		int index = getResultList().indexOf("-+--b-");
		if (index > -1) {
			System.out.println(getResultList().get(index));
		}
//		perm1(elements);
		// System.out.println();
		perm2(elements);
	}

	// abcd e
	public static String nextPermutation(String s) {
	  
		// Base cases
		if (s == null || s.trim().isEmpty() || s.trim().length() == 1) {
			return null;
		}
		
		char[] c = s.trim().toCharArray();
		
		// 1- find the pivot
		int i = c.length-1;
		
		while (i > 0 && Character.getNumericValue(c[i]) <= Character.getNumericValue(c[i-1])) i--;
		
		if (i == 0) {
			return null;
		}

		int pivot = i-1;
		
		// 2- Find the exchange point
		int e = i;
		
		for (int index = i + 1; index < c.length; index++) {
			if (Character.getNumericValue(c[index]) > Character.getNumericValue(c[pivot]) &&
					Character.getNumericValue(c[index]) <= Character.getNumericValue(c[e])) {
				e = index;
			}
		}
		
		char aux = c[pivot];
		c[pivot] = c[e];
		c[e] = aux;
		
		int start = i;
		int end = c.length-1;
		
		while (start < end) {
			
			aux = c[end];
			c[end] = c[start];
			c[start] = aux;
			
			start++;
			end--;
		}
		
		return new String(c);
  }
}
