package ggoncalves.trainingMSVXGWNZ2;

public class Solution {

  public boolean solution(int[] A) {
    // write your code in Java SE 8

    int[] sumArray = new int[A.length];

    sumArray[0] = A[0];

    for (int i = 1; i < A.length; i++) {

      sumArray[i] = sumArray[i - 1] + A[i];

    }

    int total = sumArray[sumArray.length - 1];

    int i = 1;
    int j = A.length - 2;

    while (i < j) {

      int p1 = total - (total - sumArray[i - 1]);
      int p2 = total - sumArray[j];

      if (p2 > p1) {
        i++;
      } else if (p2 < p1) {
        j--;
      } else {
        int result = sumArray[j - 1] - sumArray[i];
        if (result == p1) return true;
        if (i == j - 1) return true;
        i++;


      }

    }

    return false;

  }

  public static void main(String[] args) {
    Solution s = new Solution();

    int[] A = {1, 2, 1, 2, 1, 2, 1, 2};

    boolean result = s.solution(A);
////
    System.out.println("Resultado : " + result);

  }

}
