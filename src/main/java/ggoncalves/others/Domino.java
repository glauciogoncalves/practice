package ggoncalves.others;

import javafx.util.Pair;

import java.util.List;
import java.util.Objects;

class Domino {

  boolean isValidArrangement(List<Pair<Integer, Integer>> arrangementList) {
    if (arrangementList.isEmpty() || arrangementList.size() == 1) return true;

    Pair<Integer, Integer> firstPiece = arrangementList.get(0);

    Integer left = firstPiece.getKey();
    Integer right = firstPiece.getValue();

    for (int i = 1; i < arrangementList.size(); i++) {
      Pair<Integer, Integer> piece = arrangementList.get(i);
      if (Objects.equals(piece.getKey(), left)) {
        left = piece.getValue();
      } else if (Objects.equals(piece.getKey(), right)) {
        right = piece.getValue();
      } else if (Objects.equals(piece.getValue(), left)) {
        left = piece.getKey();
      } else if (Objects.equals(piece.getValue(), right)) {
        right = piece.getKey();
      } else {
        return false;
      }

    }

    return true;
  }

}
