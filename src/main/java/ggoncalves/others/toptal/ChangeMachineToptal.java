package ggoncalves.others.toptal;

//A vending machine has the following denominations:1c,5c,10c,25c,50c,and $1.
//    Your task is to write a program that will be used in a vending machine to return change.
//    Assume that the vending machine will always want to return the least number of coins or notes.
//    Devise a function getChange(M,P)where M is how much money was inserted into the machine
//    and P the price of the item selected,that returns an array of integers representing the number
//    of each denomination to return.
//
//    Example:
//    getChange(5,0.99) // should return [1,0,0,0,0,4]

import java.math.BigDecimal;
import java.util.Arrays;

public class ChangeMachineToptal {

  public Integer[] getChange(Integer moneyInserted, BigDecimal price) {

    // 1c,5c,10c,25c,50c,$1
    Integer[] result = {0, 0, 0, 0, 0, 0};

    // Cents
    price.setScale(2);

    BigDecimal nextValue = BigDecimal.valueOf(price.intValue()).add(BigDecimal.ONE);

    BigDecimal cents = nextValue.subtract(price);

//    BigDecimal rest = BigDecimal.ONE.subtract(cents);

    while (cents.compareTo(BigDecimal.ZERO) != 0) {
      if (cents.subtract(new BigDecimal("0.5")).compareTo(BigDecimal.ZERO) >= 0) {
        cents = cents.subtract(new BigDecimal("0.5"));
        result[4]++;
      }
      if (cents.subtract(new BigDecimal("0.25")).compareTo(BigDecimal.ZERO) >= 0) {
        cents = cents.subtract(new BigDecimal("0.25"));
        result[3]++;
      }
      if (cents.subtract(new BigDecimal("0.10")).compareTo(BigDecimal.ZERO) >= 0) {
        cents = cents.subtract(new BigDecimal("0.10"));
        result[2]++;
      }
      if (cents.subtract(new BigDecimal("0.05")).compareTo(BigDecimal.ZERO) >= 0) {
        cents = cents.subtract(new BigDecimal("0.05"));
        result[1]++;
      }
      if (cents.subtract(new BigDecimal("0.01")).compareTo(BigDecimal.ZERO) >= 0) {
        cents = cents.subtract(new BigDecimal("0.01"));
        result[0]++;
      }
    }

    result[5] = moneyInserted - nextValue.intValue();
    return result;

  }

  public static void main(String[] args) {
    ChangeMachineToptal cm = new ChangeMachineToptal();
    Arrays.stream(cm.getChange(5, new BigDecimal("2.99"))).forEach((i) -> System.out.println(i));
  }
}
