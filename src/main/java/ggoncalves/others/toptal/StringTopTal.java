package ggoncalves.others.toptal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//
//A precedence rule is given as “P>E”, which means that letter “P” is followed by letter “E”. Write a function, given an
//    array of precedence rules, that finds the word represented by the given rules.
//
//    Note: Each represented word contains a set of unique characters, i.e. the word does not contain duplicate letters.
//
//    Examples:
//    - findWord([“P>E”,”E>R”,”R>U”]) -> PERU
//    - findWord([“I>N”,”A>I”,”P>A”,”S>P”]) -> SPAIN
//    - findWord([“U>N”, “G>A”, “R>Y”, “H>U”, “N>G”, “A>R”]) -> HUNGARY
//    - findWord([“I>F”, “W>I”, “S>W”, “F>T”]) -> SWIFT
//    - findWord([“R>T”, “A>L”, “P>O”, “O>R”, “G>A”, “T>U”, “U>G”]) -> PORTUGAL
//    - findWord([“W>I”, “R>L”, “T>Z”, “Z>E”, “S>W”, “E>R”, “L>A”, “A>N”, “N>D”, “I>T”]) -> SWITZERLAND
public class StringTopTal {

  private String findWord(String... words) {

    // Mount a hash
    Map<String, String> map = new HashMap<>();
    Set<String> secondSet = new HashSet<>();
    Set<String> firstSet = new HashSet<>();


    for (String word : words) {
      String[] splitWord = word.split(">");
      String first = splitWord[0];
      String second = splitWord[1];
      map.put(first, second);
      firstSet.add(first);
      secondSet.add(second);
    }

    firstSet.removeAll(secondSet);

    StringBuilder resultWordBuilder = new StringBuilder();

    String removeString = firstSet.iterator().next();
    resultWordBuilder.append(removeString);

    while (!map.isEmpty()) {
      removeString = map.remove(removeString);
      resultWordBuilder.append(removeString);
    }

    return resultWordBuilder.toString();
  }

  public static void main(String[] args) {
    StringTopTal s = new StringTopTal();
    System.out.println(s.findWord("R>T", "A>L", "P>O", "O>R", "G>A", "T>U", "U>G"));
  }

}
