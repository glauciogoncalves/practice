package ggoncalves.others.toptal.toptalscreen;

/**
 * Task Description
 * An S string is built up from "+" and "-" characters exclusively. The balance of such string is the number of all plus characters subtracted by the number of all minus characters within S.
 * <p>
 * For example the balance of "++-+" is 2 and the balance of "+-+-" is 0.
 * <p>
 * You can modify the account balance by removing the right most character. This result can be further modified using this same method until the resulting string is empty.
 * <p>
 * Your task is to write a function that accepts String S and Integer N. This function returns the minimum number of removals necessary for the balance to become greater than or equal to N. If the desired balance isn’t achievable the function should return -1.
 * <p>
 * Example:
 * possibleBalance("++-", 2) // should return 1
 */
public class ScreenInterview {

  //  possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 12) // 1
//  possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 13) // 2
//  possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 14) // -1
//  possibleBalance("+++---", 3) // 3
//  possibleBalance("+++-+---", 3) // 3 (3 on the rightful most)
//  possibleBalance("----+-", -2) // 4
  // -1 -1 -1 -1 +1 -1 = -4
  static int possibleBalance(String s, Integer a) {

    // can be negative
    int[] accumulateCount = new int[s.length()];

    int i = 0;
    int previous = 0;
    for (char c : s.toCharArray()) {

      int count = (c == '+') ? 1 : -1;
      accumulateCount[i++] = previous + count;
      previous += count;

    }

    // base case (right sum)
    if (accumulateCount[s.length() - 1] == a) return 0;

    // +++---
    // 1 2 3 2 1 0
    // find 3
    // 1- 2- 3!

    // find the index
    i = 0;

    while (accumulateCount[i++] != a) ;

    // 6 - 3
    // needs an if?

    // ----+-
    // -1 -2 -3 -4 -3 -4  (-2)
    // i = 1
    //
//    if (accumulateCount[i] == a) {
    System.out.println(accumulateCount.length - (i));
    return accumulateCount.length - (i);
//    }
  }


  public static void main(String[] args) {
    possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 12); // 1
    possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 13); // 2
    possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 14); // -1
    possibleBalance("+++---", 3); // 3
    possibleBalance("+++-+---", 3); // 3 (3 on the rightful most)
    possibleBalance("----+-", -2); // 4

//    System.out.println(result);
  }
}
