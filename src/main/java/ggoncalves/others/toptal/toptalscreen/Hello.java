package ggoncalves.others.toptal.toptalscreen;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Hello {


  private int[] getChange(double M, double P) {

    BigDecimal change = new BigDecimal(M - P);

    change = change.setScale(2, RoundingMode.CEILING);

    BigDecimal currentChange = change;

    int[] result = {0, 0, 0, 0, 0, 0};

    while (currentChange.compareTo(BigDecimal.valueOf(0)) != 0) {

      BigInteger bi = currentChange.toBigInteger();

      if (bi.compareTo(BigInteger.valueOf(0)) == 0) {

        if (currentChange.remainder(BigDecimal.valueOf(0.5)).compareTo(BigDecimal.valueOf(0)) == 0) {
          result[4]++;
          currentChange = currentChange.subtract(BigDecimal.valueOf(0.5));

        } else if (currentChange.remainder(BigDecimal.valueOf(0.25)).compareTo(BigDecimal.valueOf(0)) == 0) {
          result[3]++;
          currentChange = currentChange.subtract(BigDecimal.valueOf(0.25));

        } else if (currentChange.remainder(BigDecimal.valueOf(0.1)).compareTo(BigDecimal.valueOf(0)) == 0) {
          result[2]++;
          currentChange = currentChange.subtract(BigDecimal.valueOf(0.10));
        } else if (currentChange.remainder(BigDecimal.valueOf(0.05)).compareTo(BigDecimal.valueOf(0)) == 0) {
          result[1]++;
          currentChange = currentChange.subtract(BigDecimal.valueOf(0.05));

        } else if (currentChange.remainder(BigDecimal.valueOf(0.01)).compareTo(BigDecimal.valueOf(0)) == 0) {
          result[0]++;
          currentChange = currentChange.subtract(BigDecimal.valueOf(0.01));

        }


      } else {
        result[5] += bi.intValue();
        currentChange = currentChange.subtract(BigDecimal.valueOf(bi.intValue()));
      }


    }

    return result;

  }

  public static void main(String[] args) {
    Hello h = new Hello();

    int[] res = h.getChange(5, 0.99);

    for (int r : res) {
      System.out.print(r + " ");
    }

  }
}
